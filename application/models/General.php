<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class General extends CI_Model {

	var $table  = 'tab_data';
	var $id     = 'iddata';
	var $fields = array();

	function __construct(){
		parent::__construct();
	}

	function insert( $cliente = null ){
		$this->fields = array();
		if( $cliente != null && is_array($cliente) ){
			foreach( $cliente as $key => $info )
				$this->fields[$key] = $info['value'];
			$this->fields['fecha'] = date('Y-m-d H:i:s');
			return $this->db->insert($this->table, $this->fields);
		}
		return false;
	}



	function get( $id = null, $filter = null, $select = "*", $limit = "", $escape = true, $join=null){

		if( $id != null )
			$this->db->where($this->id,$id);

		if( $limit != "" )
			$this->db->limit( $limit );

		if( $filter != null && is_array($filter) )
			foreach( $filter as $key=>$data )
				$this->db->where($key,$data,$escape);

		if ( $join != null && is_array($join) ) 
			foreach ($join as $key => $data) 
				$this->db->join($key, $data, $escape);
			
		$this->db->order_by($this->id,"desc");
		$this->db->select( $select, false );
		
		$query = $this->db->get( $this->table );

		if ($query->num_rows() >= 1)
			return $query->result();
		else
			return false;
	}


 
	public function update( $object = null, $idobject = null ){
		$fields = array();
		if( $object != null && is_array($object) && $idobject != null && is_numeric($idobject) ){

			foreach( $object as $key => $info )
				$fields[$key] = $info['value'];

			if( is_numeric($idobject) && $idobject > 0 )
				$this->db->where($this->id,$idobject);

			return $this->db->update($this->table, $fields);
		}

		return false;
	}


	function delete( $idobject = null ){

		if( $idobject != null && is_numeric($idobject) ){
			$this->db->where($this->id, $idobject);
			$this->db->delete($this->table);
			return true;
		}
		return false;
	}



	/**
	* Servicio encargado de buscar un usuario para login
	*/
	public function getLogin($usuario = '',$pwd='') {
		$this->db->where("email",$usuario);
		$this->db->where("pwd",md5($pwd));
		$query = $this->db->get( $this->table );
		if ($query->num_rows() >= 1)
			return $query->result();
		else
			return false;
	}


	public function getClientInfo($usuario='', $pwd){
		$this->db->where("email",$usuario);
		$this->db->where("pwd",md5($pwd));
		$query = $this->db->get( $this->table );
		if ($query->num_rows() >= 1)
			return $query->result();
		else
			return false;
	}//end reporte function


}//end class

