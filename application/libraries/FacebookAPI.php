<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FacebookAPI {
    
    public function __construct(){
        require_once APPPATH.'third_party/Facebook/autoload.php';
    }


    public function getloginurl(){
		$login_url = "";
		$fb = new \Facebook\Facebook([
			'app_id'                => '641528966700167',
			'app_secret'            => 'c7c755ead51bb66779bbfd26861f4fc3',
			'default_graph_version' => 'v2.10',
		]);

		try {
			
			$helper = $fb->getRedirectLoginHelper();
			$login_url = $helper->getLoginUrl("http://localhost/triplo/");
			
			#echo "<a href='$login_url'>Login with Facebook </a>";

			$access_token = $helper->getAccessToken();
			if (isset($access_token)):
			
				#echo "<pre>";
				#print_r($access_token);
				#echo "</pre>";
				
				$_SESSION['access_token'] = (string)$access_token;
				$response = $fb->get('/me', $_SESSION['access_token']);
				$me = $response->getGraphUser();
				$_SESSION['FB_USER_NAME'] = $me->getName();
				

			endif;
			
			#$response = $fb->get('/me', $_SESSION['access_token']);
		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
		// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
		// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		return $login_url;
    }//end start function



}