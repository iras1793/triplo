<?php echo $header; ?>

<main>

	<!-- Burger-Brand -->



	<section class="bgslider interiorsections" id="sectionRoadTrip">
			

			<div class="sectionContent">
				<input type="text" placeholder="Trip Name" id="tripnameRT" class="input">
				<ul class="optionsTrip">
			    		<li class="Ocontainer" id="1">			    		  
						  		<h2 class="title"> where to?</h2>
						   		<input type="text" class="selectionC" name="typefrom" placeholder="From" id="PlaceFrom">
						   		<input type="text" class="selectionC" name="typeto" placeholder="To" id="PlaceTo">
						   		<hr><span class="rounded"></span>
						   		<img src="<?php echo base_url('assets/img/iconospng/car.png') ?>" alt="car" data-aos="fade-in" class="image">

			    		</li>
			    		<li class="Ocontainer" id="2">
			    				<h2 class="title"> when?</h2>
			    				<div class="inline">		    		  
							  		<input type="text" class="selectionC datepicker" name="datewhen" placeholder="Start" id="whenstart">
							  		<input type="text" class="selectionC datepicker" name="datewhen" placeholder="Finish" id="whenfinish">
							  	</div>
			    		</li>
			    		<li class="Ocontainer" id="3">	
			    				<h2 class="title"> Driving on</h2>
			    				<div class="inline">
				    				<label class="radio">
									    <input type="radio" name="car" value="1">
										<span> My car</span>
									</label>
									<label class="radio">
										<input type="radio" name="car" value="2" checked>
										<span>Rent car</span>
									</label>
								 </div>
						  		<input type="text" class="selectionC" name="typemodel" placeholder="Model">
						  		<input type="text" class="selectionC" name="typecolor" placeholder="Color">
						   		
		               </li>
		               <li class="Ocontainer" id="4">				
						  <input type="text" class="selectionC" name="typecountry" placeholder="Number of stops">
						  <div class="select-radio">
							   <label class="radio">
					             <input type="radio" name="rstops" value="1">
								 <span> select stops</span>
								</label>
								<label class="radio">
									<input type="radio" name="rstops" value="2" checked>
									<span>Select on the way</span>
					            </label> 	
						  </div>
						   		
		               </li>
			    </ul>
				
			</div>
			<a href="<?php echo site_url('travel/letsplan_rt') ?>" class="enlace"> Let's ride <img src="<?php echo base_url('assets/img/iconospng/go.png') ?>" alt="icono go" class="icon"></a>

			<div id="contentspotOc1" class="pops">
				<div class="content">
						<input type="text" class="selectionC" name="typecountry" placeholder="write">
				   		<div class="btnselectmap"><a href="#">Current Location</a></div>
				    	<span class="close-spotpop">X</span>
				    </div>
			</div>	
			
			<div id="contentspotOc2" class="pops">
				<div class="content">
						<input type="text" class="selectionC" name="typecountry" placeholder="write">
				   		<div class="btnselectmap"><a href="<?php echo site_url('travel/img/iconospng/arbustoverde.png') ?>">Open Trip</a></div>
				    	<span class="close-spotpop">X</span>
				    </div>
						   		
			</div>	

		<img src="<?php echo base_url('assets/img/road.png') ?>" alt="camino" data-aos="fade-in" class="imagebg">
		<img src="<?php echo base_url('assets/img/cloud.png') ?>" alt="camino" data-aos="fade-in" class="cloud1">
		<img src="<?php echo base_url('assets/img/cloud.png') ?>" alt="camino" data-aos="fade-in" class="cloud2">
		<img src="<?php echo base_url('assets/img/iconospng/aves.png') ?>" alt="arbustoverde"  data-aos="zoom-in-down" class="birds" data-aos-offset="300" data-aos-easing="ease-in-sine">
		<img src="<?php echo base_url('assets/img/iconospng/arbustoverde.png') ?>" alt="arbustoverde" class="greentree" data-aos="zoom-in-up">
		<img src="<?php echo base_url('assets/img/iconospng/arbustorosa.png') ?>" alt="arbustorosa" class="pinktree" data-aos="zoom-in-up">
		<img src="<?php echo base_url('assets/img/iconospng/arbustoverde.png') ?>" alt="arbustoverde" class="greentree" data-aos="zoom-in-up" id="arbustoverde2">									 
	</section>
		
		
</main>
