<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	

	<section class="bgslider interiorsections" id="sectionAddupdate">

			<div class="sectionContent">
				<h1 class="SectionName"> Add Pic (Update Place) </h1>
				<div id="addupdate" class="stepscontent">
					 <div class="container">
						    <div class="wrapper">
						      <ul class="steps">
						        <li class="is-active">Update</li>
						      </ul>
						      <form class="form-wrapper">
						        <fieldset class="section  is-active">
						       		  <h2 class="questionstep">What important update would you like to provide?...</h2>
							          <textarea name="updateplace" placeholder="Type update of place here..." class="updatedescription"></textarea> 
							       
						          <input class="submit button" type="submit" value="Add">
						        </fieldset>
						        <fieldset class="section">
						          <h3 class="congrats">Thank you so much for adding your Pic!!!</h3>
						          <p class="congratstxt">As soon as this is evaluated and verified we'll let you know and provide you with your PICS.
						          	<br>
						        	For any questions or comments feel free to contact us.</p>
						          <a href="<?php echo site_url('backpack/addpic') ?>" class="enlace"> Back to Add pic <img src="<?php echo base_url('assets/img/iconospng/go.png') ?>" alt="icono go" class="icon"></a>
						          <!-- <div class="button">Reset Form</div> -->
						        </fieldset>
						      </form>
						    </div>
						  </div>

					</div> <!-- END CONTAINER -->
				</div>	<!-- END STEPS CONTENT -->
			</div><!-- END SECTION CONTENT -->


			
	</section> <!-- END SECTION ADD IMAGE -->
</main>
