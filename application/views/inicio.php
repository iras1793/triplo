
<main class="seccionesinicio">
	<div class="head" id="headinicio">
				      <div class="logo"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 623.96 169.01"><defs><style>.cls-1{fill:#c5e1f7;}.cls-2{fill:#f1d8c2;}.cls-3{fill:#fff;stroke:#fff;stroke-miterlimit:10;stroke-width:5px;}.cls-4{fill:#cfe4c3;}</style></defs><title>triplo_h</title><g id="Capa_2" data-name="Capa 2"><g id="Capa_5" data-name="Capa 5"><path class="cls-1" d="M623.91,87.25c0,5.07.14,10.14,0,15.2-.31,9.07-2.72,17.48-8.47,24.71-6,7.59-13.75,12.37-23.49,13.62a45.38,45.38,0,0,1-18.84-1.09c-9.4-2.77-16.33-8.68-21.22-17a40.51,40.51,0,0,1-5.27-18.26c-.63-10.4-.33-20.82-.23-31.22.08-9.31,2.32-18.1,8.15-25.67a35.74,35.74,0,0,1,25.75-14.4,43.07,43.07,0,0,1,16.51,1.27c11.81,3.4,19.62,11.23,24.17,22.44a42.91,42.91,0,0,1,3,15.85c.05,4.86,0,9.72,0,14.57Zm-58.45-.17h-.05V101a26.78,26.78,0,0,0,.5,5.86c1.31,5.83,4.14,10.59,9.57,13.47,3.47,1.84,7.23,2.13,11.09,2A17.75,17.75,0,0,0,603.1,111a27.93,27.93,0,0,0,1.95-10.28c.05-8.66,0-17.31,0-26a32.75,32.75,0,0,0-.89-8.15c-1.89-7.48-6.28-12.64-14.06-14.39a22.62,22.62,0,0,0-8.37-.3,17.61,17.61,0,0,0-11.72,6.44A20.61,20.61,0,0,0,565.49,71C565.37,76.39,565.46,81.73,565.46,87.08Z"/><path class="cls-1" d="M410,110.73a4.86,4.86,0,0,0-.37,2.59c0,6.13,0,12.25,0,18.38a9.4,9.4,0,0,1-6.56,9.12,9.28,9.28,0,0,1-12.28-8.88c0-29.7.08-59.4-.09-89.11a9.79,9.79,0,0,1,9.75-10c8.4,0,16.8-.32,25.18.05,16.65.73,28.42,9,35.23,24.11a36.27,36.27,0,0,1,2.67,18.55,36.46,36.46,0,0,1-8.09,20.25c-6.21,7.64-14,12.74-23.9,14.35a34.33,34.33,0,0,1-6.09.57c-4.37-.07-8.75,0-13.12,0Zm-.21-58.42v39.4a4.36,4.36,0,0,0,.77.19c5.08,0,10.17.16,15.24,0a18.29,18.29,0,0,0,10.43-3.61c6.27-4.66,9-10.79,8.43-18.63C443.93,60.19,435.15,52,426.19,52c-4.94,0-9.88,0-14.81,0A2.26,2.26,0,0,0,409.8,52.31Z"/><path class="cls-2" d="M310.18,108c1.1,1.49,2.09,2.85,3.09,4.18l14.38,19.07c1.56,2.06,2.42,4.25,1.49,6.88a6.72,6.72,0,0,1-11.44,2.43c-2.29-2.56-4.25-5.43-6.34-8.18-5.49-7.24-11-14.46-16.42-21.75a3.61,3.61,0,0,0-3.35-1.62c-3.73.1-7.46,0-11.07,0-.77.88-.52,1.77-.52,2.58,0,7.25,0,14.5,0,21.75a19.23,19.23,0,0,1-.31,3.78,6.63,6.63,0,0,1-7.24,5.63,6.83,6.83,0,0,1-6.21-6.77c.09-24.92,0-49.84,0-74.77,0-7.67.18-15.35-.07-23a7.22,7.22,0,0,1,7.53-7.53c9,0,18.06-.11,27.09,0a39.19,39.19,0,0,1,11.27,76.55C311.51,107.5,311,107.74,310.18,108ZM280.15,95a2,2,0,0,0,1.57.43c6.84,0,13.69.11,20.53-.09s12.34-3.27,16.84-8.18a24.61,24.61,0,0,0,6.51-18.19,24.75,24.75,0,0,0-8.84-18.25c-4.95-4.37-10.87-6.11-17.32-6.23-5.79-.11-11.58,0-17.36,0a2.59,2.59,0,0,0-1.93.37Z"/><path class="cls-1" d="M500.44,122.13a5.09,5.09,0,0,0,2.61.3c8.18,0,16.37,0,24.55,0a10.09,10.09,0,0,1,7.82,3.07,9.48,9.48,0,0,1-4.2,15.39,8.6,8.6,0,0,1-2.51.28c-12.78,0-25.55-.1-38.32.06-4.34.05-9.05-4.45-9-9.07.1-30,0-60,.06-90a9.17,9.17,0,0,1,8.17-9c5.26-.38,9.39,2.58,10.3,7.34a19.86,19.86,0,0,1,.32,3.77q0,37.5,0,75A7.23,7.23,0,0,0,500.44,122.13Z"/><path class="cls-2" d="M227.76,44.67a8.8,8.8,0,0,0-.18,2.82c0,29.5-.05,59,.06,88.51a6.82,6.82,0,0,1-7.18,6.84,6.89,6.89,0,0,1-6.32-6.88c.12-18.59.05-37.18.05-55.77q0-16.57,0-33.16a5.85,5.85,0,0,0-.19-2.31c-.68-.52-1.49-.29-2.24-.3-6.21,0-12.42,0-18.63,0-4,0-6.55-2.28-7-6.14-.39-3.65,1.91-6.58,5.73-7.36a11.33,11.33,0,0,1,2.32-.17q26.67,0,53.35,0c2.72,0,5.2.53,6.89,2.91A6.32,6.32,0,0,1,255,40.8a6.49,6.49,0,0,1-6,3.61q-9.42,0-18.84,0A5.15,5.15,0,0,0,227.76,44.67Z"/><path class="cls-2" d="M353.53,86.81q0-24.18,0-48.36a8.35,8.35,0,0,1,.54-3.51c1-2.15,3-4.32,7.49-3.86a6.61,6.61,0,0,1,5.32,4.81,12.13,12.13,0,0,1,.36,3.13q0,47.94,0,95.88c0,4.11-1.69,6.68-5,7.66a6.71,6.71,0,0,1-8.69-6.33c-.05-8.16,0-16.33,0-24.5Z"/><circle class="cls-3" cx="84.51" cy="84.51" r="82.01"/><path class="cls-1" d="M85.8,59.77V2.71c5.82-1.33,28.32,4,33.7,7.88a26.4,26.4,0,0,0-8,12.89,13,13,0,0,0-.45,6.71,10.27,10.27,0,0,0,4.78,6.21c2.5,1.66,5.14,3.09,7.66,4.72a11.13,11.13,0,0,1,2.42,2.23c1.76,2.12,1.83,3.69.3,6-1,1.44-2.06,2.77-3,4.2-3,4.37-2.89,8.78.32,12.79a10.7,10.7,0,0,0,7.33,3.8c4,.52,7.54-1,10.87-3,.75-.46,1.53-.86,2.27-1.34a6.68,6.68,0,0,1,6.26-.57c2.82,1,5.68,1.85,8.58,2.54a27.14,27.14,0,0,0,5,.39c.58,0,.79.14.91.77A81.4,81.4,0,0,1,160,116a79.65,79.65,0,0,1-11.32,19.26,81.68,81.68,0,0,1-43.64,28.65,77,77,0,0,1-18.67,2.65H85.22v-1.58c0-6.65,0-13.3,0-19.94,0-1,.32-1.22,1.23-1.33,3.27-.4,6.44-1.21,9-3.44,4.07-3.56,4.77-7.63,2.37-12.67-.47-1-.9-2-1.28-3a3,3,0,0,1,.77-3.62c1.83-1.52,3.78-2.9,5.72-4.28a16.9,16.9,0,0,0,5-4.71,7.63,7.63,0,0,0-1.09-9.49,53.07,53.07,0,0,0-6.28-4.61c-2.33-1.57-4.28-3.31-4-6.45A29.94,29.94,0,0,1,97.31,87a42.26,42.26,0,0,0,1.41-12.2A20.11,20.11,0,0,0,93.9,62.6a8.57,8.57,0,0,0-6.08-2.68Z"/><path class="cls-2" d="M82.5,166.24C37.61,164.75.45,128.35,2.25,82c2-51.62,45.3-79.49,80.91-79.37v19.7c-2.15.61-4.25,1.29-5.78,3A7.65,7.65,0,0,0,76,27.19c-.4.9-1,1-1.89,1.06-8.6.24-17.15.83-25.42,3.5-4.27,1.38-4.28,1.33-5.8,5.81a101,101,0,0,0-4.38,17.08c-.48,3.23-.75,6.5-.93,9.76-.25,4.75-.33,9.5-.49,14.25q-.21,5.91-.43,11.82c-.14,3.57-.27,7.14-.46,10.71a1.2,1.2,0,0,1-.7.81A1.86,1.86,0,0,0,34.07,104c.22,2.41.34,4.84.62,7.24.34,2.92.8,5.82,1.2,8.72.07.51.14,1,.19,1.35-1.44,1.09-3,1.89-4,3.14a9.47,9.47,0,0,0,0,11.81,25.35,25.35,0,0,0,15.68,9.61,31.61,31.61,0,0,0,19.63-1.58,25.51,25.51,0,0,1,11.84-2c1.07.07,2.14,0,3.3,0Z"/><path class="cls-2" d="M55.89,79l-4.68-.3-3-.19c-2.33-.15-2.9-.73-2.88-3.07,0-3,.12-6,.15-9.06,0-4.73,0-9.45.07-14.18.05-5.53.14-11.06.17-16.59a1.24,1.24,0,0,1,1-1.42,56.44,56.44,0,0,1,14-3.3c4.35-.41,8.72-.71,13.08-.91,3.09-.14,6.19,0,9.27,0V79.39L70.5,79c.66-2-.16-4,.9-5.78.41-.7-.21-1.46-1.19-1.55-2.82-.26-5.64-.48-8.46-.69-1.21-.09-2.42-.16-3.63-.18a2,2,0,0,0-1.83,2.68,6.14,6.14,0,0,1,.14,2,12.79,12.79,0,0,1-.25,1.74C56.09,77.78,56,78.35,55.89,79Z"/><path class="cls-2" d="M44.72,120.49a7.65,7.65,0,0,1-.24-1.26c-.09-6.38-.49-12.78-.15-19.14.28-5.17,1.44-10.29,2.23-15.43.2-1.27.55-2.51.74-3.77.09-.61.31-.85.91-.8,2.31.16,4.62.29,6.93.51.24,0,.57.49.61.79a28.78,28.78,0,0,1,.2,3.08,1.69,1.69,0,0,0,1.6,1.79c2,.28,4.08.47,6.13.7,1.2.13,2.41.23,3.61.4,1.71.25,2.49-.36,2.65-2.06.13-1.49.36-3,.56-4.52,1.68,0,3.33,0,5,0,2.17,0,4.34.14,6.51.17.69,0,1,.19,1,1q-.11,10.29-.17,20.58c0,.18,0,.36-.1.68L78.85,103c-1.84-.09-3.68-.2-5.52-.24-.73,0-.82.61-1,1.22a21.23,21.23,0,0,1-1.66,4.69,5,5,0,0,1-5,2.67,44.48,44.48,0,0,0-4.62-.16,1.86,1.86,0,0,0-1.34.79,16.8,16.8,0,0,1-10.66,8.12A23.22,23.22,0,0,1,44.72,120.49Z"/><path class="cls-2" d="M42.27,108.39c-3.2,1.07-5.9-.83-6.43-4.63,0-.05.09-.13.14-.14,1.79-.15,1.77-.15,1.84-2q.32-8.89.67-17.78c.14-3.64.26-7.28.44-10.92.26-5,.45-10.07.9-15.09a77.24,77.24,0,0,1,3.82-17.22c.06-.15.16-.29.32-.6,0,5.17,0,10.15,0,15.13,0,6.71-.15,13.42-.2,20.14A12.69,12.69,0,0,0,44,77.93a1.76,1.76,0,0,0,.73,1.17c1.3.53,1,1.46.78,2.43-.88,4.89-1.93,9.77-2.55,14.71C42.47,100.21,42.49,104.24,42.27,108.39Z"/><path class="cls-2" d="M46.78,121.87c-.35,2.33-.8,4.52-1,6.73a4.32,4.32,0,0,0,1.63,3.8,2.76,2.76,0,0,0,4.12-.16l.57-.54,7.4,9.41a10.15,10.15,0,0,1-4.53,1,25.79,25.79,0,0,1-11.39-2.51,32.2,32.2,0,0,1-8.33-5.08c-4.42-3.8-2.55-8.4.59-10.89.85-.67,2.17-.75,3.28-1.09a3.78,3.78,0,0,1,.75-.12Z"/><path class="cls-2" d="M73.13,119.64h9.64v1c0,4.43,0,8.86,0,13.29a1.16,1.16,0,0,1-1,1.35c-4.53,1.17-9,2.45-13.56,3.61a47.47,47.47,0,0,1-6.24,1.23c-.57.07-1.25-.72-1.94-1.16,2.08-1.68,4-3.14,5.83-4.73,3.16-2.76,5.94-5.86,7-10.06A25,25,0,0,0,73.13,119.64Z"/><path class="cls-2" d="M58.83,137.91l-5.6-7.8c3.56-3.05,7-6,10.45-8.88a2.59,2.59,0,0,1,1.59-.36,4.43,4.43,0,0,0,4-1.82c.17-.23.37-.43.57-.67,2.29,2.21,2,4.72.74,7.1a24.69,24.69,0,0,1-3.91,5.42C64.3,133.32,61.62,135.45,58.83,137.91Z"/><path class="cls-2" d="M62.43,74.28c0,1.34,0,2.55,0,3.76,0,.63.09,1.28.85,1.28s.91-.58.9-1.25c0-1.14,0-2.28,0-3.58l5.12.39c-.34,3.69-.66,7.24-1,10.85-3.57-.35-6.85-.66-10.13-1a1.11,1.11,0,0,1-.63-.82,31.61,31.61,0,0,1,.84-9.29c.15-.58.34-.65.9-.56C60.29,74.18,61.31,74.2,62.43,74.28Z"/><path class="cls-2" d="M33.74,135.48c.88.69,1.72,1.42,2.64,2.05a35.54,35.54,0,0,0,14,5.87,20.45,20.45,0,0,0,10.72-1c2.15-.78,4.45-1.14,6.67-1.72q7.17-1.87,14.31-3.78c.17,0,.35,0,.68-.1,0,1.09,0,2.11,0,3.12,0,.23-.41.57-.67.62a7,7,0,0,1-1.77.06,30.37,30.37,0,0,0-14.18,2.44c-7.13,2.85-14.23,2.44-21.44.29A19.91,19.91,0,0,1,33.74,135.48Z"/><path class="cls-2" d="M36.06,108.43c1.88,1.65,3.84,2.58,6.42,1.72l.45,10.21c-1.62.25-3.13.51-4.65.67-.2,0-.62-.46-.66-.75-.5-3.55-.95-7.11-1.41-10.67C36.17,109.29,36.13,109,36.06,108.43Z"/><path class="cls-2" d="M82.73,113.93v3.93c-3,.65-5.88.2-8.8,0-.18,0-.37-.12-.54-.09-2.15.4-2.93-1.26-3.85-2.62-.71-1-.3-1.68,1-1.64C74.54,113.64,78.57,113.79,82.73,113.93Z"/><path class="cls-2" d="M67.39,112.88c.37,1.25.63,2.33,1,3.36s-1,2.91-2.48,2.86a4.47,4.47,0,0,0-3.71,1.1,4.87,4.87,0,0,0-.08-1.26c-.2-.48-.42-1.1-.82-1.32a2.35,2.35,0,0,1-1-3.4,2.25,2.25,0,0,1,2.49-1.36C64.43,113,66,112.88,67.39,112.88Z"/><path class="cls-2" d="M69.89,112a26.3,26.3,0,0,0,2.16-2.31,18.93,18.93,0,0,0,1.35-2.46c1.25,1.17,2.95.68,4.45,1a36.58,36.58,0,0,0,4.06.28c.62,0,.92.26.88,1-.06.87,0,1.75,0,2.59Z"/><path class="cls-2" d="M73.73,105.7l.29-1.24,8.7.4v1.59Z"/><path class="cls-2" d="M49.58,121.52a45.57,45.57,0,0,0-.89,5.11,37.89,37.89,0,0,0,.33,4.74,2,2,0,0,1-1.44-1.83,14.48,14.48,0,0,1,1.25-7.39C48.91,122,49.19,121.84,49.58,121.52Z"/><path class="cls-2" d="M50.47,122.86c1,.64.63,1.49.53,2.26a6.27,6.27,0,0,1-.4,1.25,2.26,2.26,0,0,0,1.26,2.93l-1.52,1.6C49.12,128.15,49.82,125.54,50.47,122.86Z"/><path class="cls-2" d="M60.54,121.44l-1.41,1.14A2.63,2.63,0,0,0,58,120.27c-.51-.39-.36-1.62-.35-2.5C58.66,119,59,120.68,60.54,121.44Z"/><path class="cls-2" d="M52.07,124.92c0-.67.16-1.46-.14-2-.54-.92-.13-1.43.5-2-.07,1.28,1,2.54,0,3.83-.08.1-.27.1-.41.15Z"/><path class="cls-2" d="M83.14,24.33a9.52,9.52,0,0,0-5.52,4.09C77.65,26.66,81,24.25,83.14,24.33Z"/><path class="cls-2" d="M53.83,120l.84,4.9C53.86,124.13,53.36,121,53.83,120Z"/><path class="cls-2" d="M56.82,124.71a4.07,4.07,0,0,1-1.73-3.8c1,1,.41,2.88,2.15,3.33a1.82,1.82,0,0,1-.21.29A1.45,1.45,0,0,1,56.82,124.71Z"/><path class="cls-2" d="M56.66,122,56,118.92l.31-.06.87,3Z"/><path class="cls-2" d="M58.91,116.38l1.43,2.74A2.37,2.37,0,0,1,58.91,116.38Z"/><path class="cls-2" d="M52.05,124.91l.14,3.16c-.87-.37-.7-1-.54-1.57s.28-1,.42-1.57C52.07,124.92,52.05,124.91,52.05,124.91Z"/><path class="cls-2" d="M79.86,27.92h2.07S81.67,25.6,79.86,27.92Z"/><path class="cls-4" d="M86.19,137.93V64.57s11.61-1.93,7.74,19.55c0,0-1.91,6.52-.63,11.26a6.79,6.79,0,0,0,3.55,4.3c3.84,1.92,15.61,8.67,2.79,13.7A11.31,11.31,0,0,0,93.53,119c-1.35,2.82-1.83,6.77,1.27,11.53a3.82,3.82,0,0,1,.19,3.89C93.92,136.38,91.51,138.77,86.19,137.93Z"/><path class="cls-4" d="M123.42,13.25s-16.77,17.8,0,22.44c0,0,14.2,7,6.45,17.55,0,0-7,6.45-2.06,10.32,0,0,3.61,3.61,9.29,0,0,0,8.51-7,17-2.32,0,0,7.22,2.32,9,2.06C163.16,63.3,154.64,28.21,123.42,13.25Z"/></g></g></svg></div>
				      
						<ul class="menu">
						  <li><a href="#" class="js-trigger" data-page="page__section--login" id="login">Login</a></li>
						  <li><a href="#" class="js-trigger" data-page="page__section--register" id="register">Register</a></li>
						  <li><a href="#about" id="sectionabout">About Us</a></li>
						  <li><a href="#contact" id="sectioncontact">Contact Us</a></li>
						</ul>
		</div>
		
		<section class="page__section page__section--inicio active" data-flow="rtol" id="sectionhome">
				  <div class="textoslider">
				  		<div><p class="glow">Discover</p></div> 
				  		<div><p class="glow sub">a new way of traveling </p></div>
		          </div>
		          <div class="overlayHome">
		            <div id="about" class="hidden">
						  <div class="cd-container">
								<img src="<?php echo base_url('assets/img/cloud.svg') ?>" alt="cloud" class="nube" data-aos="zoom-out-right" data-aos-delay="3200" loading="lazy">
								<h2 class="infotitle" data-aos="fade-in" data-aos-delay="3400"> About Us</h2>
								<p class="infotext" data-aos="fade-in-left" data-aos-delay="3500">texto, descripcion aqui....</p>
						  </div>


						  <div class="cd-container" id="contact">
								  <div class="container">
								  	 <img src="<?php echo base_url('assets/img/cloud.svg') ?>" alt="cloud" class="nube" 
								  	 data-aos="zoom-out" data-aos-delay="3200" loading="lazy">
								    <div class="screen" data-aos="fade-in" data-aos-delay="3400">
								      <div class="screen-body">
								        <div class="screen-body-item left">
								          <div class="app-title">
								            <span>CONTACT</span>
								            <span>US</span>
								          </div>
								          <div class="app-contact"><a href="tel:+55000000000">CONTACT INFO : +55 000 000 0000</a></div>
								        </div>
								        <div class="screen-body-item">
								          <div class="app-form">
								            <div class="app-form-group">
								              <input class="app-form-control" placeholder="NAME" value="Name">
								            </div>
								            <div class="app-form-group">
								              <input class="app-form-control" placeholder="EMAIL">
								            </div>
								            <div class="app-form-group message">
								              <input class="app-form-control" placeholder="MESSAGE">
								            </div>
								            <div class="app-form-group buttons">
								              <button class="app-form-button">SEND</button>
								            </div>
								          </div>
								        </div>
								      </div>
					    			</div>
					  			</div>

						  </div>
					</div>
		          </div>
		          <img loading="lazy" src="<?php echo base_url('assets/img/clouds.png') ?>" alt="clouds" class="conjunto-nubes" data-aos="zoom-in" data-aos-delay="500">
		          
		          <?php
		  			echo $mapa_giratorio;
				  ?>	

			  <div class="pic-ctn">
			    <img src="<?php echo base_url('assets/img/1.jpg') ?>" alt="place1carousel" class="pic" loading="lazy">
			    <img src="<?php echo base_url('assets/img/2.jpg') ?>" alt="place2carousel" class="pic" loading="lazy">
			    <img src="<?php echo base_url('assets/img/3.jpg') ?>" alt="place3carousel" class="pic" loading="lazy">
			    <img src="<?php echo base_url('assets/img/4.jpg') ?>" alt="place4carousel" class="pic" loading="lazy">
			    <img src="<?php echo base_url('assets/img/5.jpg') ?>" alt="place5carousel" class="pic" loading="lazy">
			    <img src="<?php echo base_url('assets/img/6.jpg') ?>" alt="place6carousel" class="pic" loading="lazy">
			    <img src="<?php echo base_url('assets/img/7.jpg') ?>" alt="place7carousel" class="pic" loading="lazy">
			    <img src="<?php echo base_url('assets/img/8.jpg') ?>" alt="place8carousel" class="pic" loading="lazy">
			    <img src="<?php echo base_url('assets/img/9.jpg') ?>" alt="place9carousel" class="pic" loading="lazy">
			    <img src="<?php echo base_url('assets/img/10.jpg') ?>" alt="place10carousel" class="pic" loading="lazy">
			  </div>

		</section> <!--Termina sectionHome-->
		<section id="bloque"></section>
		
		<img src="<?php echo base_url('assets/img/cloud.svg') ?>" alt="cloud" class="nubeback" loading="lazy" >
		<img src="<?php echo base_url('assets/img/cloud.svg') ?>" alt="cloud" class="nubeback2" loading="lazy">

				 



		<section class="page__section page__section--login" data-flow="rtol" id="sectionlogin">
								<div class="menuinicio">
								  <a href="#" class="js-trigger triplo-letras" data-page="page__section--inicio"><img src="<?php echo base_url('assets/img/triplo_h.svg') ?>"
								   alt="imagenLogo" class="logo" loading="lazy"></a>
								</div>

						<div class="page-wrapper">
							  <div class="grid">
					        <div class="formulariocontent">
					          <h2 class="titleForm">Login</h2>
					          <form action="" method="post" class="form" id="formlogin">
					            <div class="form__field">
					              <input type="email" placeholder="email@mailaddress.com">
					            </div>
					            <div class="form__field">
					              <input type="password" placeholder="••••••••••••">
					            </div>
					            <div class="form__field">
					              <a class="btnsend" href="<?php echo site_url('travel/index') ?>"><p>Login</p></a>
					            </div>

					            <div class="signup-connect">
					              <a href="#" class="btn btn-social btn-facebook"><span id="facebook"></span> Login with Facebook</a>
					              <a href="#" class="btn btn-social btn-twitter"><span id="twitter"></span> Login with Twitter</a>
					              <a href="#" class="btn btn-social btn-instagram"><span id="instagram"></span> Login with Instagram</a>
					            </div>

					          </form>

					        </div>

					      </div>
					    </div>
		</section> <!--Termina sectionLogin-->

		<section class="page__section page__section--register" data-flow="rtol" id="sectionregister">
		  						<div class="menuinicio">
								  <a href="#" class="js-trigger triplo-letras" data-page="page__section--inicio"><img src="<?php echo base_url('assets/img/triplo_h.svg') ?>" alt="imagenLogo" class="logo"></a>
								</div>

						<div class="page-wrapper"> 
							<div class="grid">
					        <div class="formulariocontent">
					          <h2 class="titleForm">Register</h2>
					          <form action="" method="post" class="form" id="formregistro">
					            <div class="form__field">
					              <input type="text" placeholder="Name">
					            </div>
					            <div class="form__field scountry">
					                <?php
					                    echo $select_paises;
					                  ?>       
					            </div>
					            <div class="form__field">
					              <input type="email" placeholder="email@mailaddress.com">
					            </div>
					            <div class="form__field">
					              <input type="password" placeholder="••••••••••••">
					            </div>

					            <div class="form__field">
					                <div class="sel sel--box">
					                  <select name="select-gender" id="select-gender">
					                    <option value="" disabled>Gender</option>
					                    <option value="male">Male</option>
					                    <option value="female">Female</option>
					                  </select>
					                </div>
					            </div>

					            <div class="form__field">
					              <!-- <input type="submit" value="Login" class="btnsend"> -->
					              <a class="btnsend" href="<?php echo site_url('travel/index') ?>"><p>Login</p></a>
					            </div>

					            <div class="signup-connect">
					              <a href="#" class="btn btn-social btn-facebook"><span id="facebook"></span> Login with Facebook</a>
					              <a href="#" class="btn btn-social btn-twitter"><span id="twitter"></span> Login with Twitter</a>
					              <a href="#" class="btn btn-social btn-instagram"><span id="instagram"></span> Login with Instagram</a>
					            </div>

					          </form>

					        </div>

					      </div>
					</div>
		</section> <!--Termina sectionRegister-->

</main>