<div class="menuLateral">
	<div class="hamburger">
    	<div class="hamb-ico"></div>
	</div>
	 <nav class="menuList">
	 	<ul class="b-nav">
		  <li><a class="b-link" href="<?php echo site_url('travel/picturebrowsing') ?>" target="_self">Picture Browsing</a></li>
		  <li><a class="b-link" href="<?php echo site_url('travel/startfinish') ?>" target="_self">Start - Finish</a></li>
		  <li><a class="b-link" href="<?php echo site_url('travel/topcountries') ?>" target="_self">Top Countries</a></li>
		  <li><a class="b-link" href="<?php echo site_url('travel/selectcountry') ?>" target="_self">Select a country</a></li>
		  <li><a class="b-link" href="<?php echo site_url('travel/date') ?>" target="_self">Date</a></li>
		  <li><a class="b-link" href="<?php echo site_url('travel/roadtrip') ?>" target="_self">Road trip</a></li>
		</ul>
	 </nav>
</div>