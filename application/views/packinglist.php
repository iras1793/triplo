<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->
		<!-- <p class="b-brand darkblue">Plan Journey</p>-->

	<section class="bgslider interiorsections" id="sectionPackingList">
		
			<div class="sectionContent">
				<h1 class="SectionName"> Packing List </h1>
				<ul class="contentpackingL">
					<li class="newPacking">
					  <a href="<?php echo site_url('backpack/newpacking') ?>">
						<div class="newPL">
							<span class="iconomas"> + </span>
							<p> New </p>
						</div>
					  </a>
					</li>
					<li class="listaPL">
				
							       <div class="half">
							        <div class="tab blue">
							          <input id="tab-zero" type="radio" name="tabs1" class="tabInpt" checked>
							          <label for="tab-zero" class="tabLabel">Lists <svg class="arrowD" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 275.35 238.46"><defs><style>.cls-1ad{fill:#f3bdbd;}</style></defs><title>arrowDown</title><g id="Capa_2" data-name="Capa 2"><g id="Warstwa_1" data-name="Warstwa 1"><polygon class="cls-1ad" points="137.67 238.46 0 0 275.35 0 137.67 238.46"/></g></g></svg></label>
							          <div class="tab-content">
											      	<a href="#">Russia</a>
										            <a href="#">Australia</a>
										            <a href="#">Europe</a>
							          </div>
							        </div>
							       </div>						
					</li>
				</ul>
			</div>
		<img src="<?php echo base_url('assets/img/iconospng/aves.png') ?>" alt="arbustoverde"  data-aos="zoom-in-down" class="birds" data-aos-offset="300" data-aos-easing="ease-in-sine">
		<img src="<?php echo base_url('assets/img/iconospng/arbustoverde.png') ?>" alt="arbustoverde" class="greentree" data-aos="zoom-in-up">
		<img src="<?php echo base_url('assets/img/iconospng/arbustorosa.png') ?>" alt="arbustorosa" class="pinktree" data-aos="zoom-in-up">
	</section>
</main>