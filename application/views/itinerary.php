<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->
		<!-- <p class="b-brand darkblue">Plan Journey</p>-->

	<section class="bgslider interiorsections" id="sectionItineraries">
		
			<div class="sectionContent">
				<h1 class="SectionName"> Itineraries </h1>
				<ul class="contentitineraries">
					<li class="listItinerary">
					  <a href="#">
						<img src="<?php echo base_url('assets/img/iconospng/pin.png') ?>" alt="pinicon" class="iconoItinerary pin" loading="lazy">
						<p>Russia</p>
					  </a>
					</li>
					<li class="listItinerary">
					  <a href="#">
						<img src="<?php echo base_url('assets/img/iconospng/clip.png') ?>" alt="clipicon" class="iconoItinerary" loading="lazy">
						<p>Australia</p>
					  </a>
					</li>
					<li class="listItinerary">
					  <a href="<?php echo site_url('travel/index') ?>">
						<div class="newPL">
							<span class="iconomas"> + </span>
							<p> New </p>
						</div>
					  </a>
					</li>
				</ul>
			</div>
			 <img src="<?php echo base_url('assets/img/clouds.png') ?>" alt="clouds" class="conjunto-nubes" loading="lazy">
		 	<?php
		  		echo $mapa_giratorio;
			?>	
	</section>
</main>