<?php echo $header; ?>

<main>
	<?php
  		echo $menu_lateral;
	?>	

	<section class="bgslider interiorsections" id="sectionLetsPlanSC">
			

			<div class="sectionContent">

				<div class="letstitle">
					<h2 class="title"> Let's Plan</h2>
				</div>
				<ul class="optionsLp">
			    		<li class="Ocontainer">			    		  
						   		<div class="form__field">
										<input type="text" placeholder="Name of Journey" id="tripname">   
							 	 </div>
			    		</li>
			    		<li class="Ocontainer">
			    				<div class="inline">		    		  
							  		<label><img src="<?php echo base_url('assets/img/iconospng/calendar.png') ?>" alt="icono calendar" class="icon" loading="lazy"><input type="text" class="selectionC" name="arrival" placeholder="Arrival" id="arrival"></label>
							  		<label><img src="<?php echo base_url('assets/img/iconospng/calendar.png') ?>" alt="icono calendar" class="icon" loading="lazy"><input type="text" class="selectionC" name="departure" placeholder="Departure" id="departure"></label>
							  	</div>
			    		</li>
			    		<li class="Ocontainer">	
						   	<div class="form__field">
								<input type="text" placeholder="Arrive to specific city" id="spcity">   
							</div>
							<div class="form__field">
								<input type="text" placeholder="Departure from specific city" id="spcity">   
							</div>	
		               </li>
			    </ul>
				<div data-aos="zoom-in" id="letsmap1" class="mapas">
						<div class="orb"></div>
				</div>
				<div class="contour"></div>
				<a href="<?php echo site_url('travel/whattodo') ?>" class="enlace"> Continue <img src="<?php echo base_url('assets/img/iconospng/go.png') ?>" alt="icono go" class="icon" loading="lazy"></a>
			</div>									 
	</section>
		
</main>
