<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->
	<!--<p class="b-brand">Plan Journey</p>-->

	<section class="bgslider interiorsections" id="sectionNotes">

			<div class="sectionContent">
				<h1 class="SectionName"> Notes </h1>
				<ul class="itemsList">
					<li class='menu-item'>
			            <a class='itemsbackpack' href='#' target='_self'>
			              <img src="<?php echo base_url('assets/img/iconospng/notes.png') ?>" alt="icononotes" class="imgsectionsback">
			              <img src="<?php echo base_url('assets/img/iconospng/notesHover.png') ?>" alt="icononotes" class="imghover">
			              <p class="nameSectionBack" id="Atrip"> Australia </p></a>
			        </li>
			        <li class='menu-item'>
			            <a class='itemsbackpack' href='#' target='_self'>
			              <img src="<?php echo base_url('assets/img/iconospng/notes.png') ?>" alt="icononotes" class="imgsectionsback">
			              <img src="<?php echo base_url('assets/img/iconospng/notesHover.png') ?>" alt="icononotes" class="imghover">
			              <p class="nameSectionBack" id="Etrip"> Europe </p></a>
			        </li>
			        <li class='menu-item'>
			            <a class='itemsbackpack' href='<?php echo site_url('backpack/newnote') ?>' target='_self'>
			              <img src="<?php echo base_url('assets/img/iconospng/notes.png') ?>" alt="icononotes" class="imgsectionsback">
			              <img src="<?php echo base_url('assets/img/iconospng/notesHover.png') ?>" alt="icononotes" class="imghover">
			              <p class="nameSectionBack" id="newNote"> + <br> New </p></a>
			        </li>
				</ul>
			</div>
	</section>
</main>