<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	

	<section class="bgslider interiorsections" id="sectionAddpic">

			<div class="sectionContent">
				<h1 class="SectionName"> Add Pic </h1>
				<ul class="addpiclist">
					<li class='piclist-item'>
			            <a class='btnselectmap' href='<?php echo site_url('backpack/addimage') ?>' target='_self'> Add image </a>
			        </li>
			        <li class='piclist-item'>
			            <a class='btnselectmap' href='<?php echo site_url('backpack/addupdate') ?>' target='_self'> Add  a place update</a>
			        </li>
			        </li>
			        <li class='piclist-item'>
			            <a class='btnselectmap' href='<?php echo site_url('backpack/addstop') ?>' target='_self'> Add some place</a>
			        </li>
				</ul>
			</div>
	</section>
</main>
