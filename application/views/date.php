<?php echo $header; ?>

<main>

	<?php echo $menu_lateral; ?>

	<!-- Burger-Brand -->
	<p class="b-brand">Date</p>

	<section class="bgslider interiorsections" id="sectionDate">
		  <!-- <img src="img/clouds.png" alt="clouds" class="conjunto-nubes"> -->

		 <?php
		  		echo $mapa_giratorio;
			?>	
		
		  <div class="date_tab">
				
							       <div class="half">
							        <div class="tab blue">
							          <input id="tab-date" type="radio" name="tabdate" class="tabInpt" checked>
							          <label for="tab-date" class="tabLabel">Date <svg class="arrowD" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 275.35 238.46"><defs><style>.cls-1ad{fill:#f3bdbd;}</style></defs><title>arrowDown</title><g id="Capa_2" data-name="Capa 2"><g id="Warstwa_1" data-name="Warstwa 1"><polygon class="cls-1ad" points="137.67 238.46 0 0 275.35 0 137.67 238.46"/></g></g></svg></label>
							          <div class="tab-content">
											 <form  class="form" id="form-date" action="date_submit" method="get" accept-charset="utf-8">
											 	<ul class="date-list">
												    <li>
														<label class="radio">
													        <input type="radio" name="r" value="1">
													        <span> One way</span>
													    </label>
													    <label class="radio">
													        <input type="radio" name="r" value="2" checked>
													        <span>Round trip</span>
													    </label>
												    </li>
												    <li>
												      <div class="form__field">
												      	  <label class="labeldate" for="departure">Departure</label>
											              <input type="text" placeholder="dd/mm/yyyy" name="departure" id="departure" class="datepicker">
											          </div>
											          <div class="form__field">
												      	  <label class="labeldate" for="return">Return</label>
											              <input type="text" placeholder="dd/mm/yyyy" name="return" id="return" class="datepicker">
											          </div>
												    </li>
												    <li>
												    	<div class="form__field">
											              <input type="text" placeholder="Where" name="where" id="where">
											          </div>
											          <div class="select-checkbox">
														  <input type="checkbox" id="selectingMap" name="selectingMap" value="">
														  <label for="selectingMap" data-content="SelectOnMap"><a href="#popnotsure" class="venobox" data-vbtype="inline">Not sure </a></label>
													  </div>
												    </li>
												    <li>		  		 	
												  		<a href="#" id="addMplaces"><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="icono más" class="icon"> More places </a>
												  		<a href="<?php echo site_url('travel/letsplanselectcountry') ?>" id="go"> GO </a>
												    </li>
												</ul>		
											 </form>
											    		
							          </div>
							        </div>
							       </div>						
			</div>			

	</section>
	

</main>
