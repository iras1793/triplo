<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->
	<!--<p class="b-brand darkblue">Plan Journey</p>-->

	<section class="bgslider interiorsections" id="sectionPackingList">
		
			<div class="sectionContent">
				<h1 class="SectionName"> Packing List </h1>
				<div class="formulario">
				    <form action="newnote_submit" method="get" accept-charset="utf-8" id="formPackingL" class="form">
						<div class="form__field">
					       <input type="text" placeholder="Name" class="txtBlue">
					    </div>
					    <div class="form__field">
					       <input type="text" placeholder="yy / mm / dd" class="txtBlue datepicker">
					    </div>

						<div class="dropdowns">
							<div class="columnas">
								<div id="col-left">
							       <div class="half">
							        <div class="tab blue">
							          <input id="tab-four" type="radio" name="tabs2" class="tabInpt" checked>
							          <label class="tabLabel" for="tab-four">Clothes <img src="<?php echo base_url('assets/img/iconospng/pensil.png') ?>" alt="iconopensil" class="iconopensil"></a><svg class="arrowD" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 275.35 238.46"><defs><style>.cls-1ad{fill:#f3bdbd;}</style></defs><title>arrowDown</title><g id="Capa_2" data-name="Capa 2"><g id="Warstwa_1" data-name="Warstwa 1"><polygon class="cls-1ad" points="137.67 238.46 0 0 275.35 0 137.67 238.46"/></g></g></svg></label>
							          	  <div class="tab-content">
								            	  <div class="optionList">
											      	<input  class="optionLIn" type="checkbox" id="article1C" name="article-1C" value="">
											      	<label  class="optionLabel" for="article1C">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article2C" name="article-2C" value="Check me!" checked disabled>
												  	<label  class="optionLabel" for="article2C">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article3C" name="article-3C" value="Check me!">
												  	<label  class="optionLabel" for="article3C">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article4C" name="article-4C" value="Check me!">
												  	<label  class="optionLabel" for="article4C">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article5C" name="article-5C" value="Check me!">
												  	<label  class="optionLabel" for="article5C">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article6C" name="article-6C" value="Check me!">
												  	<label  class="optionLabel" for="article6C">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article7C" name="article-7C" value="Check me!">
												  	<label  class="optionLabel" for="article7C">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
								         </div>
							          </div>

							        <div class="tab blue">
							          <input id="tab-five" type="radio" name="tabs2" class="tabInpt">
							          <label class="tabLabel" for="tab-five">Toilettries <img src="<?php echo base_url('assets/img/iconospng/pensil.png') ?>" alt="iconopensil" class="iconopensil"></a><svg class="arrowD" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 275.35 238.46"><defs><style>.cls-1ad{fill:#f3bdbd;}</style></defs><title>arrowDown</title><g id="Capa_2" data-name="Capa 2"><g id="Warstwa_1" data-name="Warstwa 1"><polygon class="cls-1ad" points="137.67 238.46 0 0 275.35 0 137.67 238.46"/></g></g></svg></label>
							          <div class="tab-content">
							            		 <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article1T" name="article-1T" value="Check me!" checked>
												  	<label  class="optionLabel" for="article1T">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article2T" name="article-2T" value="Check me!">
												  	<label  class="optionLabel" for="article2T">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article3T" name="article-3T" value="Check me!">
												  	<label  class="optionLabel" for="article3T">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article4T" name="article-4T" value="Check me!">
												  	<label  class="optionLabel" for="article14T">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article5T" name="article-5T" value="Check me!">
												  	<label  class="optionLabel" for="article5T">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article6T" name="article-6T" value="Check me!">
												  	<label  class="optionLabel" for="article6T">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article7T" name="article-7T" value="Check me!">
												  	<label  class="optionLabel" for="article7T">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
							          </div>
							        </div>						
							 </div><!-- end half -->
							</div> <!--end col-left -->

								<div id="col-right">
							        <div class="tab blue">
							          <input id="tab-six" type="radio" name="tabs2" class="tabInpt">
							          <label class="tabLabel" for="tab-six">Electronics <img src="<?php echo base_url('assets/img/iconospng/pensil.png') ?>" alt="iconopensil" class="iconopensil"></a><svg class="arrowD" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 275.35 238.46"><defs><style>.cls-1ad{fill:#f3bdbd;}</style></defs><title>arrowDown</title><g id="Capa_2" data-name="Capa 2"><g id="Warstwa_1" data-name="Warstwa 1"><polygon class="cls-1ad" points="137.67 238.46 0 0 275.35 0 137.67 238.46"/></g></g></svg></label>
							          <div class="tab-content">
							           			  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article1E" name="article-1E" value="Check me!">
												  	<label  class="optionLabel" for="article1E">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article2E" name="article-2E" value="Check me!" checked disabled>
												  	<label  class="optionLabel" for="article2E">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article3E" name="article-3E" value="Check me!">
												  	<label  class="optionLabel" for="article3E">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article4E" name="article-4E" value="Check me!">
												  	<label  class="optionLabel" for="article4E">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article5E" name="article-5E" value="Check me!">
												  	<label  class="optionLabel" for="article5E">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article6E" name="article-6E" value="Check me!">
												  	<label  class="optionLabel" for="article6E">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article7E" name="article-7E" value="Check me!">
												  	<label  class="optionLabel" for="article7E">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
							          </div>
							        </div>

							        <div class="tab blue">
							          <input id="tab-seven" type="radio" name="tabs2" class="tabInpt">
							          <label class="tabLabel" for="tab-seven">Entertainment <img src="<?php echo base_url('assets/img/iconospng/pensil.png') ?>" alt="iconopensil" class="iconopensil"></a><svg class="arrowD" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 275.35 238.46"><defs><style>.cls-1ad{fill:#f3bdbd;}</style></defs><title>arrowDown</title><g id="Capa_2" data-name="Capa 2"><g id="Warstwa_1" data-name="Warstwa 1"><polygon class="cls-1ad" points="137.67 238.46 0 0 275.35 0 137.67 238.46"/></g></g></svg></label>
							          <div class="tab-content">
							            		  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article1ET" name="article-1ET" value="Check me!">
												  	<label  class="optionLabel" for="article1ET">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article2ET" name="article-2ET" value="Check me!" checked disabled>
												  	<label  class="optionLabel" for="article2ET">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article3ET" name="article-3ET" value="Check me!">
												  	<label  class="optionLabel" for="article3ET">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article4ET" name="article-4ET" value="Check me!">
												  	<label  class="optionLabel" for="article4ET">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article5ET" name="article-5ET" value="Check me!">
												  	<label  class="optionLabel" for="article5ET">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article6ET" name="article-6ET" value="Check me!">
												  	<label  class="optionLabel" for="article6ET">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article7ET" name="article-7ET" value="Check me!">
												  	<label  class="optionLabel" for="article7ET">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
							          </div>
							        </div>

							        <div class="tab blue">
							          <input id="tab-eight" type="radio" name="tabs2" class="tabInpt">
							          <label class="tabLabel" for="tab-eight">Documents <img src="<?php echo base_url('assets/img/iconospng/pensil.png') ?>" alt="iconopensil" class="iconopensil"></a><svg class="arrowD" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 275.35 238.46"><defs><style>.cls-1ad{fill:#f3bdbd;}</style></defs><title>arrowDown</title><g id="Capa_2" data-name="Capa 2"><g id="Warstwa_1" data-name="Warstwa 1"><polygon class="cls-1ad" points="137.67 238.46 0 0 275.35 0 137.67 238.46"/></g></g></svg></label>
							          <div class="tab-content">
							            		  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article1D" name="article-1D" value="Check me!">
												  	<label  class="optionLabel" for="article1D">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article2D" name="article-2D" value="Check me!" checked>
												  	<label  class="optionLabel" for="article2D">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article3D" name="article-3D" value="Check me!">
												  	<label  class="optionLabel" for="article3D">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article4D" name="article-4D" value="Check me!">
												  	<label  class="optionLabel" for="article4D">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article5D" name="article-5D" value="Check me!" checked>
												  	<label  class="optionLabel" for="article5D">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article6D" name="article-6D" value="Check me!">
												  	<label  class="optionLabel" for="article6D">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
												  <div class="optionList">
											      	<input class="optionLIn" type="checkbox" id="article7D" name="article-7D" value="Check me!">
												  	<label  class="optionLabel" for="article7D">
														<input  class="txtCL" type="text" id="" name="" value="Article name">
													</label>
												  </div>
							          </div>
							        </div>

							        <div class="savebtn">
							          	<button class="btnSv" type="submit"><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="iconomore"></button>
							        </div>

							        
							    </div> <!-- end col-right -->
      						</div> <!-- end columnas -->								
						</div> <!-- end dropdowns -->
				    </form>
				    <ul class="btnsNote">
						<li class="btns"><a href="<?php echo site_url('backpack/packinglist') ?>"><img src="<?php echo base_url('assets/img/iconospng/arrowback.png') ?>" alt="iconoArrowBack"></a></li>
					</ul>					
				</div>
			</div>
		<img src="<?php echo base_url('assets/img/iconospng/aves.png') ?>" alt="arbustoverde"  data-aos="zoom-in-down" class="birds" data-aos-offset="300" data-aos-easing="ease-in-sine">
		<img src="<?php echo base_url('assets/img/iconospng/arbustoverde.png') ?>" alt="arbustoverde" class="greentree" data-aos="zoom-in-up">
		<img src="<?php echo base_url('assets/img/iconospng/arbustorosa.png') ?>" alt="arbustorosa" class="pinktree" data-aos="zoom-in-up">
	</section>
</main>