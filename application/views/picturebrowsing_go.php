<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>
	
	<!-- Burger-Brand -->
	<p class="b-brand">Picture Browsing</p>

	<section class="bgslider interiorsections" id="sectionPictureGo">
			<div class="sectionContent">
						<ul class="screenshot_slider owl-carousel">
							 <li class="item">
								 	<img src="<?php echo base_url('assets/img/PlaceTop13.jpg') ?>" alt="imagen" class="imgpictureB">
								      <ul class="optionsImage">
								      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconpictureB"></a></li>
								      	<li><a href="#picContent"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpictureB"></a></li>
								      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/trash.png') ?>" alt="icono trash" class="iconpictureB"></a></li>
								      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/info2.png') ?>" alt="icono info" class="iconpictureB infopictureB"></a></li>
								      </ul>
								      <p class="pictureName"> Place name </p>
								      <div class="infoPic">
								      	<div class="content-infoPic">
									      	<p class="picturesafety"> Safety </p>
									      	<p class="picturePopularity"> Popularity </p>
									      	<p class="pictureRestoration"> This place is under restoration </p>
									      	<p class="pictureGuide"> You don't need Guide for this place </p>
									    </div>
								      </div>
							 </li>
							 <li class="item">
								 	<img src="<?php echo base_url('assets/img/PlaceTop11.jpg') ?>" alt="imagen" class="imgpictureB">
								      <ul class="optionsImage">
								      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconpictureB"></a></li>
								      	<li><a href="#picContent"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpictureB"></a></li>
								      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/trash.png') ?>" alt="icono trash" class="iconpictureB"></a></li>
								      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/info2.png') ?>" alt="icono info" class="iconpictureB infopictureB"></a></li>
								      </ul>
								      <p class="pictureName"> Place name </p>
								      <div class="infoPic">
								      	<div class="content-infoPic">
									      	<p class="picturesafety"> Safety </p>
									      	<p class="picturePopularity"> Popularity </p>
									      	<p class="pictureRestoration"> This place is under restoration </p>
									      	<p class="pictureGuide"> You don't need Guide for this place </p>
									    </div>
							 </li>
							 <li class="item">
								 		<img src="<?php echo base_url('assets/img/PlaceTop7.jpg') ?>" alt="imagen" class="imgpictureB">
									      <ul class="optionsImage">
									      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconpictureB"></a></li>
									      	<li><a href="#picContent"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpictureB"></a></li>
									      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/trash.png') ?>" alt="icono trash" class="iconpictureB"></a></li>
									      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/info2.png') ?>" alt="icono info" class="iconpictureB infopictureB"></a></li>
									      </ul>
									      <p class="pictureName"> Place name </p>
									      <div class="infoPic">
									      	<div class="content-infoPic">
										      	<p class="picturesafety"> Safety </p>
										      	<p class="picturePopularity"> Popularity </p>
										      	<p class="pictureRestoration"> This place is under restoration </p>
										      	<p class="pictureGuide"> You don't need Guide for this place </p>
										    </div>
							 </li>
							 <li class="item">
								 	<img src="<?php echo base_url('assets/img/PlaceTop4.jpg') ?>" alt="imagen" class="imgpictureB">
								      <ul class="optionsImage">
								      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconpictureB"></a></li>
								      	<li><a href="#picContent"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpictureB"></a></li>
								      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/trash.png') ?>" alt="icono trash" class="iconpictureB"></a></li>
								      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/info2.png') ?>" alt="icono info" class="iconpictureB infopictureB"></a></li>
								      </ul>
								      <p class="pictureName"> Place name </p>
								      <div class="infoPic">
								      	<div class="content-infoPic">
									      	<p class="picturesafety"> Safety </p>
									      	<p class="picturePopularity"> Popularity </p>
									      	<p class="pictureRestoration"> This place is under restoration </p>
									      	<p class="pictureGuide"> You don't need Guide for this place </p>
									    </div>
							 </li>
							 <li class="item">
										<img src="<?php echo base_url('assets/img/PlaceTop10.jpg') ?>" alt="imagen" class="imgpictureB">
									      <ul class="optionsImage">
									      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconpictureB"></a></li>
									      	<li><a href="#picContent"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpictureB"></a></li>
									      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/trash.png') ?>" alt="icono trash" class="iconpictureB"></a></li>
									      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/info2.png') ?>" alt="icono info" class="iconpictureB infopictureB"></a></li>
									      </ul>
									      <p class="pictureName"> Place name </p>
									      <div class="infoPic">
									      	<div class="content-infoPic">
										      	<p class="picturesafety"> Safety </p>
										      	<p class="picturePopularity"> Popularity </p>
										      	<p class="pictureRestoration"> This place is under restoration </p>
										      	<p class="pictureGuide"> You don't need Guide for this place </p>
										    </div> 
							 </li>
							 <li class="item">
								 		<img src="<?php echo base_url('assets/img/PlaceTop3.jpg') ?>" alt="imagen" class="imgpictureB">
									      <ul class="optionsImage">
									      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconpictureB"></a></li>
									      	<li><a href="#picContent"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpictureB"></a></li>
									      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/trash.png') ?>" alt="icono trash" class="iconpictureB"></a></li>
									      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/info2.png') ?>" alt="icono info" class="iconpictureB infopictureB"></a></li>
									      </ul>
									      <p class="pictureName"> Place name </p>
									      <div class="infoPic">
									      	<div class="content-infoPic">
										      	<p class="picturesafety"> Safety </p>
										      	<p class="picturePopularity"> Popularity </p>
										      	<p class="pictureRestoration"> This place is under restoration </p>
										      	<p class="pictureGuide"> You don't need Guide for this place </p>
										    </div>
							 </li>
						</ul>


				
			</div> <!-- end section content -->
			
			<a href="<?php echo site_url('travel/picturebrowsing_go') ?>" class="viewmaplink"><img src="<?php echo base_url('assets/img/iconospng/view.png') ?>" alt="icono view" class="icon"> View Map </a>
		  	<a href="<?php echo site_url('travel/letsplan') ?>"> Let's plan <img src="<?php echo base_url('assets/img/iconospng/go.png') ?>" alt="icono go" class="icon"></a>

		 <?php echo $mapa_giratorio; ?>
		

		  		

	</section>
	

</main>
