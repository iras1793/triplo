<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->
	<!--<p class="b-brand">Plan Journey</p>-->

	<section class="bgslider interiorsections" id="sectionTranslator">

			<div class="sectionContent">
				<h1 class="SectionName"> Translator </h1>
				<div class="formulario">
				    <form action="newnote_submit" method="get" accept-charset="utf-8" id="formtranslator" class="form">
						<div class="form__field">
									<p class="instruction">From :</p>
					                <div class="sel sel--box">
					                  <select name="select-language" id="select-language">
					                    <option value="" disabled>Language:</option>
					                    <option value="english">English</option>
					                    <option value="spanish">Spanish</option>
					                  </select>
					                </div>
					    </div>
					     <textarea name="txttranslate" placeholder="Type in here the text you want to translate ..." id="txtranslate"></textarea>

					   <div class="form__field">
					   				<p class="instruction">To :</p>
					                <div class="sel sel--box">
					                  <select name="select-language" id="select-language">
					                    <option value="" disabled>Language:</option>
					                    <option value="english">English</option>
					                    <option value="spanish">Spanish</option>
					                  </select>
					                </div>
					   </div>
					    <textarea name="txttranslate" id="txtranslated"></textarea>
				   
							</div>
				    </form>
					
				</div>
			</div>
	</section>
</main>