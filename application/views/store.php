<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->
		<!--<p class="b-brand darkblue">Plan Journey</p>-->

	<section class="bgslider interiorsections" id="sectionStore">
		
			<div class="sectionContent">
				<h1 class="SectionName"> Store </h1>
				<div class="containerStore">
					<div class="searchfilter" id="search">
						<form id="search-form" action="" method="POST" enctype="multipart/form-data" class="inlineSearch">
							<div class="form-group col-xs-9">
								<input class="form-control" type="text" placeholder="Search" />
							</div>
							<div class="form-group col-xs-3">
								<button type="submit" class="btnsend">Search</button>
							</div>
						</form>
					</div>
					<div class="searchfilter" id="filter">
						<form  class="inlineSearch">
							<div class="form-group col-sm-3 col-xs-6 ">
								<select data-filter="item" class="filter-item filter form-control">
									<option value="">Item</option>
									<option value="">Show All</option>
									<option value="">Fotography</option>
									<option value="">Gear</option>
									<option value="">Flights</option>
									<option value="">Vocation Package</option>
									<option value="">Hiking gear</option>
									<option value="">Acomodation</option>
									<option value="">Other</option>
								</select>
							</div>
							<div class="form-group col-sm-3 col-xs-6">
								<select data-filter="by" class="filter-by filter form-control">
									<option value="">Filter by</option>
									<option value="">Show All</option>
									<option value="">Location</option>
									<option value="">Price Low to High</option>
									<option value="">Price High to Low</option>
								</select>
							</div>
						</form>
					</div>
					<div class="searchfilter" id="productsList">
						<div class="product">
						  <p class="seller__name"> Daniela is selling...</p>
						  <p class="product__price"> $120</p>
						  <img class="product__image" src="<?php echo base_url('assets/img/productos/nikefloral.png') ?>">
						  <h1 class="product__title">Nike Roshe Run Print</h1>
						  <hr />
						  <p class="product__description">The Nike Roshe One Print Men's Shoe offers breathability, lightweight cushioning and bold style with an injected unit midsole and colorful mesh upper. </p>
						  <a href="#" class="product__btn btn">Buy Now</a>
						  <a href="#" class="product__btn btn contactSeller">Contact seller</a>
						</div>

						<div class="product">
						  <p class="seller__name"> Genaro is selling...</p>
						  <p class="product__price"> $60</p>
						  <img class="product__image" src="<?php echo base_url('assets/img/productos/rope.png') ?>">
						  <h1 class="product__title">CLIMBED ROPE</h1>
						  <hr />
						  <p class="product__description">Measurement: 50 mm x 10 mtr • Fique material with plastic and iron stop. </p>
						  <a href="#" class="product__btn btn">Buy Now</a>
						  <a href="#" class="product__btn btn contactSeller">Contact seller</a>
						</div>

						<div class="product">
						  <p class="seller__name"> Mariana is selling...</p>
						  <p class="product__price"> $90</p>
						  <div class="product-carousel">
						  			<ul class='product_slider owl-carousel'>
									    <li class='item'>
									      <img src="<?php echo base_url('assets/img/productos/colemanCamp.png') ?>" alt="imagen" class="imgpictureB">
									    </li>
									    <li class='item'>
									      <img src="<?php echo base_url('assets/img/productos/colemanCamp2.png') ?>" alt="imagen" class="imgpictureB">
									    </li>
									    <li class='item'>
									      <img src="<?php echo base_url('assets/img/productos/colemanCamp3.png') ?>" alt="imagen" class="imgpictureB">
									    </li>
									    <li class='item'>
									      <img src="<?php echo base_url('assets/img/productos/colemanCamp4.png') ?>" alt="imagen" class="imgpictureB">
									    </li>
									    <li class='item'>
									      <img src="<?php echo base_url('assets/img/productos/colemanCamp5.png') ?>" alt="imagen" class="imgpictureB">
									    </li>
									</ul>
						  </div>
						  <h1 class="product__title">Coleman "Darwin" Tent</h1>
						  <hr />
						  <p class="product__description">The Darwin series offers a compact dome shop that mounts quickly. When dimensions and weight are essential and necessary. For 1-2 people.</p>
						  <a href="#" class="product__btn btn">Buy Now</a>
						  <a href="#" class="product__btn btn contactSeller">Contact seller</a>
						</div>

					</div>
				</div><!-- END CONTAINER STORE -->
			</div><!-- END SECTION CONTENT -->
		<img src="<?php echo base_url('assets/img/iconospng/aves.png') ?>" alt="arbustoverde"  data-aos="zoom-in-down" class="birds" data-aos-offset="300" data-aos-easing="ease-in-sine">
		<img src="<?php echo base_url('assets/img/iconospng/arbustoverde.png') ?>" alt="arbustoverde" class="greentree" data-aos="zoom-in-up">
		<img src="<?php echo base_url('assets/img/iconospng/arbustorosa.png') ?>" alt="arbustorosa" class="pinktree" data-aos="zoom-in-up">
	</section>


</main>
