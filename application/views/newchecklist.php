<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->
	<!-- <p class="b-brand">Plan Journey</p> -->

	<section class="bgslider interiorsections" id="sectionNewCheckList">
		
			<div class="sectionContent">
				<h1 class="SectionName"> New Check List </h1>
				<div class="content bge">
					<div class="form__field">
					       <input type="text" placeholder="Name" class="txtBlue">
					</div>

					<div class="form__field">
					       <input type="text" placeholder="yy / mm / dd" class="txtBlue datepicker">
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="artone" name="artone" value="" >
							<label  class="optionLabel" for="artone">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="arttwo" name="arttwo" value="" >
							<label  class="optionLabel" for="arttwo">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="artthree" name="artthree" value="">
							<label  class="optionLabel" for="artthree">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="artfour" name="artfour" value="" >
							<label  class="optionLabel" for="artfour">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="artfive" name="artfive" value="">
							<label  class="optionLabel" for="artfive">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="artsix" name="artsix" value="">
							<label  class="optionLabel" for="artsix">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="artseven" name="artseven" value="">
							<label  class="optionLabel" for="artseven">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="arteight" name="arteight" value="">
							<label  class="optionLabel" for="arteight">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="artnine" name="artnine" value="">
							<label  class="optionLabel" for="artnine">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="artten" name="artten" value="">
							<label  class="optionLabel" for="artten">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="arteleven" name="arteleven" value="">
							<label  class="optionLabel" for="arteleven">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="arttwelve" name="arttwelve" value="">
							<label  class="optionLabel" for="arttwelve">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="artthirteen" name="artthirteen" value="">
							<label  class="optionLabel" for="artthirteen">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>
					<div class="optionList">
							<input  class="optionLIn" type="checkbox" id="artfourteen" name="artfourteen" value="">
							<label  class="optionLabel" for="artfourteen">
								<input  class="txtCL" type="text" id="" name="" value="">
							</label>
					</div>

					<ul class="btnsNote">
						 <li class="btns"><a href="<?php echo site_url('backpack/checklist') ?>"><img src="<?php echo base_url('assets/img/iconospng/arrowback.png') ?>" alt="iconoArrowBack"></a></li>
						 <li class="btns"><a href=""><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="iconomore"></a></li>
						</ul>
					
				</div>
			</div>
	</section>
</main>
