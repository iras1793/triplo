<?php echo $header; ?>


<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->
	<!--<p class="b-brand">Plan Journey</p> -->

	<section class="bgslider interiorsections" id="sectionNotes">

			<div class="sectionContent">
				<h1 class="SectionName"> Notes </h1>
				<div class="formulario">
				    <form action="newnote_submit" method="get" accept-charset="utf-8" id="formNote" class="form">
						<div class="form__field">
					       <input type="text" placeholder="Name">
					    </div>
					    <div class="form__field">
					       <input type="text" placeholder="yy / mm / dd" class="datepicker">
					    </div>
						<!-- The toolbar will be rendered in this container. -->
							<div id="toolbar-container"></div>

							<!-- This container will become the editable. -->
							<div id="editor">
							    <p>Write a note ...</p>
							</div>

						<ul class="btnsNote">
						 <li class="btns"><a href="<?php echo site_url('backpack/notes') ?>"><img src="<?php echo base_url('assets/img/iconospng/arrowback.png') ?>" alt="iconoArrowBack"></a></li>
						 <li class="btns"><a href=""><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="iconomore" loading="lazy"></a></li>
						</ul>

				    </form>
					
				</div>
			</div>
	</section>
</main>
