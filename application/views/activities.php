<?php echo $header; ?>


<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->

	<section class="bgslider interiorsections" id="sectionActivities">

			<div class="sectionContent">
				<h1 class="SectionName"> Activities List </h1>
				<ul class="item-grid">
  
					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/beach.jpg') ?>" alt="icono go" class="image" loading="lazy">
					    </div>
					      <ul class="optionsImage">					      	
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpicture icon" loading="lazy"></a></li>
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconbucket icon" loading="lazy"></a></li>
					      </ul>
					      <p class="activityName"> Swimming </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>

					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/rappel.jpg') ?>" alt="icono go" class="image" loading="lazy">
					    </div>
					      <ul class="optionsImage">					      	
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpicture icon" loading="lazy"></a></li>
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconbucket icon" loading="lazy"></a></li>
					      </ul>
					      <p class="activityName"> Rappel </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>

					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/surf.jpg') ?>" alt="icono go" class="image" loading="lazy">
					    </div>
					      <ul class="optionsImage">					      	
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpicture icon" loading="lazy"></a></li>
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconbucket icon" loading="lazy"></a></li>
					      </ul>
					      <p class="activityName"> Surf </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>

					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/diving.jpg') ?>" alt="icono go" class="image" loading="lazy">
					    </div>
					      <ul class="optionsImage">					      	
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpicture icon" loading="lazy"></a></li>
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconbucket icon" loading="lazy"></a></li>
					      </ul>
					      <p class="activityName"> Diving </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>

					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/senderism.jpg') ?>" alt="icono go" class="image" loading="lazy">
					    </div>
					      <ul class="optionsImage">					      	
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpicture icon" loading="lazy"></a></li>
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconbucket icon" loading="lazy"></a></li>
					      </ul>
					      <p class="activityName"> Senderism </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>

					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/scalade.jpg') ?>" alt="icono go" class="image" loading="lazy">
					    </div>
					      <ul class="optionsImage">					      	
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpicture icon" loading="lazy"></a></li>
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconbucket icon" loading="lazy"></a></li>
					      </ul>
					      <p class="activityName"> Scalade </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>

					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/winetasting.jpg') ?>" alt="icono go" class="image" loading="lazy">
					    </div>
					      <ul class="optionsImage">					      	
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpicture icon" loading="lazy"></a></li>
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconbucket icon" loading="lazy"></a></li>
					      </ul>
					      <p class="activityName"> Wine Tasting </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>

					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/camping.jpg') ?>" alt="icono go" class="image" loading="lazy">
					    </div>
					      <ul class="optionsImage">					      	
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpicture icon" loading="lazy"></a></li>
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconbucket icon" loading="lazy"></a></li>
					      </ul>
					      <p class="activityName"> Camping </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>

					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/zipline.jpg') ?>" alt="icono go" class="image" loading="lazy">
					    </div>
					      <ul class="optionsImage">					      	
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpicture icon" loading="lazy"></a></li>
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconbucket icon" loading="lazy"></a></li>
					      </ul>
					      <p class="activityName"> Zip Line </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>

					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/rowing.jpg') ?>" alt="icono go" class="image" loading="lazy">
					    </div>
					      <ul class="optionsImage">					      	
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/pinlocation.png') ?>" alt="icono pic" class="iconpicture icon" loading="lazy"></a></li>
					      	<li><a href="#"><img src="<?php echo base_url('assets/img/iconospng/bucketlist2.png') ?>" alt="icono bucketlist" class="iconbucket icon" loading="lazy"></a></li>
					      </ul>
					      <p class="activityName"> Rowing </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>
				  
					 

				</ul> <!-- item grid ends -->

			</div>

			<a href="<?php echo site_url('travel/whodecides') ?>" class="enlace"> Go <img src="<?php echo base_url('assets/img/iconospng/go.png') ?>" alt="icono go" class="icon" loading="lazy"></a>
	</section>


</main>
