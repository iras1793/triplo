<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="manifest" href="site.webmanifest">
	<link rel="apple-touch-icon" href="<?php echo base_url('assets/img/icon.png') ?>">

	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url('assets/img/favicon.ico') ?>" type="image/x-icon">

	<link rel="stylesheet" href="<?php echo base_url('assets/css/normalize.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/particulas.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/aos.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/venobox.css') ?>">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css">
	<link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@3.2.12/dist/leaflet-routing-machine.css" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.min.css') ?>">
	<script type="text/javascript" src="<?php echo base_url('assets/js/ckeditor.js') ?>"></script>

	<meta name="theme-color" content="#fafafa">
</head>

<body>
  <div class="loader"></div>
  
  <div class="containerparticles">
      <div class="particle-1"></div>
  </div>



	<?php echo $content; ?>




	<?php echo $div_abre; ?>
	


	<footer></footer>
	<div id="backpack" style="display: none;">
		<div class='menubackpack'>
			<hr class="imglines" id="lineNotes">
			<hr class="imglines" id="lineChecklist">
			<hr class="imglines" id="lineItinerary">
			<hr class="imglines" id="lineAdd">
			<hr class="imglines" id="linePackingL">
			<hr class="imglines" id="lineMoneyconverter">
			<hr class="imglines" id="lineTrackMoney">
			<hr class="imglines" id="lineTranslator">
			<hr class="imglines" id="lineStore">
			<hr class="imglines" id="lineActivities">
		 
			<img src="<?php echo base_url('assets/img/mochilaCircle.png') ?>" alt="mochila" class="imgbackpack">
		  
			<ul>
				<li class='menu-item' id="icoNotes">
					<a class='itemsbackpack' href='<?php echo site_url('backpack/notes') ?>' target='_self'>
						<img src="<?php echo base_url('assets/img/iconospng/notesHover.png') ?>" alt="icononotas" class="imgsectionsback">
						<p class="nameSectionBack"> Notes</p>
					</a>
				</li>
			 <li class='menu-item' id="icoChecklist">
				<a class='itemsbackpack' href='<?php echo site_url('backpack/checklist') ?>' target='_self'>
				  <img src="<?php echo base_url('assets/img/iconospng/checklistHover.png') ?>" alt="iconochecklist" class="imgsectionsback">
				  
				  <p class="nameSectionBack"> Check List</p></a>
			 </li>
			 <li class='menu-item' id="icoItinerary">
				<a class='itemsbackpack' href='<?php echo site_url('backpack/itinerary') ?>' target='_self'>
				  <img src="<?php echo base_url('assets/img/iconospng/itineraryHover.png') ?>" alt="iconoitinerario" class="imgsectionsback">
				  
				  <p class="nameSectionBack"> Itinerary </p></a>
			 </li>
			 <li class='menu-item' id="icoAdd">
				<a class='itemsbackpack' href='<?php echo site_url('backpack/addpic') ?>' target='_self'>
				  <img src="<?php echo base_url('assets/img/iconospng/addHover.png') ?>" alt="iconoadd" class="imgsectionsback">
				  
				  <p class="nameSectionBack"> Add Pic </p></a>
			 </li>
			 <li class='menu-item' id="icoPackingL">
				<a class='itemsbackpack' href='<?php echo site_url('backpack/packinglist') ?>' target='_self'>
				  <img src="<?php echo base_url('assets/img/iconospng/packingLHover.png') ?>" alt="iconopackinglist" class="imgsectionsback">
				 
				  <p class="nameSectionBack"> Packing List</p></a>
			 </li>
			 <li class='menu-item' id="icoMoneyconverter">
				<a class='itemsbackpack' href='<?php echo site_url('backpack/moneyconverter') ?>' target='_self'>
				  <img src="<?php echo base_url('assets/img/iconospng/moneyconverterHover.png') ?>" alt="iconomoneyconverter" class="imgsectionsback">
				 
				  <p class="nameSectionBack"> Money Converter </p></a>
			 </li>
			 <li class='menu-item' id="icoTrackMoney">
				<a class='itemsbackpack' href='<?php echo site_url('backpack/trackmoney') ?>' target='_self'>
				  <img src="<?php echo base_url('assets/img/iconospng/trackmoneyHover.png') ?>" alt="iconotrackmoney" class="imgsectionsback">
				  <p class="nameSectionBack"> Track Money </p></a>          
			 </li>
			 <li class='menu-item' id="icoTranslator">
				<a class='itemsbackpack' href='<?php echo site_url('backpack/translator') ?>' target='_self'>
				  <img src="<?php echo base_url('assets/img/iconospng/translatorHover.png') ?>" alt="iconotraductor" class="imgsectionsback">
				  
				  <p class="nameSectionBack"> Translator </p></a>
			 </li>
			 <li class='menu-item' id="icoStore">
				<a class='itemsbackpack' href='<?php echo site_url('backpack/store') ?>' target='_self'>
				  <img src="<?php echo base_url('assets/img/iconospng/store2Hover.png') ?>" alt="iconotienda" class="imgsectionsback">
				  
				  <p class="nameSectionBack"> Store </p></a>
			 </li>
			 <li class='menu-item' id="icoActivities">
				<a class='itemsbackpack' href='<?php echo site_url('backpack/activities') ?>' target='_self'>
				  <img src="<?php echo base_url('assets/img/iconospng/activities.png') ?>" alt="iconoactividades" class="imgsectionsback">
				 
				  <p class="nameSectionBack"> Activities </p></a>
			 </li>
		  </ul>
		</div>
</div>


<div id="popfirst" style="display: none;">
	 <div class='messagepop' id="popfirstCont">
		  <p class="txtpops"> What whould you like to do first? </p>
		  <ul class="options">
					 <li class="btnselectmap"><a href="<?php echo site_url('travel/activities') ?>">Select activities for each place</a></li>
					 <li class="btnselectmap"><a href="<?php echo site_url('travel/whodecides') ?>">Select number of days to stay per city</a></li>
					 <li class="btnselectmap"><a href="#">Save and continue later</a></li>
		  </ul>
	 </div>      
</div>



<div id="popexceed" style="display: none;">
	 <div class='messagepop'>
		<p class="txtpops"> You have selected of <span class="dayspics"> 15 days </span>  in your pics but you plan is for only <span class="daysplan"> 9 days:</span> </p>
		<ul class="options">
		  <li><a class='btnselectmap' href='<?php echo site_url('travel/tripplandays') ?>' target='_self'>Change days in my plan</a></li>
		  <li><a class='btnselectmap' href='<?php echo site_url('travel/tripplan') ?>' target='_self'> Change days in my pics </a></li>
		  <li><a class='btnselectmap' href='<?php echo site_url('travel/tripplan') ?>' target='_self'> Delete pics </a></li>
		</ul>
	 </div>
</div>

<div id="popnotsure" style="display: none;">
	 <div class='messagepop' id="popfirstCont">
		  <p class="txtpops"> Would you like to start by? </p>
		  <ul class="options">
					 <li class="btnselectmap"><a href="<?php echo site_url('travel/selectcountry') ?>">Selecting a country</a></li>
					 <li class="btnselectmap"><a href="<?php echo site_url('travel/picturebrowsing') ?>">Selecting a continet</a></li>
					 <li class="btnselectmap"><a href="<?php echo site_url('travel/picturebrowsing_go') ?>">Picture browse randomly from any location</a></li>
		  </ul>
	 </div>      
</div>







	<script src="<?php echo base_url('assets/js/vendor/modernizr-3.8.0.min.js') ?>"></script>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous">
	</script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>window.jQuery || document.write('<script src="<?php echo base_url("js/vendor/jquery-3.4.1.min.js") ?>"><\/script>')</script>
	<script src="<?php echo base_url('assets/js/plugins.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/main.js') ?>"></script>
	<script src="https://d3js.org/d3.v4.min.js"></script>
	<!-- <script src="https://d3js.org/queue.v1.min.js"></script> -->
	<script src="https://d3js.org/topojson.v0.min.js"></script>
	<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
	<script src="https://unpkg.com/leaflet-routing-machine@3.2.12/dist/leaflet-routing-machine.js"></script>
	<script src="<?php echo base_url('assets/js/mapas.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/TweenMax.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/venobox.min.js') ?>"></script>

  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <!-- <script>
	 window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
	 ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script> -->
</body>

</html>

<?php echo $div_cierra; ?>