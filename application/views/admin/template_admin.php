<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo $title; ?></title>
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/icon.png') ?>" />
  <meta name="description" content="A premium collection of beautiful hand-crafted Bootstrap 4 admin dashboard templates and dozens of custom components built for data-driven applications.">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/font-awesome.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/fonts.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/bootstrap.css') ?>" >
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/shards-dashboards.1.3.1.min.css') ?>" id="main-stylesheet" data-version="1.3.1">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/extras.1.3.1.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/preloader.css') ?>">
  <script async defer src="<?php echo base_url('assets/admin/js/git-buttons.js') ?>"></script>
  <!-- custom css -->
  <?php if (isset($estilos)): ?>  
    <?php foreach ($estilos as $key => $css): ?>
        <link rel="stylesheet" href="<?php echo base_url($css) ?>" >
    <?php endforeach ?>
  <?php endif ?>
  <!-- end custom css -->

</head>


<body class="h-100">

<!-- preloader -->

<div class="loader" id="preloader">
  <div class="loader-inner">
    <div class="loader-line-wrap">
      <div class="loader-line"></div>
    </div>
    <div class="loader-line-wrap">
      <div class="loader-line"></div>
    </div>
    <div class="loader-line-wrap">
      <div class="loader-line"></div>
    </div>
    <div class="loader-line-wrap">
      <div class="loader-line"></div>
    </div>
    <div class="loader-line-wrap">
      <div class="loader-line"></div>
    </div>
  </div>
</div>

<!-- end preloader -->

  <div class="container-fluid">
    <div class="row">
      <main class="main-content col-lg-12 col-md-12 col-sm-12 p-0">
        <div class="main-navbar  bg-white">
          <div class="container p-0">
            <!-- Main Navbar -->
            <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
              
              <a class="navbar-brand" href="<?php echo site_url('admin/dashboard') ?>" style="line-height: 25px;">
                <div class="d-table m-auto">
                  <img id="main-logo" class="d-inline-block align-top mr-1 ml-3" style="min-width: 35px;" src="<?php echo base_url('assets/img/triplo_logo.svg') ?>" alt="Triplo-logo" syle="">
                  
                </div>
              </a>

              <form action="#" class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">
                <div class="input-group input-group-seamless ml-3">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <i class="fas fa-search"></i>
                    </div>
                  </div>
                  <input class="navbar-search form-control" type="text" placeholder="Buscar en el sistema" aria-label="Search">
                </div>
              </form>

              <ul class="navbar-nav border-left flex-row border-right ml-auto">
<!--                 <li class="nav-item border-right dropdown notifications">
                  <a class="nav-link nav-link-icon text-center" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="nav-link-icon__wrapper">
                      <i class="material-icons">&#xE7F4;</i>
                      <span class="badge badge-pill badge-danger">2</span>
                    </div>
                  </a>

                  <div class="dropdown-menu dropdown-menu-small" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="#">
                      <div class="notification__icon-wrapper">
                        <div class="notification__icon">
                          <i class="material-icons">&#xE6E1;</i>
                        </div>
                      </div>

                      <div class="notification__content">
                        <span class="notification__category">Analytics</span>
                        <p>Your website’s active users count increased by <span class="text-success text-semibold">28%</span> in the last week. Great job!</p>
                      </div>

                    </a>

                    <a class="dropdown-item" href="#">
                      <div class="notification__icon-wrapper">
                        <div class="notification__icon">
                          <i class="material-icons">&#xE8D1;</i>
                        </div>
                      </div>
                      <div class="notification__content">
                        <span class="notification__category">Sales</span>
                        <p>Last week your store’s sales count decreased by <span class="text-danger text-semibold">5.52%</span>. It could have been worse!</p>
                      </div>
                    </a>
                      
                    <a class="dropdown-item notification__all text-center" href="#"> View all Notifications </a>
                  </div>
                </li> -->

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <?php #$imagen = ($this->auth->userImage()!='') ? $this->auth->userImage() : 'ico.png'; ?>
                    <?php $imagen = 'ico.png'; ?>
                    <img class="user-avatar rounded-circle mr-2" src='<?php echo base_url("assets/img/tile.png") ?>' alt="User Avatar"> 
                    <span class="d-none d-md-inline-block"><?php echo $this->auth->userName(); ?></span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-small">
                    <?php #$iduser = $this->auth->userid(); ?>
                    <?php $iduser = '1' ?>
                    <a class="dropdown-item" href="<?php echo site_url("admin/profile/index/$iduser") ?>"><i class="material-icons">&#xE7FD;</i> Perfil</a>
                    <a class="dropdown-item" href="<?php echo site_url('admin/dashboard/respaldar') ?>"><i class="material-icons">&#xE2C7;</i> Respaldar</a>
                    <a class="dropdown-item" href="<?php echo site_url('admin/dashboard/conf') ?>"><i class="material-icons">&#xE8B8;</i> Conf</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" href="<?php echo site_url('admin/login/logout') ?>">
                      <i class="material-icons text-danger">&#xE879;</i> Salir </a>
                    </div>
                </li>
              </ul>

              <nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar  d-inline d-lg-none text-center " data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
               </nav>
            </nav>
          </div> <!-- / .container -->
        </div> <!-- / .main-navbar -->
        
        <div class="header-navbar collapse d-lg-flex p-0 bg-white border-top">
          <div class="container">
            <div class="row">
              <div class="col">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                  <li class="nav-item">
                    <a href="<?php echo site_url('admin/dashboard') ?>" class="<?php echo ( $pagina=='dashboard' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-tasks"></i> Dashboard</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('admin/users') ?>" class="<?php echo ( $pagina=='users' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-user"></i> Users</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('admin/countries') ?>" class="<?php echo ( $pagina=='countries' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-flag"></i> Countries</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('admin/countries/topcountries') ?>" class="<?php echo ( $pagina=='topcountries' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-star"></i> Top Countries</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('admin/moneyconverter') ?>" class="<?php echo ( $pagina=='moneyconverter' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-transfer"></i> Money Converter</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('admin/images') ?>" class="<?php echo ( $pagina=='images' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-picture"></i> Images</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('admin/products') ?>" class="<?php echo ( $pagina=='products' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-shopping-cart"></i> Products</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('admin/clients') ?>" class="<?php echo ( $pagina=='clients' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-globe"></i> Clients</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('admin/reports') ?>" class="<?php echo ( $pagina=='reports' )  ? 'nav-link active' : 'nav-link'; ?>" ><i class="glyphicon glyphicon-stats"></i> Reports</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('admin/help') ?>" class="<?php echo ( $pagina=='reports' )  ? 'nav-link active' : 'nav-link'; ?>" ><i class="glyphicon glyphicon-question-sign"></i> Help</a>
                  </li>
                  
                </ul>
              </div>
            </div>
          </div>
        </div>


      <div class="main-content-container container">

         <!-- Page Header -->
         <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
               <span class="text-uppercase page-subtitle "><?php echo $miga_pan ?></span>
               <h3 class="page-title mt-2"><?php echo $encabezado ?></h3>
            </div>
         </div>

        <?php echo $content; ?>

      </div> <!-- termina main-content-container container -->



      </main>

    </div>

  </div>

  <!-- generic js -->
  <script src="<?php echo base_url('assets/admin/js/loader.js') ?>" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/admin/js/jquery.js') ?>"></script>
  <script src="<?php echo base_url('assets/admin/js/popper.js') ?>"></script>
  <script src="<?php echo base_url('assets/admin/js/bootstrap.js') ?>"></script>
  <!-- end generic js -->
  
  <!-- custom js -->
  <?php if(is_array($javascript)):?>
          <?php foreach ($javascript as $key => $js): ?>
              <script type="text/javascript" src="<?php echo base_url($js) ?>"></script>
          <?php endforeach ?>
  <?php endif; ?>
<!-- end custom js -->

  <script>
    $(window).on('load', function() {
        $('#preloader').remove();
        
    });
  </script>
  <?php #if ($pagina!='dashboard'): ?>
  <script>
    
    $(window).on('load', function() {
        if (window.jQuery) {  
          $(document).ready(function() {
            
            if( $('#genericTable').val() !== undefined ){
                var titulo = $('#tableTitle').text();
                var table =  $('#genericTable').DataTable( {
                      dom: 'Bfrtip',
                      scrollX: true,
                      buttons: [
                          {
                            extend: 'excel',
                            title:titulo,
                            footer: true,
                            exportOptions: {
                              columns: 'thead th:not(#noExport)'
                            }
                          },
                          {
                            extend: 'pdf',
                            title:titulo,
                            footer: true,
                            exportOptions: {
                              columns: 'thead th:not(#noExport)'
                            }
                          }
                      ]
                });
    
                table.button( 0 ).nodes().css( 'background', '#3ba858' );
                table.button( 0 ).nodes().css( 'color', '#FFFFFF' );
                table.button( 1 ).nodes().css( 'background', '#ff4554' );
                table.button( 1 ).nodes().css( 'color', '#FFFFFF' );
    
              
              
            }
          });
        }
    });

  </script>    
  <?php #endif ?>
  <!-- end custom js -->


</body>


</html>