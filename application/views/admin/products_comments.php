
  <div class="row">
    <div class="col">
      
      <a href="<?php echo site_url("admin/products") ?>" class="btn btn-danger mb-3 mt-3"><i class="glyphicon glyphicon-arrow-left"></i> Go Back</a>
      
      <div class="card card-small mb-4">
        
        <div class="card-header border-bottom">
          <h6 class="m-0" id="tableTitle"><?php echo $encabezado ?></h6>
          
          
        </div>
        <div class="">

          <table class="" id="genericTable">
            <thead class="bg-light">
              <tr>
                <th scope="col" class="border-0">#</th>
                <th scope="col" class="border-0">Cliente</th>
                <th scope="col" class="border-0">Comment</th>
                <th scope="col" class="border-0">Rating</th>
                <th scope="col" class="border-0">Date</th>
                <!-- <th scope="col" class="border-0" id="noExport"></th> -->
              </tr>
            </thead>
            <tbody>
              <?php if (is_array($comentarios)): ?>
                <?php $i=1; ?>
                <?php foreach ($comentarios as $key => $data): ?>                  
                  <tr>
                    <td><?php echo $i ?></td>
                    <td>
                      <a href='<?php echo site_url("clients/profile/$data->idcliente") ?>' alt="View profile"><?php echo $data->nombre ?></a>
                        
                    </td>
                    <td><?php echo $data->comentario ?></td>
                    <td>
                        
                        <?php for($j=1; $j<=$data->rating; $j++): ?> 
                            <img class="img-fluid" src="<?php echo base_url("assets/img/star.png") ?>" alt="star" style="max-width: 50px;">
                        <?php endfor; ?>
                        
                    </td>
                    <td><?php echo date("jS F, Y - G:i:s", strtotime($data->fecha));  ?></td>
                  </tr>
                  <?php $i++; ?>
                <?php endforeach ?>
              <?php endif ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
