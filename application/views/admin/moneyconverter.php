<style>
  .error{height: 30px;}
  label[class='error']{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  div[class='error']{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;margin:10px;}
</style>


<div class="alert alert-success" id="success-alert">
  <strong id="success-text"></strong>
</div>
<div class="alert alert-danger" id="danger-alert">
  <strong id="danger-text"></strong>
</div>

<?php echo _print_messages(); ?>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure you want to delete this currency?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Select one option
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-danger" data-dismiss="modal">Cancel</a>
        <a href="#" class="btn btn-success" id="delete">Yes, delete</a>
      </div>
    </div>
  </div>
</div>

    <div class="row">
        <div class="col-lg-10 mx-auto mt-4">
          
          <?php echo _print_messages(); ?>
          <div class="card card-small edit-user-details mb-4">
            <div class="card-body p-0">
            <a href="<?php echo site_url("admin/moneyconverter/add/") ?>" class="btn btn-success float-right mb-3 mt-3"><i class="fas fa-plus"></i> Add New Currency</a>
              <div class="border-bottom clearfix d-flex">
                <ul class="nav nav-tabs border-0 mt-auto mx-4 pt-2">
                  <li class="nav-item">
                    <a class="nav-link active" href="#">General Data for Currency</a>
                  </li>
                </ul>
              </div>
              <?php echo form_open_multipart("admin/moneyconverter", array('class'=>'py-4', 'id'=>'form-validate')); ?>
                <div class="form-row mx-4">
                  <div class="col mb-3">
                    <h6 class="form-text m-0">Data </h6>
                  </div>
                </div>
                <div class="form-row mx-4">
                  <div class="col-lg-12">
                    <div class="form-row">
                      
                      <?php if (is_array($converter)): ?>
                          <?php foreach ($converter as $key => $data): ?>
                                
                                <div class="form-group col-md-3">
                                  <label for="countryName"><?php echo $data->nombre ?></label>
                                    <br>
                                    <?php $imagen = $data->imagen_pais; ?>
                                    <img src='<?php echo base_url("assets/monedas/$imagen") ?>' alt="Avatar" style="max-width: 45px; height: auto;">
                                </div>
                                
                                <div class="form-group col-md-3">
                                  <label for="capitalName"><?php echo $data->tipo ?></label>
                                    <br>
                                    <?php $imagen = $data->imagen_moneda; ?>
                                    <img src='<?php echo base_url("assets/monedas/$imagen") ?>' alt="Avatar" style="max-width: 45px; height: auto;">
                                </div>

                                <div class="form-group col-md-3">
                                  <label for="<?php echo strtoupper($data->tipo) ?>"></label>
                                  <input type="text" name="valor" class="form-control" id="<?php echo strtoupper($data->tipo) ?>" value="<?php echo '$ '.$data->valor.' '.$data->tipo; ?>"  readonly>
                                </div>

                                <div class="form-group col-md-3">

                                    <a href='<?php echo site_url("admin/moneyconverter/edit/$data->id") ?>' class="btn btn-warning text-white ml-3 mt-3">Edit</a>
                                    <a href="javascript:void(0);" class="btn btn-danger mt-3 ml-3 eliminar"  id="<?php echo $data->id ?>">Delete</a>
                                    
                                </div>

                          <?php endforeach ?>
                      <?php endif ?>

                    </div>
                  </div>

                </div>

            </div>

            <div class="card-footer border-top">
                <a href="javascript:void(0);" class="btn btn-accent ml-auto d-table mr-3" id="refreshCurrency">Refresh all currency</a>
            </div>

            </form>
          </div>
          <!-- End Edit User Details Card -->
        </div>
    </div> 

<script>
  
   window.onload = function(){
      if (window.jQuery) {

          $("#success-alert").hide();
          $("#danger-alert").hide();
          
          $("#delete-modal").hide();

          $('.eliminar').click(function eliminar(event){
              $('.modal-body').load('/render/62805',function(result){
                $('#exampleModalCenter').modal({show:true});
                $('#delete').attr("href", "<?php echo site_url("admin/moneyconverter/delete/") ?>"+event.target.id);
              });
          });

          $("#refreshCurrency").click(function (){
              $.ajax({
                 'url' : '<?php echo base_url('admin/moneyconverter/refreshCurrency') ?>',
                 'type' : 'GET',
                 success : function(data) {
                      var keys;
                      var info = JSON.parse(data);
                      for (keys in info.result[0]) {
                          $("#" + keys).val("$ "+info.result[0][keys]['value']+" "+keys);
                      }
                 },
                 error : function(request,error){
                    console.log("Request: "+JSON.stringify(request));
                 }
              });//end ajax
          });

          $("#success").fadeTo(2000, 500).slideUp(500, function() {
            $("#success").slideUp(500);
          });

          $("#error").fadeTo(2000, 500).slideUp(500, function() {
            $("#error").slideUp(500);
          });
          
      }//end if
   }//end window onload

</script>