<div id="reportPage">

  <div class="row">
    <div class="col">
      <div class="card card-small mb-4">
        <div class="card-header border-bottom">
          <h6 class="m-0" id="tableTitle"><?php echo $encabezado ?></h6>
          
        </div>
        <div class="">
          <!-- <a href="" class="btn btn-success float-right">Agregar nuevo usuario</a> -->


          <!-- comienza tabs -->
          <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="pills-inventario-tab" data-toggle="pill" href="#pills-inventario" role="tab" aria-controls="pills-inventario" aria-selected="true">Stock</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pills-sesiones-mes-tab" data-toggle="pill" href="#pills-sesiones-mes" role="tab" aria-controls="pills-sesiones-mes" aria-selected="false">New clients per month</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pills-sesiones-semana-tab" data-toggle="pill" href="#pills-sesiones-semana" role="tab" aria-controls="pills-sesiones-semana" aria-selected="false">Products to be validate</a>
            </li>
            
            <!--
            <li class="nav-item">
              <a class="nav-link" id="pills-entrenadores-mes-tab" data-toggle="pill" href="#pills-entrenadores-mes" role="tab" aria-controls="pills-entrenadores-mes" aria-selected="false">Entrenadores - Sesiones por mes</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pills-entrenadores-semana-tab" data-toggle="pill" href="#pills-entrenadores-semana" role="tab" aria-controls="pills-entrenadores-semana" aria-selected="false">Entrenadores- Sesiones por semana</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pills-venta-tab" data-toggle="pill" href="#pills-venta" role="tab" aria-controls="pills-venta" aria-selected="false">Venta de Productos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pills-paquetes-tab" data-toggle="pill" href="#pills-paquetes" role="tab" aria-controls="pills-paquetes" aria-selected="false">Paquetes vendidos</a>
            </li>
            -->
          </ul>

          <div class="tab-content" id="pills-tabContent">

            <div class="tab-pane fade show active" id="pills-inventario" role="tabpanel" aria-labelledby="pills-inventario-tab">
                <button id="btnInventario" class="btn btn-danger float-right">PDF</button>
                <canvas id="myChart" width="400" height="250"></canvas>
            </div>
            <div class="tab-pane fade" id="pills-sesiones-mes" role="tabpanel" aria-labelledby="pills-sesiones-mes-tab">
                <button id="btnPaquetes" class="btn btn-danger float-right">PDF</button>
                <canvas id="myChart2" width="400" height="250"></canvas>
            </div>
            <div class="tab-pane fade" id="pills-sesiones-semana" role="tabpanel" aria-labelledby="pills-sesiones-semana-tab">
                <button id="btnSocios" class="btn btn-danger float-right">PDF</button>
                <canvas id="myChart3" width="400" height="250"></canvas>
            </div>
            <div class="tab-pane fade" id="pills-entrenadores-mes" role="tabpanel" aria-labelledby="pills-entrenadores-mes-tab">
                <button id="btnRegistro" class="btn btn-danger float-right">PDF</button>
                <canvas id="myChart4" width="400" height="250"></canvas>
            </div>
            <div class="tab-pane fade" id="pills-entrenadores-semana" role="tabpanel" aria-labelledby="pills-entrenadores-semana-tab">
                <button id="btnVisitas" class="btn btn-danger float-right">PDF</button>
                <canvas id="myChart6" width="400" height="250"></canvas>
            </div>
            <div class="tab-pane fade" id="pills-venta" role="tabpanel" aria-labelledby="pills-venta-tab">
                <button id="btnVentas" class="btn btn-danger float-right">PDF</button>
                <canvas id="myChart5" width="400" height="250"></canvas>
            </div>
            <div class="tab-pane fade" id="pills-paquetes" role="tabpanel" aria-labelledby="pills-paquetes-tab">
                <button id="btnPaquetesVendidos" class="btn btn-danger float-right">PDF</button>
                <canvas id="myChart7" width="400" height="250"></canvas>
            </div>

            
          </div>
          <!-- termina tabs -->


        </div>
      </div>
    </div>
  </div>


  
</div>
<script>

  window.onload = function(){
      //var ctx = document.getElementsByClassName('myChart').getContext('2d');
      var ctx = document.getElementById('myChart').getContext('2d');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
              datasets: [{
                  label: '',
                  data: [12, 19, 3, 5, 2, 3],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      'rgba(75, 192, 192, 0.2)',
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
      var ctx3 = document.getElementById('myChart3').getContext('2d');
      var myChart3 = new Chart(ctx3, {
          type: 'bar',
          data: {
              labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
              datasets: [{
                  label: '',
                  data: [12, 19, 3, 5, 2, 3],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      'rgba(75, 192, 192, 0.2)',
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
      var ctx4 = document.getElementById('myChart4').getContext('2d');
      var myChart4 = new Chart(ctx4, {
          type: 'bar',
          data: {
              labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
              datasets: [{
                  label: '',
                  data: [12, 19, 3, 5, 2, 3],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      'rgba(75, 192, 192, 0.2)',
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
      var ctx5 = document.getElementById('myChart5').getContext('2d');
      var myChart5 = new Chart(ctx5, {
          type: 'bar',
          data: {
              labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
              datasets: [{
                  label: '',
                  data: [12, 19, 3, 5, 2, 3],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      'rgba(75, 192, 192, 0.2)',
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
      var ctx6 = document.getElementById('myChart6').getContext('2d');
      var myChart6 = new Chart(ctx6, {
          type: 'bar',
          data: {
              labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
              datasets: [{
                  label: '',
                  data: [12, 19, 3, 5, 2, 3],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      'rgba(75, 192, 192, 0.2)',
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
      var ctx7 = document.getElementById('myChart7').getContext('2d');
      var myChart7 = new Chart(ctx7, {
          type: 'bar',
          data: {
              labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
              datasets: [{
                  label: '',
                  data: [12, 19, 3, 5, 2, 3],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      'rgba(75, 192, 192, 0.2)',
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
/*      var ctx8 = document.getElementById('myChart8').getContext('2d');
      var myChart8 = new Chart(ctx8, {
          type: 'bar',
          data: {
              labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
              datasets: [{
                  label: '',
                  data: [12, 19, 3, 5, 2, 3],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      'rgba(75, 192, 192, 0.2)',
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });*/

      var ctx2 = document.getElementById('myChart2').getContext('2d');
      var myLineChart = new Chart(ctx2, {
          type: 'line',
          data: {
            labels: ['January', 'February', 'March'],
            datasets: [{
              label: 'Primero',
              backgroundColor: window.chartColors.red,
              borderColor: window.chartColors.red,
              data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
              ],
              fill: false,
            }, {
              label: 'Segundo',
              fill: false,
              backgroundColor: window.chartColors.blue,
              borderColor: window.chartColors.blue,
              data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
              ],
            }]
          },
          options: {
            responsive: true,
            title: {
              display: true,
              text: ''
            },
            tooltips: {
              mode: 'index',
              intersect: false,
            },
            hover: {
              mode: 'nearest',
              intersect: true
            },
            scales: {
              xAxes: [{
                display: true,
                scaleLabel: {
                  display: true,
                  labelString: 'Mes'
                }
              }],
              yAxes: [{
                display: true,
                scaleLabel: {
                  display: true,
                  labelString: 'Valor'
                }
              }]
            }
          }
      });

      /**/

      //add event listener to button
      document.getElementById('btnInventario').addEventListener("click", downloadPDF.bind(null,'#myChart', 'Stock'), false);
      document.getElementById('btnPaquetes').addEventListener("click", downloadPDF.bind(null,'#myChart2', 'New clients per month'), false);
      document.getElementById('btnSocios').addEventListener("click", downloadPDF.bind(null,'#myChart3', 'Products to be validate'), false);
 /*
      document.getElementById('btnRegistro').addEventListener("click", downloadPDF.bind(null,'#myChart4', 'Entrenadores - Sesiones por mes'), false);
      document.getElementById('btnVentas').addEventListener("click", downloadPDF.bind(null,'#myChart5', 'Ventas'), false);
      document.getElementById('btnVisitas').addEventListener("click", downloadPDF.bind(null,'#myChart6', 'Entrenadores - Sesiones por semana'), false);
      document.getElementById('btnPaquetesVendidos').addEventListener("click", downloadPDF.bind(null,'#myChart7', 'Paquetes vendidos'), false);
 */

      //donwload pdf from original canvas
      function downloadPDF(grafica, titulo) {
        var canvas = document.querySelector(grafica);
        //creates image
        var canvasImg = canvas.toDataURL();
        
        //creates PDF from img
        
        var doc = new jsPDF('landscape');
        doc.setFontSize(20);
        
        doc.text(15, 15, titulo);
        doc.addImage(canvasImg, 'JPEG', 10, 10, 280, 150 );
        doc.save('canvas.pdf');
      }
      /**/



  };

</script>