

   <div class="row">
      <div class="col-12 col-md-6 col-lg-3 mb-4">
         <div class="stats-small card card-small">
            <div class="card-body px-0 pb-0">
               <div class="d-flex px-3">
                  <div class="stats-small__data">
                     <span class="stats-small__label mb-1 text-uppercase">Clients</span>
                     <h6 class="stats-small__value count m-0">2,390</h6>
                  </div>
                  <div class="stats-small__data text-right align-items-center">
                     <span class="stats-small__percentage stats-small__percentage--increase">12.4%</span>
                  </div>
               </div>
               <canvas height="60" class="analytics-overview-stats-small-1"></canvas>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
         <div class="stats-small card card-small">
            <div class="card-body px-0 pb-0">
               <div class="d-flex px-3">
                  <div class="stats-small__data">
                     <span class="stats-small__label mb-1 text-uppercase">New clients this month</span>
                     <h6 class="stats-small__value count m-0">8,391</h6>
                  </div>
                  <div class="stats-small__data text-right align-items-center">
                     <span class="stats-small__percentage stats-small__percentage--decrease">7.21%</span>
                  </div>
               </div>
                  <canvas height="60" class="analytics-overview-stats-small-2"></canvas>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
         <div class="stats-small card card-small">
            <div class="card-body px-0 pb-0">
               <div class="d-flex px-3">
                  <div class="stats-small__data">
                     <span class="stats-small__label mb-1 text-uppercase">Sold products</span>
                     <h6 class="stats-small__value count m-0">21,293</h6>
                  </div>
                  <div class="stats-small__data text-right align-items-center">
                     <span class="stats-small__percentage stats-small__percentage--increase">23.71%</span>
                  </div>
               </div>
               <canvas height="60" class="analytics-overview-stats-small-3"></canvas>
            </div>
         </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
         <div class="stats-small card card-small">
            <div class="card-body px-0 pb-0">
               <div class="d-flex px-3">
                  <div class="stats-small__data">
                     <span class="stats-small__label mb-1 text-uppercase">Products to be validated</span>
                     <h6 class="stats-small__value count m-0">6.43</h6>
                  </div>
                  <div class="stats-small__data text-right align-items-center">
                     <span class="stats-small__percentage stats-small__percentage--decrease">2,71%</span>
                  </div>
               </div>
               <canvas height="60" class="analytics-overview-stats-small-4"></canvas>
            </div>
         </div>
      </div>
   </div>

<!--
   <div class="row">


      <div class="col-lg-4 col-sm-6 mb-4">
         <div class="card card-small">
            <div class="card-header border-bottom">
               <h6 class="m-0">Faltantes de pago</h6>
            </div>
            <div class="card-body p-0">
               <ul class="list-group list-group-small list-group-flush">
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Alexa Cruz</span>
                  <span class="ml-auto text-right text-semibold text-reagent-gray">$500</span>
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Yunuen Rojas</span>
                  <span class="ml-auto text-right text-semibold text-reagent-gray">$500</span>
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Isabel Torres</span>
                  <span class="ml-auto text-right text-semibold text-reagent-gray">$500</span>
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Gilberto Santaella</span>
                  <span class="ml-auto text-right text-semibold text-reagent-gray">$500</span>
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">José Rojas</span>
                  <span class="ml-auto text-right text-semibold text-reagent-gray">$500</span>
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Eva Flores</span>
                  <span class="ml-auto text-right text-semibold text-reagent-gray">$500</span>
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Erika Sanchéz</span>
                  <span class="ml-auto text-right text-semibold text-reagent-gray">$500</span>
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Marcos Almazo</span>
                  <span class="ml-auto text-right text-semibold text-reagent-gray">$500</span>
                </li>
               </ul>
            </div>

         </div>
      </div>


      <div class="col-lg-4 col-sm-6 mb-4">
         <div class="card card-small">
            <div class="card-header border-bottom">
               <h6 class="m-0">Faltantes de aceptar carta de concentimiento</h6>
            </div>
            <div class="card-body p-0">
               <ul class="list-group list-group-small list-group-flush">
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Alexa Cruz</span>
                  
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Yunuen Rojas</span>
                  
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Isabel Torres</span>
                  
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Gilberto Santaella</span>
                  
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">José Rojas</span>
                  
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Eva Flores</span>
                  
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Erika Sanchéz</span>
                  
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Marcos Almazo</span>
                  
                </li>
               </ul>
            </div>

         </div>
      </div>

        
      <div class="col-lg-4 col-sm-6 mb-4">
         <div class="card card-small">
            <div class="card-header border-bottom">
               <h6 class="m-0">Usuarios frecuentes</h6>
            </div>
            <div class="card-body p-0">
               <ul class="list-group list-group-small list-group-flush">
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Alexa Cruz</span>
                  
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Yunuen Rojas</span>
                  
                </li>
                <li class="list-group-item d-flex px-3">
                  <span class="text-semibold text-fiord-blue">Isabel Torres</span>
                  
                </li>

               </ul>
            </div>

         </div>
      </div>
      
   </div>
-->   