
<div class="alert alert-success" id="success-alert">
  <strong id="success-text"></strong>
</div>
<div class="alert alert-danger" id="danger-alert">
  <strong id="danger-text"></strong>
</div>

<?php echo _print_messages(); ?>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure you want to delete this record?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Select one option
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-danger" data-dismiss="modal">Cancel</a>
        <a href="#" class="btn btn-success" id="delete">Yes, delete</a>
      </div>
    </div>
  </div>
</div>


  <div class="row">
    <div class="col">
      
      
      
      <div class="card card-small mb-4">
        
        <div class="card-header border-bottom">
          <h6 class="m-0" id="tableTitle"><?php echo $encabezado ?></h6>
          
          
        </div>
        <div class="">
          <a href="<?php echo site_url("admin/users/add") ?>" class="btn btn-success float-right mb-3 mt-3"><i class="fas fa-plus"></i> Add New User</a>
          
          <table class="" id="genericTable">
            <thead class="bg-light">
              <tr>
                <th scope="col" class="border-0">User Name</th>
                <th scope="col" class="border-0">Email</th>
                <th scope="col" class="border-0">Permissions</th>
                
                <th scope="col" class="border-0" id="noExport"></th>
              </tr>
            </thead>
            <tbody>
              <?php if (is_array($usuarios)): ?>
                <?php foreach ($usuarios as $key => $data): ?>                  
                  <tr>
                    <td><?php echo $data->usuario ?></td>
                    <td><?php echo $data->email ?></td>
                    <td>
                      <?php if (is_null($data->idrol)): ?>
                        Without permissions
                      <?php else: 
                          echo "tiene permisos";
                        ?>  

                      <?php endif; ?>
                        
                      </td>
              
                    <td>
                      <a href='<?php echo site_url("admin/users/edit/$data->id") ?>' class="btn btn-warning text-white mt-1 mb-1">Edit</a>
                      <a href="javascript:void(0);" class="btn btn-danger mt-1 mb-1 eliminar"  id="<?php echo $data->id ?>">Delete</a>
                    </td>                
                  </tr>
                <?php endforeach ?>
              <?php endif ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


<script>
  
  window.onload = function(){
      if (window.jQuery) {
          $("#success-alert").hide();
          $("#danger-alert").hide();


          $("#delete-modal").hide();
          
          $('.eliminar').click(function eliminar(event){
              $('.modal-body').load('/render/62805',function(result){
                $('#exampleModalCenter').modal({show:true});
                $('#delete').attr("href", "<?php echo site_url('admin/users/delete/') ?>"+event.target.id);
              });
          });

          $("#success").fadeTo(2000, 500).slideUp(500, function() {
            $("#success").slideUp(500);
          });

          $("#error").fadeTo(2000, 500).slideUp(500, function() {
            $("#error").slideUp(500);
          });


      }
  };

</script>
