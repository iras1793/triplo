
<?php echo _print_messages(); ?>

                      
  <div class="row">
    <div class="col">
      <a href="<?php echo site_url("admin/products") ?>" class="btn btn-danger mb-3 mt-3"><i class="glyphicon glyphicon-arrow-left"></i> Go Back</a>
      
      
        
                  
      <div class="card card-small mb-4">
        <div class="card-header border-bottom">
          <h6 class="m-0" id="tableTitle"><?php echo $encabezado ?></h6>
          
          
        </div>

        <div class="">
          
          
          <table class="" id="genericTable">
            <thead class="bg-light">
              <tr>
                <th scope="col" class="border-0">#</th>
                <th scope="col" class="border-0">Name</th>
                <th scope="col" class="border-0">Description</th>
                <th scope="col" class="border-0">Claim by</th>
                <th scope="col" class="border-0">Reason</th>
                
                <!-- <th scope="col" class="border-0" id="noExport"></th> -->
              </tr>
            </thead>
            <tbody>
              <?php if (is_array($productos)): ?>
                <?php $i=1; ?>
                <?php foreach ($productos as $key => $data): ?>                  
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $data->nombre ?></td>
                    <td><?php echo $data->descripcion ?></td>
                    <td>
                            User
                        
                    </td>
                    <td>por definir</td>
                  </tr>
                  <?php $i++; ?>
                <?php endforeach ?>
              <?php endif ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


<script>
/* 
  window.onload = function(){
      if (window.jQuery) {
          $("#success-alert").hide();



      }
  };
*/
</script>
