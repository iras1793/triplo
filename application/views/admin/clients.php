
<div class="alert alert-success" id="success-alert">
  <strong id="success-text"></strong>
</div>
<div class="alert alert-danger" id="danger-alert">
  <strong id="danger-text"></strong>
</div>

<?php echo _print_messages(); ?>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure you want to delete this record?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Select one option
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-danger" data-dismiss="modal">Cancel</a>
        <a href="#" class="btn btn-success" id="delete">Yes, delete</a>
      </div>
    </div>
  </div>
</div>


  <div class="row">
    <div class="col">
      
      
      
      <div class="card card-small mb-4">
        
        <div class="card-header border-bottom">
          <h6 class="m-0" id="tableTitle"><?php echo $encabezado ?></h6>
          
          
        </div>
        <div class="">
          
          
          <table class="" id="genericTable">
            <thead class="bg-light">
              <tr>
                <th scope="col" class="border-0">#</th>
                <th scope="col" class="border-0">Name</th>
                <th scope="col" class="border-0">Email</th>
                <th scope="col" class="border-0">Country</th>
                
                <th scope="col" class="border-0" id="noExport"></th>
              </tr>
            </thead>
            <tbody>
              <?php if (is_array($clientes)): ?>
                <?php $i=1; ?>
                <?php foreach ($clientes as $key => $data): ?>                  
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $data->nombre_cliente ?></td>
                    <td><?php echo $data->email ?></td>
                    <td><?php echo $data->nombre_pais ?></td>
                    
                    
                    <td>
                      <?php if ($data->es_activo=='0'): ?>
                          <a href="javascript:void(0);" class="btn btn-primary text-white mt-1 mb-1 habilitar" id="<?php echo "habilitar-".$data->idcliente ?>">Acept</a>
                          <a href="javascript:void(0);" class="btn btn-warning mt-1 mb-1 deshabilitar" id="<?php echo "deshabilitar-".$data->idcliente ?>">Deny</a>
                      <?php endif ?>
                      

                      <a href="javascript:void(0);" class="btn btn-danger mt-1 mb-1 "  id="<?php echo $data->id ?>">Delete</a>
                      
                    </td>                
                  </tr>
                  <?php $i++; ?>
                <?php endforeach ?>
              <?php endif ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


<script>
  
  window.onload = function(){
      if (window.jQuery) {
          $("#success-alert").hide();
          $("#danger-alert").hide();
          $(".habilitar").click(function showAlert(event) {
              
              $.ajax({
                  url : "<?php echo site_url('admin/clients/status/') ?>",
                  data : { estado : 1 , registro:event.target.id},
                  type : 'POST',
                  dataType : 'json',
                  success : function(json) {
                      $("#success-text").text(json.message);
                      $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                        $("#success-alert").slideUp(500);
                      });

                      var element = event.target.id;
                      var stract = element.split("-");
                      $("#habilitar-"+stract[1]).hide();
                      $("#deshabilitar-"+stract[1]).hide();
                      

                  },
                  error : function(xhr, status) {
                    $("#danger-text").text('Error, please try it later.');
                      $("#danger-alert").fadeTo(2000, 500).slideUp(500, function() {
                        $("#danger-alert").slideUp(500);
                        console.log(status);
                      });
                  }
              });

          });


          $(".deshabilitar").click(function showAlert(event) {
              
              $.ajax({
                  url : "<?php echo site_url('admin/clients/status/') ?>",
                  data : { estado : -1 , registro:event.target.id},
                  type : 'POST',
                  dataType : 'json',
                  success : function(json) {
                      $("#success-text").text(json.message);
                      $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                        $("#success-alert").slideUp(500);
                      });

                      var element = event.target.id;
                      var stract = element.split("-");
                      $("#habilitar-"+stract[1]).hide();
                      $("#deshabilitar-"+stract[1]).hide();

                  },
                  error : function(xhr, status) {
                    $("#danger-text").text('Error, please try it later.');
                      $("#danger-alert").fadeTo(2000, 500).slideUp(500, function() {
                        $("#danger-alert").slideUp(500);
                        console.log(status);
                      });
                  }
              });

          });

          $("#delete-modal").hide();
          
          $('.eliminar').click(function eliminar(event){
              $('.modal-body').load('/render/62805',function(result){
                $('#exampleModalCenter').modal({show:true});
                $('#delete').attr("href", "<?php echo site_url('admin/clients/delete/') ?>"+event.target.id);
              });
          });

          $("#success").fadeTo(2000, 500).slideUp(500, function() {
            $("#success").slideUp(500);
          });

          $("#error").fadeTo(2000, 500).slideUp(500, function() {
            $("#error").slideUp(500);
          });


      }
  };

</script>
