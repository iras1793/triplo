<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="es">
   <head>
      <!-- Required meta tags-->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="triplo">
      <meta name="author" content="Iras Dargor">
      <meta name="keywords" content="Iras Dargor development">
      <!-- <link rel="icon" type="image/png" href="<?php echo base_url('assets/admin/images/icon/logo-mini.png') ?>" /> -->
      <!-- Title Page-->
      <title><?php echo $title; ?></title>
      <!-- Fontfaces CSS-->
      <link href="<?php echo base_url('assets/admin/css/font-face.css') ?>" rel="stylesheet" media="all">
      <link href="<?php echo base_url('assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css') ?>" rel="stylesheet" media="all">
      <link href="<?php echo base_url('assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css') ?>" rel="stylesheet" media="all">
      <link href="<?php echo base_url('assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css') ?>" rel="stylesheet" media="all">
      <!-- Bootstrap CSS-->
      <link href="<?php echo base_url('assets/admin/vendor/bootstrap-4.1/bootstrap.min.css') ?>" rel="stylesheet" media="all">

      <style>
         body{background: url("<?php echo base_url('assets/admin/images/login-back.jpg') ?>") no-repeat center center fixed;background-size: cover;}
         .main-section{margin: 0 auto;margin-top: 20%;padding: 0;}
         .modal-content{background-color: #3b4652;opacity: 0.95;padding: 0 18px;box-shadow: 0px 0px 3px #848484;}
         .user-img{margin-top: -17px;margin-bottom: -2px;}
         .user-img img {height: 150px;width: 200px;border-radius: 5px;}
         .form-group{margin-bottom: 25px;}
         .form-group input{height: 42px;border-radius: 5px;border:0;font-size: 18px;padding-left: 54px;}
         .form-group::before{font-family: 'Font Awesome\ 5 Free';content:"\f007";position: absolute;font-size: 22px;color: #555e60;left: 28px;padding-top: 4px;}
         .form-group:last-of-type::before{content: url("<?php echo base_url('assets/admin/images/icon/locked.png') ?>");position: absolute;font-size: 22px;color: #555e60;left: 25px;padding-top: 8px;}
         .my-btn{padding: 7px 14px;margin-bottom: 20px;font-size: 18px;}
         .forgot{padding: 10px 0 25px;margin-top: 10px;}
         #email-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
         #pwd-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
      </style>

   </head>
   <body>

      <div class="modal-dialog text-center">
         <div class="col-sm-12 main-section">
            <div class="modal-content">


               <div class="col-12 user-img">
                  <img class="img-fluid" src="<?php echo base_url('assets/img/triplo_logo.svg') ?>" style="max-width: 85px;" alt="bodies-logo">
               </div>
               <!-- <form class="col-12" action="" id="form-validate"> -->
               <?php echo form_open('admin/login/index',array('id'=>'form-validate','class'=>'col-12 ')) ?>
                  <div class="alert alert-primary">
                     <p>Welcome to Triplo's Admin</p>
                     <?php echo _print_messages(); ?>
                  </div>

                  <div class="form-group">

                     <input type="text" class="form-control" name="email" placeholder="Email">
                  </div>
                  <div class="form-group">

                     <input type="password" class="form-control" name="pwd" placeholder="Password">
                  </div>
                  <br>
                  <!-- <button type="submit" class="btn btn-info my-btn"> <i class="fas fa-sign-in-alt"></i> Ingresar </button> -->
                  <input type="submit" class="btn btn-info my-btn" value="Ingresar">
               </form>

            </div><!-- end modal content -->
         </div>
      </div>

      <!-- Jquery JS-->
      <script src="<?php echo base_url('assets/admin/vendor/jquery-3.2.1.min.js') ?>"></script>
      <!-- Bootstrap JS-->
      <script src="<?php echo base_url('assets/admin/vendor/bootstrap-4.1/popper.min.js') ?>"></script>
      <script src="<?php echo base_url('assets/admin/vendor/bootstrap-4.1/bootstrap.min.js') ?>"></script>
      <script src="<?php echo base_url('assets/admin/js/jquery.validate.js') ?>"></script>

      <script type="text/javascript">

         window.onload = function(){
            if (window.jQuery) {
               $("#form-validate").validate({
                     rules: {
                        email: {
                           required : true,
                           minlength: 3,
                           maxlength: 30
                        },
                        pwd: {
                           required : true,
                           minlength: 3,
                           maxlength: 30
                        }
                     },
                     messages:{
                        email:{
                           required: "Email required",
                           minlength: "Type atleast 3 characters",
                           maxlength: "No more than 30 characters"
                        },
                        pwd:{
                           required: "Password required",
                           minlength: "Type atleast 3 characters",
                           maxlength: "No more than 30 characters"
                        }
                     }
               });

            }//end if
         }//end window onload

      </script>

   </body>
</html>
