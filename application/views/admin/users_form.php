<style>
  .error{height: 30px;}
  label[class='error']{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
</style>

    <div class="row">
        <div class="col-lg-10 mx-auto mt-4">
        <?php echo _print_messages(); ?>
          <a href='<?php echo site_url("admin/users") ?>' class="btn btn-danger mb-3"><i class="glyphicon glyphicon-arrow-left"></i> Go Back</a>
          
          <div class="card card-small edit-user-details mb-4">
            <div class="card-body p-0">
              <div class="border-bottom clearfix d-flex">
                <ul class="nav nav-tabs border-0 mt-auto mx-4 pt-2">
                  <li class="nav-item">
                    <a class="nav-link active" href="#">General Data</a>
                  </li>
                </ul>
              </div>
              <?php echo form_open_multipart("admin/users/$action/$id", array('class'=>'py-4', 'id'=>'form-validate')); ?>
                <div class="form-row mx-4">
                  <div class="col mb-3">
                    <h6 class="form-text m-0">Complete the info for the user</h6>
                  </div>
                </div>
                <div class="form-row mx-4">
                  <div class="col-lg-12">
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="user">User Name</label>
                        <input type="text" name="usuario" class="form-control" id="user" value="<?php echo $fields['usuario']['value'] ?>">
                      </div>
                      
                      <div class="form-group col-md-4">
                        <label for="email">Email</label>
                        <input type="text" name="email" class="form-control" id="email" value="<?php echo $fields['email']['value'] ?>">
                      </div>


                      <div class="form-group col-md-4">
                        <label for="displayEmail">Permissions</label>
                        <select class="custom-select" name="idrol" id="mirol">
                          <option value="">Select Permission</option>
                          <?php foreach ($roles as $key => $data): ?>

                              <?php if ($fields['idrol']['value'] == $data->id): ?>
                                  <option value="<?php echo $fields['idrol']['value'] ?>" selected><?php echo $data->nombre ?></option>
                              <?php else: ?>
                                  <option value="<?php echo $data->id ?>"><?php echo $data->nombre ?></option>
                              <?php endif; ?>
                              
                            
                          <?php endforeach; ?>
                        </select>
                      </div>
      
                      <div class="form-group col-md-4" id="oculto" style="display: none;">
                        <label for="email">Email 2</label>
                        <input type="text" name="email" class="form-control" id="email" value="<?php echo $fields['email']['value'] ?>">
                      </div>

                    <?php if ($action=='add'): ?>

                      <div class="form-group col-md-6">
                        <label for="pwd">Password</label>
                        <input type="password" name="pwd" class="form-control" id="pwd" value="<?php echo $fields['pwd']['value'] ?>">
                      </div>
                      
                    <?php endif; ?>

     
                      
                    </div>
                  </div>

                </div>
                
              <?php if ($action=='edit'): ?>
                
              
                  <hr>
                  
                  <div class="form-row mx-4">
                    <div class="col mb-3">
                      <h6 class="form-text m-0">Change password</h6>
                    </div>
                  </div>
                  <div class="form-row mx-4">
                    <div class="form-group col-md-4">
                      <label for="">New Password</label>
                      <input type="password" name="nueva_pwd" class="form-control" id="new">
                    </div>

                  </div>
              <?php endif ?>
            </div>
            <div class="card-footer border-top">
              <input type="submit" class="btn btn-accent ml-auto d-table mr-3" value="Save">
            </div>
            </form>
          </div>
          <!-- End Edit User Details Card -->
        </div>
    </div> 

<script>
  
         window.onload = function(){
            if (window.jQuery) {
               $("#form-validate").validate({
                     rules: {
                        usuario: {
                           required : true,
                           minlength: 3,
                           maxlength: 100
                        },
                        email: {
                           required : true,
                           minlength: 3,
                           maxlength: 100
                        },
                        idrol: {
                           required : true,
                           minlength: 1,
                           maxlength: 1
                        },
                        pwd: {
                           required : true,
                           minlength: 3,
                           maxlength: 100
                        },
                        nueva_pwd: {
                           maxlength: 100
                        }
                     }
               });

                $("#success").fadeTo(2000, 500).slideUp(500, function() {
                  $("#success").slideUp(500);
                });

                $("#error").fadeTo(2000, 500).slideUp(500, function() {
                  $("#error").slideUp(500);
                });
                
                $("#mirol").change(function(){
                  $("#oculto").show();
                }); 

            }//end if



         }//end window onload

</script>