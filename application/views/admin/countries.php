            <div class="row mb-2">
                <br>
              <div class="col-12">
                <span style="font-size: 16px;" class="d-block mb-2 text-muted"><strong>Select a continent</strong></span>
              </div>
                <br><br>
              <div class="col mb-4" style="margin-top:2rem!important;">
                <a href='<?php echo site_url("admin/countries/country_list/america")?>' class="bg-info rounded text-black text-center" style="box-shadow: inset 0 0 5px rgba(0,0,0,.2); text-decoration:none; padding:3rem!important">
                    <img src="<?php echo base_url('assets/admin/images/icon/america.png') ?>" style="max-width:30%; height:auto;">
                    
                </a>
              </div>
              
              <div class="col mb-4" style="margin-top:2rem!important;">
                <a href='<?php echo site_url("admin/countries/country_list/europe")?>' class="bg-info rounded text-black text-center" style="box-shadow: inset 0 0 5px rgba(0,0,0,.2); text-decoration:none; padding:3rem!important">
                    <img src="<?php echo base_url('assets/admin/images/icon/europe.png') ?>" style="max-width:30%; height:auto;">
                    
                </a>
              </div>
              
              <div class="col mb-4" style="margin-top:2rem!important;">
                <a href='<?php echo site_url("admin/countries/country_list/africa")?>' class="bg-info rounded text-black text-center" style="box-shadow: inset 0 0 5px rgba(0,0,0,.2); text-decoration:none; padding:3rem!important">
                    <img src="<?php echo base_url('assets/admin/images/icon/africa.png') ?>" style="max-width:30%; height:auto;">
                    
                </a>
              </div>
              
              <div class="col mb-4" style="margin-top:2rem!important;">
                <a href='<?php echo site_url("admin/countries/country_list/australia")?>' class="bg-info rounded text-black text-center" style="box-shadow: inset 0 0 5px rgba(0,0,0,.2); text-decoration:none; padding:3rem!important">
                    <img src="<?php echo base_url('assets/admin/images/icon/australia.png') ?>" style="max-width:30%; height:auto;">
                    
                </a>
              </div>              
              
              <div class="col mb-4" style="margin-top:2rem!important;">
                <a href='<?php echo site_url("admin/countries/country_list/asia")?>' class="bg-info rounded text-black text-center" style="box-shadow: inset 0 0 5px rgba(0,0,0,.2); text-decoration:none; padding:3rem!important">
                    <img src="<?php echo base_url('assets/admin/images/icon/continent-icon-9.jpg') ?>" style="max-width:30%; height:auto;">
                    
                </a>
              </div>              
              
             
            </div>
            