<style>
  .error{height: 30px;}
  label[class='error']{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  div[class='error']{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;margin:10px;}
</style>

    <div class="row">
        <div class="col-lg-10 mx-auto mt-4">
          <a href='<?php echo site_url("admin/countries/country_list/$continente") ?>' class="btn btn-danger mb-3"><i class="glyphicon glyphicon-arrow-left"></i> Go Back</a>
          <?php echo _print_messages(); ?>
          
          <div class="card card-small edit-user-details mb-4">
            <div class="card-body p-0">
              <div class="border-bottom clearfix d-flex">
                <ul class="nav nav-tabs border-0 mt-auto mx-4 pt-2">
                  <li class="nav-item">
                    <a class="nav-link active" href="#">General Data</a>
                  </li>
                </ul>
              </div>
              <?php echo form_open_multipart("admin/countries/$action/$continente/$id", array('class'=>'py-4', 'id'=>'form-validate')); ?>
                <div class="form-row mx-4">
                  <div class="col mb-3">
                    <h6 class="form-text m-0">Data </h6>
                  </div>
                </div>
                <div class="form-row mx-4">
                  <div class="col-lg-12">
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="countryName">Country Name</label>
                        <input type="text" name="nombre" class="form-control" id="countryName" value="<?php echo $fields['nombre']['value'] ?>">
                      </div>

                      <div class="form-group col-md-4">
                        <label for="capitalName">Capital</label>
                        <input type="text" name="capital" class="form-control" id="capitalName" value="<?php echo $fields['capital']['value'] ?>">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="continentName">Continent</label>
                        <input type="text" name="idcontinente" class="form-control" id="continentName" value="<?php echo $continente; ?>" readonly>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-12 mt-5" >


                      <div class="form-group col-md-6">
                          <div class="custom-control custom-toggle custom-toggle-sm mb-1">
                            <?php if ($fields['es_top']['value']=='1'): ?>
                                <input type="checkbox" id="customToggle2" name="es_top" class="custom-control-input" checked="checked">
                            <?php else: ?>
                                <input type="checkbox" id="customToggle2" name="es_top" class="custom-control-input">
                            <?php endif ?>
                            
                            <label class="custom-control-label" for="customToggle2">Is top country?</label>
                          </div>
                      </div>

                    <label for="userProfilePicture" class="text-center w-100 mb-6">Flag</label>
                    <div class="edit-user-details__avatar m-auto">
                      <?php $imagen = base_url("assets/paises/".$fields['imagen_bandera']['value']); ?>
                      <?php if ($fields['imagen_bandera']['value']!=''): ?>
                          <img src='<?php echo $imagen ?>' alt="Avatar">
                      <?php else: ?>
                          <img src='<?php echo base_url("assets/img/icon.png") ?>' alt="Avatar">  
                      <?php endif ?>
                      
                      <label class="edit-user-details__avatar__change"></label>
                    </div>
                    <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_bandera" accept="image/*" id="userProfilePicture" class="d-none">
                  </div>
                </div>

            </div>
            <div class="card-footer border-top">
              <input type="submit" class="btn btn-accent ml-auto d-table mr-3" value="Save">
            </div>
            </form>
          </div>
          <!-- End Edit User Details Card -->
        </div>
    </div> 

<script>
  
         window.onload = function(){
            if (window.jQuery) {
               $("#form-validate").validate({
                     rules: {
                        nombre: {
                           required : true,
                           minlength: 3,
                           maxlength: 50
                        },
                        capital: {
                           required : true,
                           minlength: 3,
                           maxlength: 50
                        },
                        idcontinente: {
                           required : true
                        },
                        imagen_bandera: {
                           accept: "image/*"
                        }

                     }
               });

                $("#success").fadeTo(2000, 500).slideUp(500, function() {
                  $("#success").slideUp(500);
                });

                $("#error").fadeTo(2000, 500).slideUp(500, function() {
                  $("#error").slideUp(500);
                });
                
            }//end if
         }//end window onload

</script>