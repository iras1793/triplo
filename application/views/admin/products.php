
<div class="alert alert-success" id="success-alert">
  <strong id="success-text"></strong>
</div>
<div class="alert alert-danger" id="danger-alert">
  <strong id="danger-text"></strong>
</div>

<?php echo _print_messages(); ?>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure you want to delete this record?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Select one option
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-danger" data-dismiss="modal">Cancel</a>
        <a href="#" class="btn btn-success" id="delete">Yes, delete</a>
      </div>
    </div>
  </div>
</div>


        
          
        
  <div class="row">
      
    <div class="col">
      
      
        
      <div class="card card-small mb-4">
        <div class="card-header border-bottom">
          <h6 class="m-0" id="tableTitle"><?php echo $encabezado ?></h6>
          
          
        </div>
        <div class="">
          
          <a href="<?php echo site_url("admin/products/purchased") ?>" class="btn btn-info mb-3 mt-3 mr-3 float-right" style="font-size: 15px;"> <i class="glyphicon glyphicon-gift"></i> Products purchased by clients</a>
          <a href="<?php echo site_url("admin/products/top_products") ?>" class="btn btn-success mb-3 mt-3 mr-3 float-right" style="font-size: 15px;"> <i class="glyphicon glyphicon-star"></i> Top products</a>
          <a href="<?php echo site_url("admin/products/claims") ?>" class="btn mb-3 mt-3 mr-3 float-right" style="font-size: 15px;background-color: #FFA033; color:#FFF"> <i class="glyphicon glyphicon-exclamation-sign"></i> Claims!</a>
          
          <table class="" id="genericTable">
            <thead class="bg-light">
              <tr>
                <th scope="col" class="border-0">#</th>
                <th scope="col" class="border-0">Name</th>
                <th scope="col" class="border-0">Description</th>
                <th scope="col" class="border-0">Quantity</th>
                <th scope="col" class="border-0">Price</th>
                <th scope="col" class="border-0">General Rating</th>
                <th scope="col" class="border-0">Status</th>
                <th scope="col" class="border-0">Uploaded by</th>
                
                <th scope="col" class="border-0" id="noExport"></th>
              </tr>
            </thead>
            <tbody>
              <?php if (is_array($productos)): ?>
                <?php $i=1; ?>
                <?php foreach ($productos as $key => $data): ?>                  
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $data->nombre ?></td>
                    <td><?php echo $data->descripcion ?></td>
                    <td><?php echo $data->cantidad ?></td>
                    <td><?php echo "$".$data->precio." USD" ?></td>
                    <td>
                        <?php if ($data->rating_general=='5'): ?>
                            <span class="badge badge-pill badge-success" style="font-size: 12px;"><?php echo $data->rating_general.' stars' ?></span>
                        <?php elseif($data->rating_general < '5' && $data->rating_general >='3'): ?>  
                            <span class="badge badge-pill badge-warning" style="font-size: 12px;"><?php echo $data->rating_general.' stars' ?></span>
                        <?php else: ?>
                            <span class="badge badge-pill badge-danger" style="font-size: 12px;"><?php echo $data->rating_general.' stars' ?></span>
                        <?php endif ?>
                    </td>
                    
                      <?php if(is_null($data->idusuario)): ?>
                      <td> 
                         <span class="badge badge-pill badge-warning"> <p id="<?php echo "parrafo-".$data->idproducto ?>"  style="font-size: 12px;">Waiting to be validated</p> </span> 
                      </td>  
                      <?php elseif($data->autorizado=='-1'): ?>
                      <td> 
                          <p id="<?php echo "parrafo-".$data->idproducto ?>" class="badge badge-pill badge-danger mt-4"  style="font-size: 12px;">Declined</p>
                        </td>  
                      <?php else: ?>
                      <td> <p id="<?php echo "parrafo-".$data->idproducto ?>" class="badge badge-pill badge-success mt-4" style="font-size: 12px;">Validated</p> </td>
                      <?php endif; ?>
                    
                    <td>
                      <?php echo $data->nombre_cliente ?>
                    </td>
                    
                    <td>
                      <?php if (is_null($data->idusuario)): ?>
                        <a href="javascript:void(0);" class="btn btn-primary text-white mt-1 mb-1 habilitar" id="<?php echo "habilitar-".$data->idproducto ?>">Acept</a>
                        <a href="javascript:void(0);" class="btn btn-warning mt-1 mb-1 deshabilitar" id="<?php echo "deshabilitar-".$data->idproducto ?>">Decline</a>
                      <?php endif ?>
                      <a href='<?php echo site_url("admin/products/comments/$data->idproducto") ?>' class="btn btn-royal-blue mt-1 mb-1" id="<?php echo $data->idproducto ?>">Comments</a>
                      <a href="javascript:void(0);" class="btn btn-danger mt-1 mb-1 eliminar"  id="<?php echo $data->idproducto ?>">Delete</a>
                      <!-- data-toggle="modal" data-target="#exampleModalCenter" -->
                    </td>                
                  </tr>
                  <?php $i++; ?>
                <?php endforeach ?>
              <?php endif ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


<script>
  
  window.onload = function(){
      if (window.jQuery) {
          $("#success-alert").hide();
          $("#danger-alert").hide();
          $(".habilitar").click(function showAlert(event) {
              
              $.ajax({
                  url : "<?php echo site_url('admin/products/status/') ?>",
                  data : { estado : 1 , registro:event.target.id},
                  type : 'POST',
                  dataType : 'json',
                  success : function(json) {
                      $("#success-text").text(json.message);
                      $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                        $("#success-alert").slideUp(500);
                      });
                      var element = event.target.id;
                      var stract = element.split("-");
                      $("#habilitar-"+stract[1]).hide();
                      $("#deshabilitar-"+stract[1]).hide();
                      $("#parrafo-"+stract[1]).text("Validated");
                      
                      
                  },
                  error : function(xhr, status) {
                    $("#danger-text").text('Error, please try it later.');
                      $("#danger-alert").fadeTo(2000, 500).slideUp(500, function() {
                        $("#danger-alert").slideUp(500);
                        console.log(status);
                      });
                  }
              });

          });


          $(".deshabilitar").click(function showAlert(event) {
              
              $.ajax({
                  url : "<?php echo site_url('admin/products/status/') ?>",
                  data : { estado : -1 , registro:event.target.id},
                  type : 'POST',
                  dataType : 'json',
                  success : function(json) {
                      $("#success-text").text(json.message);
                      $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                        $("#success-alert").slideUp(1000);
                      });
                      var element = event.target.id;
                      var stract = element.split("-");
                      $("#habilitar-"+stract[1]).hide();
                      $("#deshabilitar-"+stract[1]).hide();
                      $("#parrafo-"+stract[1]).text("Declined");
                      
                  },
                  error : function(xhr, status) {
                    $("#danger-text").text('Error, please try it later.');
                      $("#danger-alert").fadeTo(2000, 500).slideUp(500, function() {
                        $("#danger-alert").slideUp(500);
                        console.log(status);
                      });
                  }
              });

          });

          $("#delete-modal").hide();
          
          $('.eliminar').click(function eliminar(event){
              $('.modal-body').load('/render/62805',function(result){
                $('#exampleModalCenter').modal({show:true});
                $('#delete').attr("href", "<?php echo site_url('admin/products/delete/') ?>"+event.target.id);
              });
          });

          $("#success").fadeTo(2000, 500).slideUp(500, function() {
            $("#success").slideUp(500);
          });

          $("#error").fadeTo(2000, 500).slideUp(500, function() {
            $("#error").slideUp(500);
          });


      }
  };

</script>
