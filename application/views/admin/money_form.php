<style>
  .error{height: 30px;}
  label[class='error']{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  div[class='error']{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;margin:10px;}
</style>

    <div class="row">
        <div class="col-lg-10 mx-auto mt-4">
          <a href='<?php echo site_url("admin/moneyconverter") ?>' class="btn btn-danger mb-3"><i class="glyphicon glyphicon-arrow-left"></i> Go Back</a>
          <?php echo _print_messages(); ?>
          
          <div class="card card-small edit-user-details mb-4">
            <div class="card-body p-0">
              <div class="border-bottom clearfix d-flex">
                <ul class="nav nav-tabs border-0 mt-auto mx-4 pt-2">
                  <li class="nav-item">
                    <a class="nav-link active" href="#">General Data</a>
                  </li>
                </ul>
              </div>
              <?php echo form_open_multipart("admin/moneyconverter/$action/$id", array('class'=>'py-4', 'id'=>'form-validate')); ?>
                <div class="form-row mx-4">
                  <div class="col mb-3">
                    <h6 class="form-text m-0">Data </h6>
                  </div>
                </div>
                <div class="form-row mx-4">


                  <div class="col-lg-12 mt-5" >

                      <div class="form-row">
                                <div class="form-group col-md-4">
                                  <label for="currenceName">Currency Name</label>
                                  <input type="text" name="nombre" class="form-control" id="currenceName" value="<?php echo $fields['nombre']['value'] ?>">
                                </div>

                                <div class="form-group col-md-4">
                                  <label for="tipoName">Type of Currency</label>
                                  <input type="text" name="tipo" class="form-control" id="tipoName" value="<?php echo $fields['tipo']['value'] ?>">
                                </div>

                                <div class="form-group col-md-4">
                                  <label for="valorName">Value of Currency</label>
                                  <input type="text" name="valor" class="form-control" id="valorName" value="<?php echo $fields['valor']['value']; ?>" placeholder="Only Numbers: 12.01">
                                </div>

                                <label for="userProfilePicture" class="text-center w-100 mb-4">Flag Currency</label>
                                <div class="edit-user-details__avatar m-auto">
                                  <?php $imagen = base_url("assets/monedas/".$fields['imagen_pais']['value']); ?>
                                  <?php if ($fields['imagen_pais']['value']!=''): ?>
                                      <img src='<?php echo $imagen ?>' alt="Avatar">
                                  <?php else: ?>
                                      <img src='<?php echo base_url("assets/img/icon.png") ?>' alt="Avatar">  
                                  <?php endif ?>
                                  
                                  <label class="edit-user-details__avatar__change"></label>
                                </div>
                                <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_moneda" accept="image/*" id="userProfilePicture" class="d-none">

                                <label for="userProfilePicture" class="text-center w-100 mb-4 mt-4">Country Currency</label>
                                <div class="edit-user-details__avatar m-auto">
                                  <?php $imagen = base_url("assets/monedas/".$fields['imagen_moneda']['value']); ?>
                                  <?php if ($fields['imagen_moneda']['value']!=''): ?>
                                      <img src='<?php echo $imagen ?>' alt="Avatar">
                                  <?php else: ?>
                                      <img src='<?php echo base_url("assets/img/icon.png") ?>' alt="Avatar">  
                                  <?php endif ?>
                                  
                                  <label class="edit-user-details__avatar__change"></label>
                                </div>
                                <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_pais" accept="image/*" id="userProfilePicture" class="d-none">

                      </div>

                  </div>
                </div>

            </div>
            <div class="card-footer border-top">
              <input type="submit" class="btn btn-accent ml-auto d-table mr-3" value="Save">
            </div>
            </form>
          </div>
          <!-- End Edit User Details Card -->
        </div>
    </div> 

<script>
  
         window.onload = function(){
            if (window.jQuery) {
               $("#form-validate").validate({
                     rules: {
                        nombre: {
                           required : true,
                           minlength: 3,
                           maxlength: 50
                        },
                        tipo: {
                           required : true,
                           minlength: 1,
                           maxlength: 10
                        },
                        valor: {
                           required : true
                        },
                        imagen_moneda: {
                           accept: "image/*"
                        },
                        imagen_pais: {
                           accept: "image/*"
                        }

                     }
               });

                $("#success").fadeTo(2000, 500).slideUp(500, function() {
                  $("#success").slideUp(500);
                });

                $("#error").fadeTo(2000, 500).slideUp(500, function() {
                  $("#error").slideUp(500);
                });
                
            }//end if
         }//end window onload

</script>