<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->
	<!-- <p class="b-brand">Plan Journey</p> -->

	<section class="bgslider interiorsections" id="sectionTopActivities">

			<div class="sectionContent">
				<p class="subSectionName"> Top Activities </p> 

				<ul class="item-grid">
  
					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/beach.jpg') ?>" alt="icono go" class="image">
					    </div>
					    <p class="activityName"> Swimming </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.
						      	Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>

					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/diving.jpg') ?>" alt="icono go" class="image">
					    </div>
					    <p class="activityName"> Diving </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.
						      	Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>

					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/rowing.jpg') ?>" alt="icono go" class="image">
					    </div>
					    <p class="activityName"> Rowing </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.
						      	Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>

					  <li class="item">
					    <div class="item-image">
					    	<img src="<?php echo base_url('assets/img/activities/senderism.jpg') ?>" alt="icono go" class="image">
					    </div>
					    <p class="activityName"> Senderism </p>
					      <div class="InfoActivity">
						      	<p class="DescActivity">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.
						      	Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper.</p>
					      </div>
					  </li>
				  
					 

				</ul> <!-- item grid ends -->


			</div>
	</section>


</main>
