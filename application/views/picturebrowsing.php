<?php echo $header; ?>

<main>

	<?php echo $menu_lateral; ?>
	
	<!-- Burger-Brand -->
	<p class="b-brand">Picture Browsing</p>

	<section class="bgslider interiorsections" id="sectionPictureBrowsing">

		<h2 class="instruction">Start by selecting a continent</h2>
		<img src="<?php echo base_url('assets/img/iconospng/arrow2.png') ?>" alt="icono flecha" class="arrowpb">
		<div class="select-checkbox" id="checkpicturerandom">
			<input type="checkbox" id="picturesrandom" name="picbrowsing">
			<label for="picturesrandom" data-content="picturesrandom">Show pictures randomly</label>
	  	</div>
		
		<div class="select-checkbox" id="checkpicturecountry">
			<input type="checkbox" id="picturescountry" name="picbrowsing">
			<label for="picturescountry" data-content="picturescountry">Show pictures from a country of your selection</label>
		</div>

		<a href="<?php echo site_url('travel/picturebrowsing_go') ?>"> Go 
			<img src="<?php echo base_url('assets/img/iconospng/go.png') ?>" alt="icono go" class="go_icon" loading="lazy">
		</a>

		<?php echo $mapa_giratorio; ?>
		
	</section>
	
</main>
