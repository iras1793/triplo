<?php echo $header; ?>

<main>

	<?php echo $menu_lateral; ?>

	<!-- Burger-Brand -->
	<p class="b-brand">Start - Finish</p>

	<section class="bgslider interiorsections" id="sectionStartFinish">
 		 	<div class="sectionContent">
			  <div class="form__field">
					<input type="text" placeholder="Name of Journey" id="tripname">   
		 	 </div>
		 	 <div class="container">
				<div class="spot">
					<p class="title">Start</p>
					<p class="startcountry titlecountry">London</p>
			  		<input type="checkbox" class="hotspots" id="hotspot1" name="hotspot">				
					  <label for="hotspot1">
					    <div id="hs1" class="buttonspot"></div>
					  </label>
			  	</div>
			  
			  	  <hr> 

				  <div class="spot">
						<p class="title">Finish</p>
						<p class="finishcountry titlecountry">Paris</p>
				  		<input type="checkbox" class="hotspots" id="hotspot2" name="hotspot">
						  <label for="hotspot2">
						    <div id="hs2" class="buttonspot"></div>
						  </label>
				  </div>
			  

			  <div id="contentspot1" class="contentspot">
			  		<div class="content">
				    	<input type="text" class="selectionC" name="typecountry" placeholder="Type Location">
				    	<a href="#" class="btnselectmap">Select on map</a>
				    	<span class="close-spot">X</span>
				    </div>
			  </div>
			  <div id="contentspot2" class="contentspot">
			  		<div class="content">
				   		<input type="text" class="selectionC" name="typecountry" placeholder="Type Location">
				    	<a href="#" class="btnselectmap">Select on map</a>
				    	<span class="close-spot">X</span>
				    </div>
			  </div>

			</div><!-- end container -->
			 <div id="contentspotFinish" class="spotTrip">
			   		<input type="text" placeholder="Birthday trip" id="tripname"> 
			    	<ul>
			    		<li>			    		  
						  <label for="daysT" class="labelspotT">Days of Travel</label>
						  <input type="text" class="daysTravel" id="daysT" name="daysTravel">
			    		</li>
			    		<li>			    		  
						  <label for="stopsT" class="labelspotT">Number of Stops</label>
						  <input type="text" class="daysTravel" id="stopsT" name="stopsTravel">
			    		</li>
			    		<li class="btnselectmap" id="selectStopsSF">Select Stops</a></li>
			    		<li class="select-checkbox">				
						  <input type="checkbox" id="notsure" name="notsure" value="">
						  <label for="notsure" data-content="SelectMore">Not sure</label>

		               </li>
			    	</ul>
			  </div>
			  <div id="contentspot3" class="contentspot">
			  		<div class="content">
				   		<input type="text" class="selectionC" name="typecountry" placeholder="Type Days">
				   		<div class="btnselectmap"><img src="<?php echo base_url('assets/img/iconospng/calendar.png') ?>" alt="iconocalendario">
				   			<input type="text" class="datepicker" name="typecountry" placeholder=" Select from calendar"></div>
				   			<span class="close-spot">X</span>
				    </div>
			  </div>
			  <div id="contentspot4" class="contentspot">
			  		<div class="content">
				   		<a href="<?php echo site_url('travel/picturebrowsing_go') ?>" class="btnselectmap">Picture browsing</a>
				    	<a href="<?php echo site_url('travel/letsplan') ?>" class="btnselectmap">Select on map</a>
				    	<a href="<?php echo site_url('travel/writingthem') ?>" class="btnselectmap">Writing them</a>
				    	<div class="select-checkbox">
					    	<input type="checkbox" id="notsurespots" name="notsure" value="">
						    <label for="notsurespots" data-content="SelectMore">Not sure</label>
						</div>
						<span class="close-spot">X</span>
				    </div>
			  </div>
			</div> <!-- end section content -->

		 <div data-aos="zoom-in" class="mapas" id="SFmap"></div> 
		

		  		

	</section>
	

</main>
