<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->
	<!-- <p class="b-brand">Plan Journey</p> -->

	<section class="bgslider interiorsections" id="sectionMConverter">
		
			<div class="sectionContent">
				<h1 class="SectionName"> Money Converter </h1>
				<ul class="content">
					<li class="list">
						<div class="form__field">
					         <input type="text" placeholder="1">
					    </div>

					    <div class="form__field">
					        <div class="sel sel--box">
					            <select name="select-gender" id="select-gender">
					                <option value="" disabled>US Dollars</option>
					                <option value="mxn">(MXN) Mexican Peso</option>
					            </select>
					        </div>
					    </div>
					</li>
					<li class="list">
						<div class="form__field">
					         <input type="text" placeholder="19.15">
					    </div>

					    <div class="form__field">
					        <div class="sel sel--box">
					            <select name="select-gender" id="select-gender">
					                <option value="" disabled>(MXN) Mexican Peso</option>
					                <option value="mxn">US Dollars</option>
					            </select>
					        </div>
					    </div>
					</li>
				</ul>
			</div>
	</section>
</main>