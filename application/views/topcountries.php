<?php echo $header; ?>

<main>

	<?php echo $menu_lateral; ?>	

	<!-- Burger-Brand -->
	<p class="b-brand">Top Countries</p>

	<section class="bgslider interiorsections" id="sectionTopCountries">
		    <img src="<?php echo base_url('assets/img/clouds.png') ?>" alt="clouds" class="conjunto-nubes">
			
			 <div data-aos="zoom-in" id="TCmap">
			 	<svg></svg>
			 	<div class="pop"></div>
				<div class="orb"></div>
			 </div> 
		    
			<ul class="countries-list">
			    <li class="ITA" id="Italy">Italy</li>
			    <li class="ESP" id="Spain">Spain</li>
			    <li class="USA" id="Usa">USA</li>
			    <li class="AUS" id="Australia">Australia</li>
			    <li class="FRA" id="France">France</li>
			    <li class="MEX" id="Mexico">Mexico</li>
			    <li class="CHN" id="China">China</li>
			    <li class="CAN" id="Canada">Canada</li>
			</ul>			
	</section>
	<div id="italyInfo"  class="InfoCountry hidden">
				<div class="flexcontainer">
				  <ul class="contentInfoCountry">
				      <li class="FlagCountry" id="Italy_Flag"><span class="imgFlags"><img src="<?php echo base_url('assets/img/flags/it.png') ?>" alt="italyFlag"></span> </li>
				      <li class="datosCountry" id="Italy_info">
				          <div class="contentDCountry">
				           <p class="countryName"> Italy </p>
				           <p class="countryPics">100 PICS for this country</p> 
				           <p class="countryLanguage"> Language Spoken:<span class="Nativelanguage"> Italian </span> </p> 
				           <p class="countryVisa">Visa Needed: NO </p> 
				           <p class="countryReligion"> Religion: Catholic</p> 
				           <p class="countryCapital"> Country Capital:<span class="capitalName"> Rome </span> </p>  
				          </div>
				      </li>
				  </ul>
				  <ul class="buttonsEnlace">
				  		 <li class="selectCountry">			  		 	
				  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/check.png') ?>" alt="icono más" class="icon"> Select Country </a>
				  		 </li>
				  		 <li class="moreCountries">
				  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="icono más" class="icon"> Add more countries </a>
				  		 </li>
				  		 <li class="LP">
				  		 	<a href="<?php echo site_url('travel/visatop') ?>"><img src="<?php echo base_url('assets/img/iconospng/passport.png') ?>" alt="icono más" class="icon"> Let's Plan </a>
				  		 </li>
				  </ul>
				</div>
			</div>

			<div id="spainInfo"  class="InfoCountry hidden">
				<div class="flexcontainer">
				  <ul class="contentInfoCountry">
				      <li class="FlagCountry" id="Spain_Flag"><span class="imgFlags"><img src="<?php echo base_url('assets/img/flags/es.png') ?>" alt="spainFlag"></span></li>
				      <li class="datosCountry" id="Spain_info">
				          <div class="contentDCountry">
				           <p class="countryName"> SPAIN</p>
				           <p class="countryPics">200 PICS for this country</p> 
				           <p class="countryLanguage"> Language Spoken:<span class="Nativelanguage"> Spanish </span> </p> 
				           <p class="countryVisa">Visa Needed: NO </p> 
				           <p class="countryReligion"> Religion: Catholic</p> 
				           <p class="countryCapital"> Country Capital:<span class="capitalName"> Madrid </span> </p>  
				          </div>
				      </li>
				  </ul>
				  <ul class="buttonsEnlace">
				  		 <li class="selectCountry">			  		 	
				  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/check.png') ?>" alt="icono más" class="icon"> Select Country </a>
				  		 </li>
				  		 <li class="moreCountries">
				  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="icono más" class="icon"> Add more countries </a>
				  		 </li>
				  		 <li class="LP">
				  		 	<a href="<?php echo site_url('travel/visatopc') ?>"><img src="<?php echo base_url('assets/img/iconospng/passport.png') ?>" alt="icono más" class="icon"> Let's Plan </a>
				  		 </li>
				  </ul>
			    </div>
			</div>

			<div id="usaInfo"  class="InfoCountry hidden">
				<div class="flexcontainer">
				  <ul class="contentInfoCountry">
				      <li class="FlagCountry" id="USA_Flag"><span class="imgFlags"><img src="<?php echo base_url('assets/img/flags/us.png') ?>" alt="usaFlag"></span></li>
				      <li class="datosCountry" id="USA_info">
				          <div class="contentDCountry">
				           <p class="countryName"> U.S.A</p>
				           <p class="countryPics">250 PICS for this country</p> 
				           <p class="countryLanguage"> Language Spoken:<span class="Nativelanguage"> English </span> </p> 
				           <p class="countryVisa">Visa Needed: YES </p> 
				           <p class="countryReligion"> Religion: Catholic</p> 
				           <p class="countryCapital"> Country Capital:<span class="capitalName"> Whashington D.C. </span> </p>  
				          </div>
				      </li>
				  </ul>
				  <ul class="buttonsEnlace">
				  		 <li class="selectCountry">			  		 	
				  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/check.png') ?>" alt="icono más" class="icon"> Select Country </a>
				  		 </li>
				  		 <li class="moreCountries">
				  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="icono más" class="icon"> Add more countries </a>
				  		 </li>
				  		 <li class="LP">
				  		 	<a href="<?php echo site_url('travel/visatopc') ?>"><img src="<?php echo base_url('assets/img/iconospng/passport.png') ?>" alt="icono más" class="icon"> Let's Plan </a>
				  		 </li>
				  </ul>
				</div>
			</div>

			<div id="australiaInfo"  class="InfoCountry hidden">
				<div class="flexcontainer">
				  <ul class="contentInfoCountry">
				      <li class="FlagCountry" id="Australia_Flag"><span class="imgFlags"><img src="<?php echo base_url('assets/img/flags/au.png') ?>" alt="australiaFlag"></span></li>
				      <li class="datosCountry" id="Australia_info">
				          <div class="contentDCountry">
				           <p class="countryName"> Australia</p>
				           <p class="countryPics">180 PICS for this country</p> 
				           <p class="countryLanguage"> Language Spoken:<span class="Nativelanguage"> English </span> </p> 
				           <p class="countryVisa">Visa Needed: Yes </p> 
				           <p class="countryReligion"> Religion: Christianity</p> 
				           <p class="countryCapital"> Country Capital:<span class="capitalName"> Canberra </span> </p>  
				          </div>
				      </li>
				  </ul>
				  <ul class="buttonsEnlace">
				  		 <li class="selectCountry">			  		 	
				  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/check.png') ?>" alt="icono más" class="icon"> Select Country </a>
				  		 </li>
				  		 <li class="moreCountries">
				  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="icono más" class="icon"> Add more countries </a>
				  		 </li>
				  		 <li class="LP">
				  		 	<a href="<?php echo site_url('travel/visatopc') ?>"><img src="<?php echo base_url('assets/img/iconospng/passport.png') ?>" alt="icono más" class="icon"> Let's Plan </a>
				  		 </li>
				  </ul>
			   </div>
			</div>

			<div id="franceInfo"  class="InfoCountry hidden">
				<div class="flexcontainer">
					  <ul class="contentInfoCountry">
					      <li class="FlagCountry" id="France_Flag"><span class="imgFlags"><img src="<?php echo base_url('assets/img/flags/fr.png') ?>" alt="franceFlag"></span></li>
					      <li class="datosCountry" id="France_info">
					          <div class="contentDCountry">
					           <p class="countryName"> France </p>
					           <p class="countryPics">200 PICS for this country</p> 
					           <p class="countryLanguage"> Language Spoken:<span class="Nativelanguage"> French </span> </p> 
					           <p class="countryVisa">Visa Needed: NO </p> 
					           <p class="countryReligion"> Religion: Catholic</p> 
					           <p class="countryCapital"> Country Capital:<span class="capitalName"> Paris </span> </p>  
					          </div>
					      </li>
					  </ul>
					  <ul class="buttonsEnlace">
					  		 <li class="selectCountry">			  		 	
					  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/check.png') ?>" alt="icono más" class="icon"> Select Country </a>
					  		 </li>
					  		 <li class="moreCountries">
					  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="icono más" class="icon"> Add more countries </a>
					  		 </li>
					  		 <li class="LP">
					  		 	<a href="<?php echo site_url('travel/visatopc') ?>"><img src="<?php echo base_url('assets/img/iconospng/passport.png') ?>" alt="icono más" class="icon"> Let's Plan </a>
					  		 </li>
					  </ul>
				</div>
			</div>

			<div id="mexicoInfo"  class="InfoCountry hidden">
				<div class="flexcontainer">
					  <ul class="contentInfoCountry">
					      <li class="FlagCountry" id="Mexico_Flag"><span class="imgFlags"><img src="<?php echo base_url('assets/img/flags/mx.png') ?>" alt="mexicoFlag"></span></li>
					      <li class="datosCountry" id="Mexico_info">
					          <div class="contentDCountry">
					           <p class="countryName"> Mexico </p>
					           <p class="countryPics">300 PICS for this country</p> 
					           <p class="countryLanguage"> Language Spoken:<span class="Nativelanguage"> Spanish </span> </p> 
					           <p class="countryVisa">Visa Needed: NO </p> 
					           <p class="countryReligion"> Religion: Catholic</p> 
					           <p class="countryCapital"> Country Capital:<span class="capitalName"> Mexico City </span> </p>  
					          </div>
					      </li>
					  </ul>
					  <ul class="buttonsEnlace">
					  		 <li class="selectCountry">			  		 	
					  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/check.png') ?>" alt="icono más" class="icon"> Select Country </a>
					  		 </li>
					  		 <li class="moreCountries">
					  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="icono más" class="icon"> Add more countries </a>
					  		 </li>
					  		 <li class="LP">
					  		 	<a href="<?php echo site_url('travel/visatopc') ?>"><img src="<?php echo base_url('assets/img/iconospng/passport.png') ?>" alt="icono más" class="icon"> Let's Plan </a>
					  		 </li>
					  </ul>
			  </div>
			</div>

			<div id="chinaInfo"  class="InfoCountry hidden">
				<div class="flexcontainer">
					  <ul class="contentInfoCountry">
					      <li class="FlagCountry" id="China_Flag"><span class="imgFlags"><img src="<?php echo base_url('assets/img/flags/cn.png') ?>" alt="chinaFlag"></span></li>
					      <li class="datosCountry" id="China_info">
					          <div class="contentDCountry">
					           <p class="countryName"> China </p>
					           <p class="countryPics">150 PICS for this country</p> 
					           <p class="countryLanguage"> Language Spoken:<span class="Nativelanguage"> Chinese </span> </p> 
					           <p class="countryVisa">Visa Needed: YES </p> 
					           <p class="countryReligion"> Religion: Traditional chinese religion</p> 
					           <p class="countryCapital"> Country Capital:<span class="capitalName"> Beijing </span> </p>  
					          </div>
					      </li>
					  </ul>
					  <ul class="buttonsEnlace">
					  		 <li class="selectCountry">			  		 	
					  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/check.png') ?>" alt="icono más" class="icon"> Select Country </a>
					  		 </li>
					  		 <li class="moreCountries">
					  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="icono más" class="icon"> Add more countries </a>
					  		 </li>
					  		 <li class="LP">
				  		 		<a href="<?php echo site_url('travel/visatopc') ?>"><img src="<?php echo base_url('assets/img/iconospng/passport.png') ?>" alt="icono más" class="icon"> Let's Plan </a>
				  		 	</li>
					  </ul>
				</div>
			</div>

			<div id="canadaInfo"  class="InfoCountry hidden">
				<div class="flexcontainer">
				  <ul class="contentInfoCountry">
				      <li class="FlagCountry" id="Canada_Flag"><span class="imgFlags"><img src="<?php echo base_url('assets/img/flags/ca.png') ?>" alt="canadaFlag"></span></li>
				      <li class="datosCountry" id="Canada_info">
				          <div class="contentDCountry">
				           <p class="countryName"> Canada </p>
				           <p class="countryPics">300 PICS for this country</p> 
				           <p class="countryLanguage"> Language Spoken:<span class="Nativelanguage"> English, French </span> </p> 
				           <p class="countryVisa">Visa Needed: NO </p> 
				           <p class="countryReligion"> Religion: Christianity</p> 
				           <p class="countryCapital"> Country Capital:<span class="capitalName"> Ottawa </span> </p>  
				          </div>
				      </li>
				  </ul>
				  <ul class="buttonsEnlace">
				  		 <li class="selectCountry">			  		 	
				  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/check.png') ?>" alt="icono más" class="icon"> Select Country </a>
				  		 </li>
				  		 <li class="moreCountries">
				  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="icono más" class="icon"> Add more countries </a>
				  		 </li>
				  		 <li class="LP">
				  		 	<a href="<?php echo site_url('travel/visatopc') ?>"><img src="<?php echo base_url('assets/img/iconospng/passport.png') ?>" alt="icono más" class="icon"> Let's Plan </a>
				  		 </li>
				  </ul>
				</div>
			</div>
</main>