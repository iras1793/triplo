<?php echo $header; ?>


<main>

	<!-- Burger-Brand -->
<!-- 	<p class="b-brand">Plan Journey</p> -->
	<section class="bgslider interiorsections" id="sectionVisa">
			<h1 class="countrynameVisa"> Mexico </h1>
			<!-- <div class="bgflag_visa"><img src="img/bgFlags/mexico_bg.jpg" alt="Mexico Flag background"></div> -->
			<div class="sectionContent">
				<img src="<?php echo base_url('assets/img/bgFlags/mexico_bg.jpg') ?>" alt="Mexico Flag background" class="bgflag_visa">
				<div class="contentDCountry">
				           <p class="countryDescription"> People with a nationality from China requiere a visa to enter Mexico. </p>
				           <p class="countryLanguage"> Language Spoken: Spanish </p> 
				           <p class="countryCurrency">Currency: Mexican Peso </p> 
				           <p class="countryReligion"> Religion: Catholic</p> 
				           <p class="countryRestrictions"> Drone restrictions: </p> 
				           <a href="https://embamex.sre.gob.mx/australia/index.php/visitorvisa" class="visaRequeriments" target="_blank">
				           	<img src="<?php echo base_url('assets/img/iconospng/visa.png') ?>" alt="icono más" class="icon"> Visa Requeriments. 
				           </a>

				</div>
				<ul class="buttonsEnlace">
				  		 <li class="moreCountries">
				  		 	<a href="#"><img src="<?php echo base_url('assets/img/iconospng/savenote.png') ?>" alt="icono más" class="icon"> Add to Notes </a>
				  		 </li>
				  		 <li class="selectCountry">			  		 	
				  		 	<a href="<?php echo site_url('travel/letsplantopcountries') ?>"> Aceptar </a>
				  		 </li>
				 </ul>
			</div>
	</section>
</main>
