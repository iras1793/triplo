<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->
	<!--<p class="b-brand darkblue">Plan Journey</p>-->

	<section class="bgslider interiorsections" id="sectionTrackMoney">
			
			<div class="sectionContent">
				<h1 class="SectionName"> Budget List </h1>
				<div class="content">
					 <div class="tabbed">
					    <input type="radio" name="tabs" id="tab-nav-addbudget"  class="inputtitletab" checked>
					    <label for="tab-nav-addbudget" class="labeltitletab">Budget</label>
					    <input type="radio" name="tabs" id="tab-nav-expenses" class="inputtitletab">
					    <label for="tab-nav-expenses" class="labeltitletab">Expenses</label>
					    <div class="tabs">
					      <div>
					      	<div id="moneyleft" class="Mymoneyleft">
									<ul>
								        <li class="moneytrackIcon"><img src="<?php echo base_url('assets/img/iconospng/trackmoneyHover.png') ?>" alt="iconMoneyTrack"  data-aos="fade-in" class="imgicon"></li>
								        <li class="amountleft"></li>
								        <p class="amountL">$300 USD</p>
								    </ul>
							</div>
					      	<h2> Add Budget </h2>
					      	<ul class="contentAddBudget">
					      		<li class="Budgetform">
					      			<form action="" method="post" class="form" id="formbudget">
							            <div class="form__field">
							              <input type="text" placeholder="Amount">
							            </div>
							             <div class="form__field">
								            <div class="sel sel--box">
								                  <select name="select-currency" id="select-currency">
								                    <option value="" disabled>Currency</option>
								                    <option value="USDcurrency">(USD) American Dollar</option>
								                    <option value="MXNcurrency">(MXN) Mexican peso</option>
								                  </select>
								                </div>
								          </div>
								          <div class="form__field">
							              <input type="text" placeholder="Description">
							            </div>
							            <div class="form__field">
								            <div class="sel sel--box">
								                  <select name="select-person" id="select-person">
								                    <option value="" disabled>Person</option>
								                    <option value="friend1">Friend 1</option>
								                    <option value="friend2">Friend 2</option>
								                  </select>
								                </div>
								        </div>

							            <div class="form__field">
							              <input type="submit" value="Add friend" class="btnsend">
							            </div>

							          </form>
					      		</li>
					      		<li class="currency">
					      		    
					      		    <p class="instruction">Select to consult your friend's budget</p>
									    <div class="form__field">
								            <div class="sel sel--box">
								                  <select name="select-person" id="select-person">
								                    <option value="" disabled>Person</option>
								                    <option value="friend1">Friend 1</option>
								                    <option value="friend2">Friend 2</option>
								                  </select>
								                </div>
								        </div>
										<div class="form__field">
											<label for="fname1" class="labelcurrency"> Person Name: <input  class="txtperson" type="text" id="fname1" name="person" value="Me" disabled=""></label>
											<label for="famount1" class="labelcurrency"> Amount: <input  class="txtamount" type="text" id="famount1" name="amount" value="$30.00" disabled=""></label>
										  	<label for="fcurrency1" class="labelcurrency"> Currency: <input  class="txtcurrency" type="text" id="fcurrency1" name="currency" value="MXN" disabled=""></label>
											<label for="fconcept1" class="labelcurrency"> Description: <input  class="txtdescription" type="text" id="fconcept1" name="concept" value="Descripción" disabled=""></label>
											
											
										</div>
										
										
					      			<!--<ul class="contentExpenses">
					            	<li class="HeadExpense">							          
										<p class="tituloHexpense" id="persontitlecurrency">Person</p>
										<p class="tituloHexpense" id="amountitlecurrency">Amount</p>
										<p class="tituloHexpense" id="currencytitlecurrency">Currency</p>
										<p class="tituloHexpense" id="concepttitlecurrency">Description</p>
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtperson" type="text" id="person1" name="person" value="Me" disabled="">
											<input  class="txtamount" type="text" id="amount1" name="amount" value="$30.00" disabled="">
											<input  class="txtcurrency" type="text" id="crrency1" name="currency" value="MXN" disabled="">
											<input  class="txtdescription" type="text" id="concept1" name="concept" value="" disabled="">
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtperson" type="text" id="person2" name="person" value="Tania" disabled="">
											<input  class="txtamount" type="text" id="amount2" name="amount" value="$10.00" disabled="">
											<input  class="txtcurrency" type="text" id="crrency2" name="currency" value="MXN" disabled="">
											<input  class="txtdescription" type="text" id="concept2" name="concept2" value="" disabled="">
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtperson" type="text" id="person3" name="person" value="Alfredo" disabled="">
											<input  class="txtamount" type="text" id="amount3" name="amount" value="$50.00" disabled="">
											<input  class="txtcurrency" type="text" id="crrency3" name="currency" value="USD" disabled="">
											<input  class="txtdescription" type="text" id="concept3" name="concept3" value="" disabled="">
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtperson" type="text" id="person4" name="person" value="" disabled="">
											<input  class="txtamount" type="text" id="amount4" name="amount" value="" disabled="">
											<input  class="txtcurrency" type="text" id="crrency4" name="currency" value="" disabled="">
											<input  class="txtdescription" type="text" id="concept4" name="concept4" value="" disabled="">
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtperson" type="text" id="person5" name="person" value="" disabled="">
											<input  class="txtamount" type="text" id="amount5" name="amount" value="" disabled="">
											<input  class="txtcurrency" type="text" id="crrency5" name="currency" value="" disabled="">
											<input  class="txtdescription" type="text" id="concept5" name="concept5" value="" disabled="">
										
							        </li>
							        
							    </ul>-->
					      		</li>
					      	</ul>
					      </div>
					      <div>
					      	<div id="moneyleftExpenses" class="Mymoneyleft">
										<ul>
									        <li class="moneytrackIcon"><img src="<?php echo base_url('assets/img/iconospng/trackmoneyHover.png') ?>" alt="iconMoneyTrack"  data-aos="fade-in" class="imgicon"></li>
									        <li class="amountleft"></li>
									        <p class="amountL">$100 USD</p>
									        <p class="friendname">Alberto</p>
									    </ul>
							</div>
					      	<h2> Expenses </h2>
					      	 <ul class="contentExpenses">
					            	<li class="HeadExpense">							          
										<p class="tituloHexpense" id="amountitle">Amount</p>
										<p class="tituloHexpense" id="persontitle">Person</p>
										<p class="tituloHexpense" id="concepttitle">Description</p>
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtamount" type="text" id="amount6" name="amount" value="$30.00" disabled="">
											<input  class="txtperson" type="text" id="person6" name="person" value="Me" disabled="">
											<input  class="txtdescription" type="text" id="concept6" name="concept" value="" disabled="">
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtamount" type="text" id="amount7" name="amount" value="$10.00" disabled="">
											<input  class="txtperson" type="text" id="person7" name="person" value="Tania" disabled="">
											<input  class="txtdescription" type="text" id="concept7" name="concept2" value="" disabled="">
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtamount" type="text" id="amount8" name="amount" value="$100.00" disabled="">
											<input  class="txtperson" type="text" id="person8" name="person" value="Alberto" disabled="">
											<input  class="txtdescription" type="text" id="concept8" name="concept" value="" disabled="">
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtamount" type="text" id="amount9" name="amount" value="" disabled="">
											<input  class="txtperson" type="text" id="person9" name="person" value="" disabled="">
											<input  class="txtdescription" type="text" id="concept9" name="concept" value="" disabled="">
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtamount" type="text" id="amount10" name="amount" value="" disabled="">
											<input  class="txtperson" type="text" id="person10" name="person" value="" disabled="">
											<input  class="txtdescription" type="text" id="concept10" name="concept" value="" disabled="">
										
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtamount" type="text" id="amount11" name="amount" value="" disabled="">
											<input  class="txtperson" type="text" id="person11" name="person" value="" disabled="">
											<input  class="txtdescription" type="text" id="concept11" name="concept" value="" disabled="">
							        </li>
							        <li class="optionsExpense">							          		
											<input  class="txtamount" type="text" id="amount12" name="amount" value="">
											<input  class="txtperson" type="text" id="person12" name="person" value="">
											<input  class="txtdescription" type="text" id="concept12" name="concept" value="">
							        </li>
							        <li class="optionsExpense">							          										
											<input  class="txtamount" type="text" id="amount13" name="amount" value="">									
											<input  class="txtperson" type="text" id="person13" name="person" value="">										
											<input  class="txtdescription" type="text" id="concept13" name="concept" value="">										
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtamount" type="text" id="amount14" name="amount" value="">
											<input  class="txtperson" type="text" id="person14" name="person" value="">
											<input  class="txtdescription" type="text" id="concept14" name="concept" value="">
							        </li>
							        <li class="optionsExpense">							          
											<input  class="txtamount" type="text" id="amount15" name="amount" value="">
											<input  class="txtperson" type="text" id="person15" name="person" value="">
											<input  class="txtdescription" type="text" id="concept15" name="concept" value="">
							        </li>
							    </ul>
					      </div>
					      
					    </div> <!--end tabs-->
					  </div>
					
					
				</div> <!-- end content -->
			</div>
		<img src="<?php echo base_url('assets/img/bgBudget.png') ?>" alt="bgbudget"  data-aos="zoom-in-down" class="bgbudget" data-aos-offset="200" data-aos-easing="ease-in-sine" loading="lazy">
	</section>

</main>
