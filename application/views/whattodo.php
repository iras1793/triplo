<?php echo $header; ?>


<main>
	<?php
  		echo $menu_lateral;
	?>	

	<section class="bgslider interiorsections" id="sectionWhatDo">
			

			<div class="sectionContent">
					<h1 class="titleWD"> LIMA PERU</h1>
					<h1 class="subtitle"> know what to do? <hr>
					<hr class="vertical"></h1>
					<img src="<?php echo base_url('assets/img/place20.jpg') ?>" alt="peru" data-aos="fade-in" class="placeWhatD">
					<ul class="options">
						<li class="btnselectmap"><a href="<?php echo site_url('travel/topplaces') ?>">Top Places to visit</a></li>
						<li class="btnselectmap"><a href="<?php echo site_url('travel/topactivities') ?>">Top Activities</a></li>
						<li class="btnselectmap"><a href="<?php echo site_url('travel/picturebrowsing') ?>">Pic Browsing</a></li>
					</ul>
			</div>
		<img src="<?php echo base_url('assets/img/cloud.png') ?>" alt="camino" data-aos="fade-in" class="cloud1">
		<img src="<?php echo base_url('assets/img/cloud.png') ?>" alt="camino" data-aos="fade-in" class="cloud2">
		<img src="<?php echo base_url('assets/img/iconospng/aves.png') ?>" alt="arbustoverde"  data-aos="zoom-in-down" class="birds" data-aos-offset="300" data-aos-easing="ease-in-sine">
		<img src="<?php echo base_url('assets/img/iconospng/arbustoverde.png') ?>" alt="arbustoverde" class="greentree" data-aos="zoom-in-up">
		<img src="<?php echo base_url('assets/img/iconospng/arbustorosa.png') ?>" alt="arbustorosa" class="pinktree" data-aos="zoom-in-up">	

		<a href="<?php echo site_url('travel/tripplan') ?>" class="enlace"> Continue <img src="<?php echo base_url('assets/img/iconospng/go.png') ?>" alt="icono go" class="icon"></a>								 
	</section>
		
</main>
