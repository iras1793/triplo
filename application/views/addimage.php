<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	

	<section class="bgslider interiorsections" id="sectionAddimage">

			<div class="sectionContent">
				<h1 class="SectionName"> Add Pic (Photo) </h1>
				<div id="addimage" class="stepscontent">
					 <div class="container">
						    <div class="wrapper">
						      <ul class="steps">
						      	<li class="is-active">Upload</li>
						        <li>Name</li>
						        <li>Where</li>
						        <li>Guide</li>
						        <li>Guide contact</li>
						        <li>Entry</li>
						      </ul>
						      <form class="form-wrapper">
						      	<fieldset class="section  is-active">
						          <h2 class="questionstep">If you want upload a photo, please select an option...</h2>
						          <label for="file-addphoto"> Add photo</label>
						          <input type="file" id="file-addphoto"/>
						           <script type="application/javascript">
												jQuery('input[type=file]').change(function(){
												 var filename = jQuery(this).val().split('\\').pop();
												 var idname = jQuery(this).attr('id');
												 console.log(jQuery(this));
												 console.log(filename);
												 console.log(idname);
												 jQuery('span.'+idname).next().find('span').html(filename);
												});
								   </script>
						          <label for="file-takephoto">Take photo</label>
						          <input type="file" capture="camera" id="file-takephoto"/>
						           <script type="application/javascript">
												jQuery('input[type=file]').change(function(){
												 var filename = jQuery(this).val().split('\\').pop();
												 var idname = jQuery(this).attr('id');
												 console.log(jQuery(this));
												 console.log(filename);
												 console.log(idname);
												 jQuery('span.'+idname).next().find('span').html(filename);
												});
								   </script>
						          
						         <!--  <button class="btnselectmap">Add photo</button>
						          <button class="btnselectmap">Take photo</button>-->
						          <div class="button">Continue</div>
						        </fieldset>
						        <fieldset class="section">
						          <h2 class="questionstep">Name of place?</h2>
						          <label class="inputstep">
							          <input type="text" class="name" placeholder="Type a name of place">
							      </label>
						          <div class="button">Continue</div>
						        </fieldset>
						        <fieldset class="section">
						          <h2 class="questionstep">Where is this place located?</h2>
							        <label class="inputstep">
							          <input type="text" class="name" placeholder="Location place">
							        </label>
							        <div class="button">Continue</div>
						        </fieldset>
						        <fieldset class="section">
						          <h2 class="questionstep">Is a guide needed to visit this place?</h2>
						          	<div class="optionsrbuttons">
								        <label class="radio">
											<input type="radio" name="guide" value="1">
										    <span>No guide needed</span>
									    </label>
										<label class="radio">
											<input type="radio" name="guide" value="2" checked>
											<span>Yes, its needed</span>
										</label>
										<label class="radio">
											<input type="radio" name="guide" value="3">
											<span>Only if you want one</span>
										</label>
									</div>
							        <div class="button">Continue</div>
						        </fieldset>
						        <fieldset class="section">
							          <h2 class="questionstep">What guide would you recommend?</h2>
							          <div class="formstep">
									        <label class="inputstep">
									          <span class="labelstep" for="guidename">Full name:</span>
									          <input type="text" class="name" placeholder="Name's guide here" id="guidename">
									        </label>
									        <label class="inputstep">
									          <span class="labelstep" for="guidecontact">Contact email or phone:</span>
									          <input type="text" class="name" placeholder="person@email.com" id="guidecontact">
									        </label>
									        <label class="inputstep">
									          <span class="labelstep" for="guidewebsite">Website:</span>
									          <input type="text" class="name" placeholder="www.site.com" id="guidewebsite">
									        </label>
									        <label class="inputstep">
									          <span class="labelstep" for="guidesocial">Social media:</span>
									          <input type="text" class="name" placeholder="/@username" id="guidesocial">
									        </label>
									        <label class="inputstep">
									          <span class="labelstep">Picture:</span>
									          <label for="file-guidepicture">
												    select picture
												</label>
												<input id="file-guidepicture" type="file"/>
									        </label>
									        <script type="application/javascript">
												jQuery('input[type=file]').change(function(){
												 var filename = jQuery(this).val().split('\\').pop();
												 var idname = jQuery(this).attr('id');
												 console.log(jQuery(this));
												 console.log(filename);
												 console.log(idname);
												 jQuery('span.'+idname).next().find('span').html(filename);
												});
												</script>
									  </div>
								       <div class="button">Continue</div>
						        </fieldset>
						        <fieldset class="section">
						          <h2 class="questionstep">Is there an entry free?</h2>
						          <div class="optionsrbuttons">
							          	<label class="radio">
											<input type="radio" name="entry" value="1">
										    <span>Yes</span>
									    </label>
										<label class="radio">
											<input type="radio" name="entry" value="2" checked>
											<span>No</span>
										</label>
								        <label class="inputstep">
								          <span class="labelstep" for="howmuch">How much:</span>
								          <input type="text" class="name" placeholder="$" id="howmuch">
								        </label>
								  </div>
						          <input class="submit button" type="submit" value="Add">
						        </fieldset>
						        <fieldset class="section">
						          <h3 class="congrats">Thank you so much for adding your Pic!!!</h3>
						          <p class="congratstxt">As soon as this is evaluated and verified we'll let you know and provide you with your PICS.
						          	<br>
						        	 For any questions or comments feel free to contact us.</p>
						          <a href="<?php echo site_url('backpack/addpic') ?>" class="enlace"> Back to Add pic <img src="<?php echo base_url('assets/img/iconospng/go.png') ?>" alt="icono go" class="icon"></a>
						          <!-- <div class="button">Reset Form</div> -->
						        </fieldset>
						      </form>
						    </div>
						  </div>

					</div> <!-- END CONTAINER -->
				</div>	<!-- END STEPS CONTENT -->
			</div><!-- END SECTION CONTENT -->


			
	</section> <!-- END SECTION ADD IMAGE -->
</main>
