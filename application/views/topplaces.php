<?php echo $header; ?>

<main>

	<?php
  		echo $menu_lateral;
	?>	
	<!-- Burger-Brand -->
	<p class="b-brand">Plan Journey</p>

	<section class="bgslider interiorsections" id="sectionTopPlaces">

			<div class="sectionContent">
				<!-- <h1 class="SectionName"> Activities </h1> -->
				<p class="subSectionName"> Top Places </p> 

				<div class="item-grid">
  
					  <div class="item">
					    <div class="item-image">
					      <img src="<?php echo base_url('assets/img/PlaceTop1.jpg') ?>" alt="place top1" />
					    </div>
					    <div class="item-text">
					      <div class="item-text-wrapper">
					        <h2 class="item-text-title">Pagoda Tower</h2>
					        <p class="item-text-dek">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper. Aliquam et sapien eu sapien convallis vehicula at a risus.</p>
					      </div>
					    </div>
					    <a class="item-link" href=""></a>
					  </div>
				  
					 <div class="item">
					    <div class="item-image">
					      <img src="<?php echo base_url('assets/img/PlaceTop2.jpg') ?>" alt="place top2" />
					    </div>
					    <div class="item-text">
					      <div class="item-text-wrapper">
					        <h2 class="item-text-title">Monument to Revolution, Mexico</h2>
					        <p class="item-text-dek">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper. Aliquam et sapien eu sapien convallis vehicula at a risus.</p>
					      </div>
					    </div>
					    <a class="item-link" href=""></a>
					  </div>
				  
					  <div class="item">
					    <div class="item-image">
					      <img src="<?php echo base_url('assets/img/PlaceTop3.jpg') ?>" alt="place top3" />
					    </div>
					    <div class="item-text">
					      <div class="item-text-wrapper">
					        <h2 class="item-text-title">Great Wall of China</h2>
					        <p class="item-text-dek">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper. Aliquam et sapien eu sapien convallis vehicula at a risus.</p>
					      </div>
					    </div>
					    <a class="item-link" href=""></a>
					  </div>
				  
					 <div class="item">
					    <div class="item-image">
					      <img src="<?php echo base_url('assets/img/PlaceTop4.jpg') ?>" alt="place top4" />
					    </div>
					    <div class="item-text">
					      <div class="item-text-wrapper">
					        <h2 class="item-text-title">Norway</h2>
					        <p class="item-text-dek">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper. Aliquam et sapien eu sapien convallis vehicula at a risus.</p>
					      </div>
					    </div>
					    <a class="item-link" href=""></a>
					  </div>
				  
					  <div class="item">
					    <div class="item-image">
					      <img src="<?php echo base_url('assets/img/PlaceTop5.jpg') ?>" alt="place top5" />
					    </div>
					    <div class="item-text">
					      <div class="item-text-wrapper">
					        <h2 class="item-text-title">Fugi, Japan</h2>
					        <p class="item-text-dek">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper. Aliquam et sapien eu sapien convallis vehicula at a risus.</p>
					      </div>
					    </div>
					    <a class="item-link" href=""></a>
					  </div>
				  
					  <div class="item">
					    <div class="item-image">
					      <img src="<?php echo base_url('assets/img/PlaceTop6.jpg') ?>" alt="place top6" />
					    </div>
					    <div class="item-text">
					      <div class="item-text-wrapper">
					        <h2 class="item-text-title">St. Basil's Cathedral</h2>
					        <p class="item-text-dek">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper. Aliquam et sapien eu sapien convallis vehicula at a risus.</p>
					      </div>
					    </div>
					    <a class="item-link" href=""></a>
					  </div>

					  <div class="item">
					    <div class="item-image">
					      <img src="<?php echo base_url('assets/img/PlaceTop7.jpg') ?>" alt="place top7" />
					    </div>
					    <div class="item-text">
					      <div class="item-text-wrapper">
					        <h2 class="item-text-title">Norway</h2>
					        <p class="item-text-dek">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper. Aliquam et sapien eu sapien convallis vehicula at a risus.</p>
					      </div>
					    </div>
					    <a class="item-link" href=""></a>
					  </div>

					  <div class="item">
					    <div class="item-image">
					      <img src="<?php echo base_url('assets/img/PlaceTop8.jpg') ?>" alt="place top8" />
					    </div>
					    <div class="item-text">
					      <div class="item-text-wrapper">
					        <h2 class="item-text-title">Norway</h2>
					        <p class="item-text-dek">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper. Aliquam et sapien eu sapien convallis vehicula at a risus.</p>
					      </div>
					    </div>
					    <a class="item-link" href=""></a>
					  </div>

					  <div class="item">
					    <div class="item-image">
					      <img src="<?php echo base_url('assets/img/PlaceTop9.jpg') ?>" alt="place top9" />
					    </div>
					    <div class="item-text">
					      <div class="item-text-wrapper">
					        <h2 class="item-text-title">Japan</h2>
					        <p class="item-text-dek">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper. Aliquam et sapien eu sapien convallis vehicula at a risus.</p>
					      </div>
					    </div>
					    <a class="item-link" href=""></a>
					  </div>

					  <div class="item">
					    <div class="item-image">
					      <img src="<?php echo base_url('assets/img/PlaceTop10.jpg') ?>" alt="place top10" />
					    </div>
					    <div class="item-text">
					      <div class="item-text-wrapper">
					        <h2 class="item-text-title">China</h2>
					        <p class="item-text-dek">Cras euismod vitae nisl nec laoreet. Phasellus ac massa non ex vulputate lacinia. Etiam pretium, purus eget aliquet efficitur, velit felis elementum tortor, sit amet dapibus mauris diam nec risus. In ornare tincidunt sem in mattis. Ut porta dapibus velit malesuada congue. Sed ac tincidunt velit, nec molestie sem. Nam quam metus, laoreet et tristique eu, rhoncus sed elit. Suspendisse imperdiet fermentum nisl vel ullamcorper. Aliquam et sapien eu sapien convallis vehicula at a risus.</p>
					      </div>
					    </div>
					    <a class="item-link" href=""></a>
					  </div>

				</div> <!-- item grid ends -->


			</div>
	</section>
</main>