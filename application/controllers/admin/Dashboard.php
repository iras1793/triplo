<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


	var $fields = array(
			'titulo' => array('value'=>'','validate' => array(
											'label'   => 'Titulo',
											'rules'   => 'trim|xss_clean|required'
										)),
			'subtitulo' => array('value'=>'','validate' => array(
											'label'   => 'Subtitulo',
											'rules'   => 'trim|xss_clean|required'
										)),
	);

	var $data = array('pagina'=>'dashboard','title'=>'.:Triplo - Admin:.', 'miga_pan'=>'', 'encabezado'=>'');
	var $tabla = '';
	var $id    = '';
	
	var $files_config = array(
		'upload_path'   => './assets/gallery/nuevas',
		'allowed_types' => 'jpg|png|jpeg',
		'max_size'      => 15000,//15MB
		'max_width'     => 0,
		'max_height'    => 0
	);
	

	var $estilos = array(
		'assets/admin/css/dataTables.min.css',
		'assets/admin/css/buttons.dataTables.min.css'
	);

	var $javascript = array(
		'assets/admin/js/app/dataTables/jquery.dataTables.min.js',
		'assets/admin/js/app/dataTables/dataTables.buttons.min.js',
		'assets/admin/js/app/dataTables/buttons.flash.min.js',
		'assets/admin/js/app/dataTables/jszip.min.js',
		'assets/admin/js/app/dataTables/pdfmake.js',
		'assets/admin/js/app/dataTables/vfs_fonts.js',
		'assets/admin/js/app/dataTables/buttons.html5.min.js',
		'assets/admin/js/app/dataTables/buttons.print.min.js'
	);


	
	function __construct(){
		parent::__construct();
		if( !$this->auth->loggedin() )
			redirect('admin/login');
	}	
	
	

	/**
	 * Funcion principal encargada de mostrar el template, tambien se encarga de obtener la informacion
	 * necesaria para realizar el login
	 * @return [template/html->login] [HTML relacionado al login]
	 */
	public function index(){
	    
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;
        
        $this->data['encabezado'] = 'Dashboard';
        
		#$this->general->table = '';
		#$this->general->id    = '';
		#$this->data['casos']  = $this->general->get(null, array('status'=>1));

		$this->template->content->view('admin/dashboard', $this->data);
		$this->template->publish('admin/template_admin');
	}//end function






	/**
	 * Funcion privada que ayuda a validar campos
	 */
	private function config_validates(){
		$config = array();
		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}
		$this->form_validation->set_rules($config);
		foreach( $this->fields as $key=>$field )
			$this->fields[$key]['value'] = $this->input->post($key,true);
	}//end config_validates



}