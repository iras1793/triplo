<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {


	var $fields = array(
			'usuario' => array('value'=>'','validate' => array(
											'label'   => 'Email',
											'rules'   => 'trim|xss_clean|required'
										)),
			'email' => array('value'=>'','validate' => array(
											'label'   => 'Email',
											'rules'   => 'trim|xss_clean|required|valid_email'
										)),
			'pwd' => array('value'=>'','validate' => array(
											'label'   => 'Password',
											'rules'   => 'trim|xss_clean'
										)),
			'idrol' => array('value'=>'','validate' => array(
											'label'   => 'Permissions',
											'rules'   => 'trim|xss_clean|required|numeric'
										)),
			'nueva_pwd' => array('value'=>'','validate' => array(
											'label'   => 'New Password',
											'rules'   => 'trim|xss_clean'
										))
	);

	var $data = array('pagina'=>'users','title'=>'.:Triplo - Admin:.', 'miga_pan'=>'', 'encabezado'=>'');
	var $tabla = 'usuarios';
	var $id    = 'id';
	
	var $files_config = array(
		'upload_path'   => './assets/gallery/nuevas',
		'allowed_types' => 'jpg|png|jpeg',
		'max_size'      => 15000,//15MB
		'max_width'     => 0,
		'max_height'    => 0
	);
	

	var $estilos = array(
		'assets/admin/css/dataTables.min.css',
		'assets/admin/css/buttons.dataTables.min.css'
	);

	var $javascript = array(
		'assets/admin/js/app/dataTables/jquery.dataTables.min.js',
		'assets/admin/js/app/dataTables/dataTables.buttons.min.js',
		'assets/admin/js/app/dataTables/buttons.flash.min.js',
		'assets/admin/js/app/dataTables/jszip.min.js',
		'assets/admin/js/app/dataTables/pdfmake.js',
		'assets/admin/js/app/dataTables/vfs_fonts.js',
		'assets/admin/js/app/dataTables/buttons.html5.min.js',
		'assets/admin/js/app/dataTables/buttons.print.min.js'
	);


	
	function __construct(){
		parent::__construct();
		if( !$this->auth->loggedin() )
			redirect('admin/login');
	}	
	
	

	/**
	 * Funcion principal encargada de mostrar el template, tambien se encarga de obtener la informacion
	 * necesaria para realizar el login
	 * @return [template/html->login] [HTML relacionado al login]
	 */
	public function index(){
		
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
		$this->data['usuarios']  = $this->general->get(null, array('status'=>1));
		
		$this->data['miga_pan'] = 'Users';
		$this->data['encabezado'] = 'Users';
		$this->template->content->view('admin/users', $this->data);
		$this->template->publish('admin/template_admin');
	}//end function





	public function add($id=''){
		
		
		$this->data['encabezado'] = 'New User';
		$this->data['miga_pan']   = "Users / New User";
		
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		//logica
		$this->messages->clear();
		$this->config_validates();
		if( $this->form_validation->run() != FALSE ):
			//log_message("error","\n\nPasa validaciones\n\n");
			$this->tabla = 'usuarios';
			$this->id    = 'id';
			$this->general->table = $this->tabla;
			$this->general->id    = $this->id;

			
			unset($this->fields['nueva_pwd']);

			$this->fields['pwd']['value'] = md5($this->fields['pwd']['value']);
			

			$success = $this->general->insert($this->fields);
			if ($success):
				$this->messages->add("<strong>Record added successfully.</strong>","success");
				foreach( $this->fields as $key=>$field )
					$this->fields[$key]['value'] = '';
			else:
				$this->messages->add("<strong>Please, complete all fields that are required</strong>","error");
			endif;
			
		endif;
		//termina logica
		$this->data['id'] = '';
		$this->fields['nueva_pwd']['value'] = '';
		$this->fields['repetir']['value']   = '';

		$this->general->table = 'roles';
		$this->general->id    = 'id';
		$this->data['roles']  = $this->general->get(null, array('status'=>1));

		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'add';
		$this->template->content->view('admin/users_form', $this->data);
		$this->template->publish('admin/template_admin');
	}

	public function edit($id=''){
		if( is_null($id) || !is_numeric($id) || $id <= 0):
			redirect('admin/users');
		endif;
		
		$this->data['encabezado'] = 'Edit User';
		$this->data['miga_pan']   = "Users / Edit User";

		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		//logica
		$this->messages->clear();
		$this->config_validates();
		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
		
		if( $this->form_validation->run() != FALSE && $id > 0):
			//log_message("error","\n\nPasa validaciones\n\n");

			if( !empty($this->fields['nueva_pwd']['value']) ):
				$this->fields['pwd']['value'] = md5($this->fields['nueva_pwd']['value']);
			else:
				unset($this->fields['pwd']);
			endif;
			unset($this->fields['nueva_pwd']);
			unset($this->fields['repetir']);
			
			$success = $this->general->update($this->fields, $id);
			if ($success):
				$this->messages->add("<strong>Record edited successfully.</strong>","success");
			else:
				$this->messages->add("<strong>Please, complete all fields that are required</strong>","error");
			endif;

		endif;

		$info = $this->general->get(null,array('id'=>$id, 'status'=>1), 'usuario, email, idrol');
		foreach($this->fields as $key => $value):
			if( $key=='nueva_pwd' || $key=='repetir' || $key =='pwd')
				continue;
			$this->fields[$key]['value'] = $info[0]->$key;
		endforeach;
		//termina logica
		
		$this->fields['nueva_pwd']['value'] = '';
		$this->fields['repetir']['value']   = '';
		$this->fields['pwd']['value']       = '';

		$this->general->table = 'roles';
		$this->general->id    = 'id';
		$this->data['roles']  = $this->general->get(null, array('status'=>1));

		$this->data['id']     = $id;
		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'edit';
		$this->template->content->view('admin/users_form', $this->data);
		$this->template->publish('admin/template_admin');
	}



	public function delete($id=null){
		if( is_null($id) || !is_numeric($id) || $id <= 0)
			redirect("admin/users");

		$this->messages->clear();
		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
		$campos['status']['value'] = 0;
		$success = $this->general->update($campos, $id);
		if ($success):
			$this->messages->add("<strong>The record has been removed successfully</strong>","success");
		else:
			$this->messages->add("<strong>An error was detected, please try again later</strong>","error");
		endif;
		redirect("admin/users");
	}





	/**
	 * Funcion privada que ayuda a validar campos
	 */
	private function config_validates(){
		$config = array();
		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}
		$this->form_validation->set_rules($config);
		foreach( $this->fields as $key=>$field )
			$this->fields[$key]['value'] = $this->input->post($key,true);
	}//end config_validates



}