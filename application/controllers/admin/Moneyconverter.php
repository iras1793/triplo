<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Moneyconverter extends CI_Controller {


	var $fields = array(
			'nombre' => array('value'=>'','validate' => array(
											'label'   => 'Name',
											'rules'   => 'trim|xss_clean|required'
										)),
			'tipo' => array('value'=>'','validate' => array(
											'label'   => 'Type of currency',
											'rules'   => 'trim|xss_clean|required'
										)),
			'valor' => array('value'=>'','validate' => array(
											'label'   => 'Value',
											'rules'   => 'trim|xss_clean|required'
										)),
			'imagen_moneda' => array('value'=>'','validate' => array(
											'label'   => 'Flag currency',
											'rules'   => 'trim|xss_clean|callback_store[imagen_moneda]'
										)),
			'imagen_pais' => array('value'=>'','validate' => array(
											'label'   => 'Country currency',
											'rules'   => 'trim|xss_clean|callback_store[imagen_pais]'
										)),
	);

	var $data = array('pagina'=>'moneyconverter','title'=>'.:Triplo - Admin:.', 'miga_pan'=>'', 'encabezado'=>'');
	var $tabla = 'moneda_cambio';
	var $id    = 'id';

	var $files_config = array(
		'upload_path'   => './assets/monedas',
		'allowed_types' => 'jpg|png|jpeg',
		'max_size'      => 5000,//5MB
		'max_width'     => 0,
		'max_height'    => 0
	);
	
	var $estilos = array(
		'assets/admin/css/dataTables.min.css',
		'assets/admin/css/buttons.dataTables.min.css'
	);

	var $javascript = array(
		'assets/admin/js/app/dataTables/jquery.dataTables.min.js',
		'assets/admin/js/app/dataTables/dataTables.buttons.min.js',
		'assets/admin/js/app/dataTables/buttons.flash.min.js',
		'assets/admin/js/app/dataTables/jszip.min.js',
		'assets/admin/js/app/dataTables/pdfmake.js',
		'assets/admin/js/app/dataTables/vfs_fonts.js',
		'assets/admin/js/app/dataTables/buttons.html5.min.js',
		'assets/admin/js/app/dataTables/buttons.print.min.js'
	);


	
	function __construct(){
		parent::__construct();
		if( !$this->auth->loggedin() )
			redirect('admin/login');
		$this->data['estilos']    = $this->estilos;
		$this->data['javascript'] = $this->javascript;			
	}	
	
	

	/**
	 * Funcion principal encargada de mostrar el template, tambien se encarga de obtener la informacion
	 * necesaria del convertidor de moneda
	 * @return [template/html->login] [HTML relacionado al login]
	 */
	public function index(){
	    
		$this->data['javascript'] = null;
		$this->data['estilos']    = null;
 
		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
		$this->data['converter']  = $this->general->get(null, array('status'=>1));

		$this->data['miga_pan'] = 'Money Converter';
		$this->data['encabezado'] = 'Money Converter';

		$this->template->content->view('admin/moneyconverter', $this->data);
		$this->template->publish('admin/template_admin');
	}//end function


	public function add($id=''){

		$this->data['encabezado'] = 'New Currency';
		$this->data['miga_pan']   = "Money Converter / New Currency";
		
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		//logica
		$this->messages->clear();
		$this->config_validates();
		if( $this->form_validation->run() != FALSE ):
			//log_message("error","\n\nPasa validaciones\n\n");
			$this->tabla = 'moneda_cambio';
			$this->id    = 'id';
			$this->general->table = $this->tabla;
			$this->general->id    = $this->id;
			
			$success = $this->general->insert($this->fields);
			if ($success):
				$this->messages->add("<strong>Record added successfully.</strong>","success");
				foreach( $this->fields as $key=>$field )
					$this->fields[$key]['value'] = '';
			else:
				$this->messages->add("<strong>Please, complete all fields that are required</strong>","error");
			endif;
			
		endif;
		//termina logica
		$this->data['id'] = '';


		$this->general->table = 'roles';
		$this->general->id    = 'id';
		$this->data['roles']  = $this->general->get(null, array('status'=>1));

		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'add';
		$this->template->content->view('admin/money_form', $this->data);
		$this->template->publish('admin/template_admin');
	}


	public function edit($id=''){
		if( is_null($id) || !is_numeric($id) || $id <= 0):
			redirect('admin/moneyconverter');
		endif;
		
		$this->data['encabezado'] = 'Edit Currency';
		$this->data['miga_pan']   = "Money Converter / Edit Currency";

		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		//logica
		$this->messages->clear();
		$this->config_validates();
		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
		
		if( $this->form_validation->run() != FALSE && $id > 0):
			//log_message("error","\n\nPasa validaciones\n\n");

			if( empty($this->fields['imagen_moneda']['value']) ):
				unset($this->fields['imagen_moneda']);
			endif;

			if( empty($this->fields['imagen_pais']['value']) ):
				unset($this->fields['imagen_pais']);
			endif;
			
			$success = $this->general->update($this->fields, $id);
			if ($success):
				$this->messages->add("<strong>Record edited successfully.</strong>","success");
			else:
				$this->messages->add("<strong>Please, complete all fields that are required</strong>","error");
			endif;

		endif;

		$this->fields['imagen_moneda']['value'] = '';
		$this->fields['imagen_pais']['value']   = '';

		$info = $this->general->get(null,array('id'=>$id, 'status'=>1));
		foreach($this->fields as $key => $value):
			$this->fields[$key]['value'] = $info[0]->$key;
		endforeach;
		//termina logica

		$this->data['id']     = $id;
		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'edit';
		$this->template->content->view('admin/money_form', $this->data);
		$this->template->publish('admin/template_admin');
	}



	public function delete($id=null){
		if( is_null($id) || !is_numeric($id) || $id <= 0)
			redirect('admin/moneyconverter');

		$this->messages->clear();
		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
		$campos['status']['value'] = 0;
		$success = $this->general->update($campos, $id);
		if ($success):
			$this->messages->add("<strong>The record has been removed successfully</strong>","success");
		else:
			$this->messages->add("<strong>An error was detected, please try again later</strong>","error");
		endif;
		redirect('admin/moneyconverter');
	}


	public function refreshCurrency(){
		$response = array('respuesta'=>'406', 'result'=>array()); #406 -> respuesta no aceptable
		$curl     = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL            => "https://api.exchangeratesapi.io/latest?base=USD",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "GET",
			CURLOPT_HTTPHEADER     => array(
				"Accept: */*",
				"Cache-Control: no-cache",
				"Connection: keep-alive",
				"Host: api.exchangeratesapi.io",
				"Postman-Token: 1685b37e-4a5b-4120-b2bb-6c76ca2d3ed4,78aeeb7f-abfd-494d-970a-6b473c06fe0c",
				"User-Agent: PostmanRuntime/7.15.0",
				"accept-encoding: gzip, deflate",
				"cache-control: no-cache",
				"cookie: __cfduid=df140e064b6184641791e18b9239402501586743893"
			),
		));

		$api_result = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err):
			$response['result'] = $err;
		else:
			$api_result = json_decode($api_result, true);
			$this->general->table = $this->tabla;
			$this->general->id    = $this->id;
			$monedas = $this->general->get(null, array('status'=>1));
			$campos2 = array();
			if (is_array($monedas)):
				foreach ($monedas as $key => $data):
					if ( array_key_exists($data->tipo, $api_result['rates']) ):
						$campos['valor']['value'] = round($api_result['rates'][$data->tipo], 2);
						$campos2[$data->tipo]['value'] = round($api_result['rates'][$data->tipo], 2);
						$success = $this->general->update($campos, $data->id);
					endif;
				endforeach;
			$response['respuesta'] = '200';
			endif;
			array_push($response['result'], $campos2);
		endif;
		echo json_encode($response);
	}


	public function store($value='',$source=''){

		if( isset($_FILES) && array_key_exists($source, $_FILES) && !empty($_FILES[$source]['name'])):
			$this->upload->initialize($this->files_config);
			$ext = pathinfo($_FILES[$source]["name"], PATHINFO_EXTENSION);
			$nuevo_nombre = "thumb_".date('Y-m-d-H-i-s').".".$ext;
			$_FILES[$source]["name"] = $nuevo_nombre;
			if (!$this->upload->do_upload($source)):
				$error = array('error' => $this->upload->display_errors());
				$error_message = $error["error"];
				$this->messages->add("<center><strong>Error to upload imagen, please try again later</strong><center>","warning");
			else:
				$data = array('image_metadata' => $this->upload->data());
				$this->fields[$source]['value'] = $data['image_metadata']['file_name'];
			endif;
		endif;
	}

	/**
	 * Funcion privada que ayuda a validar campos
	 */
	private function config_validates(){
		$config = array();
		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}
		$this->form_validation->set_rules($config);
		foreach( $this->fields as $key=>$field )
			$this->fields[$key]['value'] = $this->input->post($key,true);
	}//end config_validates



}