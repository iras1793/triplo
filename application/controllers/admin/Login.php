<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	var $fields = array(
			'email' => array('value'=>'','validate' => array(
											'label'   => 'Email',
											'rules'   => 'trim|required|xss_clean|valid_email'
										)),
			'pwd' => array('value'=>'','validate' => array(
											'label'   => 'Contraseña',
											'rules'   => 'trim|required|xss_clean'
										))
		);


	var $data = array('pagina'=>'login','title'=>'.:Triplo - Admin:.');
	var $tabla = 'usuarios';
	var $id    = 'id';

	/**
	 * Funcion principal encargada de mostrar el template, tambien se encarga de obtener la informacion
	 * necesaria para realizar el login
	 * @return [template/html->login] [HTML relacionado al login]
	 */
	public function index(){
		$this->messages->clear();
		#if($this->auth->loggedin())
			#redirect('admin/dashboard');

		$this->config_validates();
		if( $this->form_validation->run() != FALSE ){
			$this->general->table = $this->tabla;
			$this->general->id    = $this->id;
			$user = $this->general->getLogin($this->fields['email']['value'], $this->fields['pwd']['value']);
			if ( $user ) {
				$this->auth->login($user);
				redirect('admin/dashboard');
			}else
				$this->messages->add("Por favor verifíca tu usuario y contraseña","error");
		}
		$this->load->view('admin/login',$this->data);
	}//end function




	/**
	 * Funcion encargada de terminar la sesion del usuario
	 * @return [type] [description]
	 */
	public function logout(){
		$this->auth->logout();
		redirect('admin/login');
	}//end function


	/**
	 * Funcion privada que ayuda a validar campos
	 */
	private function config_validates(){
		$config = array();
		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}
		$this->form_validation->set_rules($config);
		foreach( $this->fields as $key=>$field )
			$this->fields[$key]['value'] = $this->input->post($key,true);
	}//end config_validates



}