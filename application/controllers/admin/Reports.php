<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {


	var $fields = array(
			'titulo' => array('value'=>'','validate' => array(
											'label'   => 'Titulo',
											'rules'   => 'trim|xss_clean|required'
										)),
			'subtitulo' => array('value'=>'','validate' => array(
											'label'   => 'Subtitulo',
											'rules'   => 'trim|xss_clean|required'
										)),
	);

	var $data = array('pagina'=>'reports','title'=>'.:Triplo - Admin:.', 'miga_pan'=>'', 'encabezado'=>'');
	var $tabla = 'usuarios';
	var $id    = 'id';
	
	var $files_config = array(
		'upload_path'   => './assets/gallery/nuevas',
		'allowed_types' => 'jpg|png|jpeg',
		'max_size'      => 15000,//15MB
		'max_width'     => 0,
		'max_height'    => 0
	);
	

	var $estilos = array(
		'assets/admin/css/dataTables.min.css',
		'assets/admin/css/buttons.dataTables.min.css'
	);

	var $javascript = array(
		'assets/admin/js/app/dataTables/jquery.dataTables.min.js',
		'assets/admin/js/app/dataTables/dataTables.buttons.min.js',
		'assets/admin/js/app/dataTables/buttons.flash.min.js',
		'assets/admin/js/app/dataTables/jszip.min.js',
		'assets/admin/js/app/dataTables/pdfmake.js',
		'assets/admin/js/app/dataTables/vfs_fonts.js',
		'assets/admin/js/app/dataTables/buttons.html5.min.js',
		'assets/admin/js/app/dataTables/buttons.print.min.js'
	);


	
	function __construct(){
		parent::__construct();
		if( !$this->auth->loggedin() )
			redirect('admin/login');
		$this->data['estilos']    = $this->estilos;
		$this->data['javascript'] = $this->javascript;
	}	
	
	

	/**
	 * Funcion principal encargada de mostrar el template, tambien se encarga de obtener la informacion
	 * necesaria para realizar el login
	 * @return [template/html->login] [HTML relacionado al login]
	 */
	public function index(){
	    
		$this->javascript = array(
			'assets/admin/js/app/chart/Chart.min.js',
			'assets/admin/js/app/chart/jspdf.js',
			'assets/admin/js/app/chart/Chart.colors.js'
		);
		$this->estilos = array(
			'assets/admin/css/Chart.min.css',
		);	    
	    
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;


        
        
        $this->data['miga_pan'] = 'Reports';
        $this->data['encabezado'] = 'Reports';
		$this->template->content->view('admin/reports', $this->data);
		$this->template->publish('admin/template_admin');
	}//end function




	public function add($continente="", $id=''){
	    $continente = mb_convert_case($continente, MB_CASE_TITLE, "UTF-8");
		
		$this->data['encabezado'] = 'New Country';
		$this->data['miga_pan']   = "Countries / $continente / New Country";
		
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos'] = $this->estilos;

		//logica
		$this->messages->clear();
		$this->config_validates();
		if( $this->form_validation->run() != FALSE ):
			//log_message("error","\n\nPasa validaciones\n\n");
			$this->tabla = 'usuarios';
			$this->id    = 'id';
			$this->general->table = $this->tabla;
			$this->general->id    = $this->id;

			$success = $this->general->insert($this->fields);
			if ($success):
				$this->messages->add("<strong>Registry added successfully.</strong>","success");
			else:
				$this->messages->add("<strong>Por favor llena los campos obligatorios</strong>","error");
			endif;
			
		endif;
		//termina logica
		$this->data['id'] = '';
		$this->data['continente'] = strtolower($continente);
		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'add';
		$this->template->content->view('admin/country_form', $this->data);
		$this->template->publish('admin/template_admin');
	}

	public function edit($continente="", $id=''){
	    $continente = mb_convert_case($continente, MB_CASE_TITLE, "UTF-8");
	    
		$this->data['encabezado'] = 'Sección principal';
		$this->data['miga_pan']   = 'Usuarios / Editar';
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos'] = $this->estilos;

		//logica

		$this->messages->clear();
		$this->config_validates();
		$this->general->table = 'usuarios';
		$this->general->id    = 'id';
		if( $this->form_validation->run() != FALSE && $id > 0){
			//log_message("error","\n\nPasa validaciones\n\n");
			$this->tabla = 'usuarios';
			$this->id    = 'id';
			$this->general->table = $this->tabla;
			$this->general->id    = $this->id;
			if(empty($this->fields['last_pwd']['value']) && empty($this->fields['nueva_pwd']['value']) && empty($this->fields['repetir']['value']) ){
				unset($this->fields['last_pwd']);
				unset($this->fields['nueva_pwd']);
				unset($this->fields['repetir']);
			}
			$this->fields['pwd']['value'] = '';
			$success = $this->general->update($this->fields, $id);
			if ($success){
				$this->messages->add("<strong>Se ha editado el registro con éxito.</strong>","success");
			} 
			else
				$this->messages->add("<strong>Por favor llena los campos obligatorios</strong>","error");
		}
		$info = $this->general->get(null,array('id'=>$id));
		foreach($this->fields as $key => $value){
			if($key=='last_pwd' || $key=='nueva_pwd' || $key=='repetir')
				continue;
			$this->fields[$key]['value'] = $info[0]->$key;
		}
		//termina logica
		$this->data['continente'] = strtolower($continente);
		$this->data['id']     = $id;
		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'edit';
		$this->template->content->view('country_form', $this->data);
		$this->template->publish('template_admin');
	}

	/**
	 * cambiar estado habilitado o deshabilitado
	 * @param  string $value [description]
	 * @return [type]        [description]
	 */
	public function status(){
		$result = array('result'=>false,'message'=>'Error al habilitar al usuario');
		
		//logica
		$this->form_validation->set_rules('estado','Estado','trim|required|xss_clean|max_length[1]');
		$this->form_validation->set_rules('registro','Registro','trim|required|xss_clean');
		if( $this->form_validation->run() != FALSE ){
			$estado   = $this->input->post('estado', true);
			$registro = $this->input->post('registro', true);
			$this->general->table = 'usuarios';
			$this->general->id    = 'id';
			$campos['habilitado']['value'] = $estado;
			$success = $this->general->update($campos, $registro);
			if($success){				
				$result['result'] = true;
				$result['message'] = ($estado == "1") ? "Se a habilitado el usuario" : "Se ha deshabilitado al usuario";
			}
		}
		//fin logica
		echo json_encode($result);
	}

	public function delete($id=null){
		if( is_null($id) || !is_numeric($id) || $id < 0)
			redirect("principal/usuarios");

		$this->messages->clear();
		$this->general->table = 'usuarios';
		$this->general->id    = 'id';
		$campos['status']['value'] = 0;
		$success = $this->general->update($campos, $id);
		if ($success){
			$this->messages->add("<strong>Se a eliminado el registro con éxito</strong>","success");
		} 
		else
			$this->messages->add("<strong>Se detectó un error, favor de intentarlo más tarde</strong>","error");
		redirect("principal/usuarios");
	}




	/**
	 * Funcion privada que ayuda a validar campos
	 */
	private function config_validates(){
		$config = array();
		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}
		$this->form_validation->set_rules($config);
		foreach( $this->fields as $key=>$field )
			$this->fields[$key]['value'] = $this->input->post($key,true);
	}//end config_validates



}