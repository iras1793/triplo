<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {


	var $fields = array(
			'titulo' => array('value'=>'','validate' => array(
											'label'   => 'Titulo',
											'rules'   => 'trim|xss_clean|required'
										)),
			'subtitulo' => array('value'=>'','validate' => array(
											'label'   => 'Subtitulo',
											'rules'   => 'trim|xss_clean|required'
										)),
	);

	var $data = array('pagina'=>'products','title'=>'.:Triplo - Admin:.', 'miga_pan'=>'', 'encabezado'=>'');
	var $tabla = 'productos';
	var $id    = 'id';
	
	var $files_config = array(
		'upload_path'   => './assets/gallery/nuevas',
		'allowed_types' => 'jpg|png|jpeg',
		'max_size'      => 15000,//15MB
		'max_width'     => 0,
		'max_height'    => 0
	);
	

	var $estilos = array(
		'assets/admin/css/dataTables.min.css',
		'assets/admin/css/buttons.dataTables.min.css'
	);

	var $javascript = array(
		'assets/admin/js/app/dataTables/jquery.dataTables.min.js',
		'assets/admin/js/app/dataTables/dataTables.buttons.min.js',
		'assets/admin/js/app/dataTables/buttons.flash.min.js',
		'assets/admin/js/app/dataTables/jszip.min.js',
		'assets/admin/js/app/dataTables/pdfmake.js',
		'assets/admin/js/app/dataTables/vfs_fonts.js',
		'assets/admin/js/app/dataTables/buttons.html5.min.js',
		'assets/admin/js/app/dataTables/buttons.print.min.js'
	);


	
	function __construct(){
		parent::__construct();
		if( !$this->auth->loggedin() )
			redirect('admin/login');
		$this->data['estilos']    = $this->estilos;
		$this->data['javascript'] = $this->javascript;
	}	
	
	

	/**
	*
	* 
	* @return [template/html->productos] [HTML relacionado a productos]
	*/
	public function index(){
		$this->data['miga_pan']   = 'Products';
		$this->data['encabezado'] = 'Products';

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		$this->general->table = $this->tabla;
		$this->general->id    = 'productos.id';
        
        $this->data['productos'] = $this->general->get(null, array('productos.status'=>1),'productos.*, productos.id as idproducto, clientes.*, clientes.nombre as nombre_cliente, clientes.id as id_cliente',"", true, array('clientes'=>'clientes.id=productos.idcliente'));
        
        
		$this->template->content->view('admin/products', $this->data);
		$this->template->publish('admin/template_admin');
	}//end function


	public function comments($product_id=null){
		if (is_null($product_id) || !is_numeric($product_id) || $product_id <=0):
			redirect("admin/products");
		endif;

		$this->data['miga_pan']   = 'Products / Comments';
		$this->data['encabezado'] = 'Comments';

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		$this->general->table = 'productos_comentarios';
		$this->general->id    = 'productos_comentarios.id';

		$this->data['comentarios'] = $this->general->get(null, array('productos_comentarios.status'=>1, 'idproducto'=>$product_id),'productos_comentarios.id as idcomentario, clientes.id as idcliente, productos_comentarios.*, clientes.nombre', "", true, array('clientes'=>'clientes.id = productos_comentarios.idcliente'));
        
        
		$this->template->content->view('admin/products_comments', $this->data);
		$this->template->publish('admin/template_admin');
	}


	/**
	 * cambiar estado habilitado o deshabilitado
	 * @param  string $value [description]
	 * @return [type]        [description]
	 */
	public function status(){
		$result = array('result'=>false,'message'=>'Error to acept/decline product');
		
		//logica
		$this->form_validation->set_rules('estado','Estado','trim|required|xss_clean|max_length[2]');
		$this->form_validation->set_rules('registro','Registro','trim|required|xss_clean');
		log_message("error",json_encode($_POST));
		if( $this->form_validation->run() != FALSE ):
			$estado   = $this->input->post('estado', true);
			$registro = explode("-", $this->input->post('registro', true));
			$registro = $registro[1];
			
			$this->general->table = 'productos';
			$this->general->id    = 'id';
			$campos['autorizado']['value'] = $estado;
			$campos['idusuario']['value']  = $this->auth->userid();
			$success = $this->general->update($campos, $registro);
			if($success):
				$result['result'] = true;
				$result['message'] = ($estado == "1") ? "The product has been accepted" : "You have declined this product";
			endif;
		endif;
		//fin logica
		echo json_encode($result);
	}

	public function delete($id=null){
		if( is_null($id) || !is_numeric($id) || $id < 0)
			redirect("admin/products");

		$this->messages->clear();
		$this->general->table = 'productos';
		$this->general->id    = 'id';
		$campos['status']['value'] = 0;
		$success = $this->general->update($campos, $id);
		if ($success):
			$this->messages->add("<strong>The record has been removed successfully</strong>","success");
		else:
			$this->messages->add("<strong>An error was detected, please try again later</strong>","error");
		endif;
		redirect("admin/products");
	}


	public function top_products(){

		$this->data['miga_pan']   = 'Products / Top Products';
		$this->data['encabezado'] = 'Top Products';

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
        
        $this->data['productos'] = $this->general->get(null, array('status'=>1, 'rating_general>='=>4));
        
        
		$this->template->content->view('admin/topproducts', $this->data);
		$this->template->publish('admin/template_admin');

	}


	public function purchased($id=null){
		$this->data['miga_pan']   = 'Products / Purchased by users';
		$this->data['encabezado'] = 'Purchased';

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
        
        $this->data['productos'] = $this->general->get(null, array('status'=>1, 'rating_general'=>4));
        
        
		$this->template->content->view('admin/purchased_products', $this->data);
		$this->template->publish('admin/template_admin');
	}


	public function claims(){
		$this->data['miga_pan']   = 'Products / Claims';
		$this->data['encabezado'] = 'Claims';

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
        
        $this->data['productos'] = $this->general->get(null, array('status'=>1, 'rating_general<='=>2));
        
        
		$this->template->content->view('admin/claims_products', $this->data);
		$this->template->publish('admin/template_admin');
	}

	/**
	 * Funcion privada que ayuda a validar campos
	 */
	private function config_validates(){
		$config = array();
		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}
		$this->form_validation->set_rules($config);
		foreach( $this->fields as $key=>$field )
			$this->fields[$key]['value'] = $this->input->post($key,true);
	}//end config_validates



}