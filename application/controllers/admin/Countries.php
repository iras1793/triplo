<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Countries extends CI_Controller {


	var $fields = array(
			'nombre' => array('value'=>'','validate' => array(
											'label'   => 'Country Name',
											'rules'   => 'trim|xss_clean|required'
										)),
			'idcontinente' => array('value'=>'','validate' => array(
											'label'   => 'Continent Name',
											'rules'   => 'trim|xss_clean'
										)),
			'capital' => array('value'=>'','validate' => array(
											'label'   => 'Capital Name',
											'rules'   => 'trim|xss_clean|required'
										)),
			'es_top' => array('value'=>'','validate' => array(
											'label'   => 'Is Top Country?',
											'rules'   => 'trim|xss_clean'
										)),
			'imagen_bandera' => array('value'=>'','validate' => array(
											'label'   => 'Flag icon',
											'rules'   => 'trim|xss_clean|callback_store[imagen_bandera]'
										)),
	);

	var $data = array('pagina'=>'countries','title'=>'.:Triplo - Admin:.', 'miga_pan'=>'', 'encabezado'=>'');
	var $tabla = 'paises';
	var $id    = 'id';
	
	var $files_config = array(
		'upload_path'   => './assets/paises',
		'allowed_types' => 'jpg|png|jpeg',
		'max_size'      => 5000,//5MB
		'max_width'     => 0,
		'max_height'    => 0
	);
	

	var $estilos = array(
		'assets/admin/css/dataTables.min.css',
		'assets/admin/css/buttons.dataTables.min.css'
	);

	var $javascript = array(
		'assets/admin/js/app/dataTables/jquery.dataTables.min.js',
		'assets/admin/js/app/dataTables/dataTables.buttons.min.js',
		'assets/admin/js/app/dataTables/buttons.flash.min.js',
		'assets/admin/js/app/dataTables/jszip.min.js',
		'assets/admin/js/app/dataTables/pdfmake.js',
		'assets/admin/js/app/dataTables/vfs_fonts.js',
		'assets/admin/js/app/dataTables/buttons.html5.min.js',
		'assets/admin/js/app/dataTables/buttons.print.min.js'
	);
	
	/*
	*Se deja esta variable para realizar la búsqueda de los ids mas rapido
	*/
	var $continenteid = array(
		'America'   => 1,
		'Europe'    => 2,
		'Africa'    => 3, 
		'Asia'      => 4,
		'Australia' => 5
	);
	
	function __construct(){
		parent::__construct();
		if( !$this->auth->loggedin() )
			redirect('admin/login');
		$this->data['estilos']    = $this->estilos;
		$this->data['javascript'] = $this->javascript;
	}	
	
	

	/**
	 * Funcion principal encargada de mostrar el template, tambien se encarga de obtener la informacion
	 * necesaria para realizar el login
	 * @return [template/html->login] [HTML relacionado al login]
	 */
	public function index(){
		
		$this->data['javascript'] = null;
		$this->data['estilos']    = null;

		
		
		$this->data['encabezado'] = 'Countries';
		$this->data['miga_pan']   = 'Countries';
		$this->template->content->view('admin/countries', $this->data);
		$this->template->publish('admin/template_admin');
	}//end function


	/**
	 * Funcion encargada de mostrar los paises de un continente previamente seleccionado
	 * @parent: index
	 * @return [template/html->countries_list] [HTML relacionado al los paises por continente]
	 */
	public function country_list($continente=''){
		$continente = mb_convert_case($continente, MB_CASE_TITLE, "UTF-8");
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
		#$this->data['casos']  = $this->general->get(null, array('status'=>1));
		
		$continente_id = $this->continenteid[$continente]; #con esto puedo buscar los paises de un continente
		$this->data['paises']  = $this->general->get(null, array('status'=>1, 'idcontinente'=>$continente_id));
		
		
		$this->data['continente'] = strtolower($continente);
		$this->data['encabezado'] = "Country List - $continente";
		$this->data['miga_pan'] = "Countries / Country List";
		$this->template->content->view('admin/country_list', $this->data);
		$this->template->publish('admin/template_admin');
	}//end function



	/**
	 * Funcion encargada de mostrar los paises de un continente previamente seleccionado
	 * @parent: index
	 * @return [template/html->countries_list] [HTML relacionado al los paises por continente]
	 */
	public function topcountries(){
		
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		$this->general->table = 'paises';
		$this->general->id    = 'id';
		$this->data['paisestop']  = $this->general->get(null, array('status'=>1, 'es_top'=>1));

		$this->data['pagina'] = 'topcountries';
		$this->data['encabezado'] = "Top Countries";
		$this->data['miga_pan'] = "Top Countries";
		$this->template->content->view('admin/topcountries', $this->data);
		$this->template->publish('admin/template_admin');
	}//end function



	public function add($continente="", $id=''){
		$continente = mb_convert_case($continente, MB_CASE_TITLE, "UTF-8");
		
		$this->data['encabezado'] = 'New Country';
		$this->data['miga_pan']   = "Countries / $continente / New Country";
		
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos'] = $this->estilos;

		//logica
		$this->messages->clear();
		$this->config_validates();
		if( $this->form_validation->run() != FALSE ):
			$this->general->table = 'paises';
			$this->general->id    = 'id';

			$this->fields['idcontinente']['value'] = $this->continenteid[$continente];
			$this->fields['es_top']['value'] = ( $this->fields['es_top']['value']=='on' || $this->fields['es_top']['value']!='') ? 1:0;
			$success = $this->general->insert($this->fields);
			if ($success):
				$this->messages->add("<strong>Registry added successfully.</strong>","success");
			else:
				$this->messages->add("<strong>Por favor llena los campos obligatorios</strong>","error");
			endif;
			
		endif;
		//termina logica
		
		foreach ($this->fields as $key => $value):
			$this->fields[$key]['value'] = '';
		endforeach;

		$this->data['id'] = '';
		$this->data['continente']   = strtolower($continente);
		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'add';
		$this->template->content->view('admin/country_form', $this->data);
		$this->template->publish('admin/template_admin');
	}

	public function edit($continente="", $id=''){
		$continente = mb_convert_case($continente, MB_CASE_TITLE, "UTF-8");
		
		$this->data['encabezado'] = 'Edit Country';
		$this->data['miga_pan']   = "Countries / $continente / Edit Country";

		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos'] = $this->estilos;

		//logica
		$this->messages->clear();
		$this->config_validates();
		$this->general->table = 'paises';
		$this->general->id    = 'id';
		if( $this->form_validation->run() != FALSE && $id > 0):
			//log_message("error","\n\nPasa validaciones\n\n");

			$this->general->table = 'paises';
			$this->general->id    = 'id';

			if(empty($this->fields['imagen_bandera']['value']) ):
				unset($this->fields['imagen_bandera']);
			endif;
			
			$this->fields['idcontinente']['value'] = $this->continenteid[$continente];
			$this->fields['es_top']['value'] = ( $this->fields['es_top']['value']=='on' || $this->fields['es_top']['value']!='') ? 1:0;

			$success = $this->general->update($this->fields, $id);
			if ($success):
				$this->messages->add("<strong>Record edited successfully.</strong>","success");
			else:
				$this->messages->add("<strong>Please, complete all fields that are required</strong>","error");
			endif;
		endif;
		$this->fields['imagen_bandera']['value'] = '';
		$info = $this->general->get(null,array('id'=>$id));
		foreach($this->fields as $key => $value):
			$this->fields[$key]['value'] = $info[0]->$key;
		endforeach;
		//termina logica
		
		$this->data['continente'] = strtolower($continente);
		$this->data['id']     = $id;
		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'edit';
		$this->template->content->view('admin/country_form', $this->data);
		$this->template->publish('admin/template_admin');
	}

	/**
	 * cambiar estado habilitado o deshabilitado
	 * @param  string $value [description]
	 * @return [type]        [description]
	 */
	public function status(){
		$result = array('result'=>false,'message'=>'Error al habilitar al usuario');
		
		//logica
		$this->form_validation->set_rules('estado','Estado','trim|required|xss_clean|max_length[1]');
		$this->form_validation->set_rules('registro','Registro','trim|required|xss_clean');
		if( $this->form_validation->run() != FALSE ){
			$estado   = $this->input->post('estado', true);
			$registro = $this->input->post('registro', true);
			$this->general->table = 'usuarios';
			$this->general->id    = 'id';
			$campos['habilitado']['value'] = $estado;
			$success = $this->general->update($campos, $registro);
			if($success){				
				$result['result'] = true;
				$result['message'] = ($estado == "1") ? "Se a habilitado el usuario" : "Se ha deshabilitado al usuario";
			}
		}
		//fin logica
		echo json_encode($result);
	}

	public function delete($continente='', $id=null){
		if( is_null($id) || !is_numeric($id) || $id < 0)
			redirect("admin/countries/country_list/$continente");

		$this->messages->clear();
		$this->general->table = 'paises';
		$this->general->id    = 'id';
		$campos['status']['value'] = 0;
		$success = $this->general->update($campos, $id);
		if ($success):
			$this->messages->add("<strong>The registry has been removed successfully</strong>","success");
		else:
			$this->messages->add("<strong>An error was detected, please try again later</strong>","error");
		endif;
		redirect("admin/countries/country_list/$continente");
	}


	public function removetopcountry($id=null){
		if( is_null($id) || !is_numeric($id) || $id < 0)
			redirect("admin/countries/topcountries");

		$this->messages->clear();
		$this->general->table = 'paises';
		$this->general->id    = 'id';
		$campos['es_top']['value'] = 0;
		$success = $this->general->update($campos, $id);
		
		if ($success):
			$this->messages->add("<strong>This country is no longer top</strong>","success");
		else:
			$this->messages->add("<strong>An error was detected, please try again later</strong>","error");
		endif;
		
		redirect("admin/countries/topcountries");
	}



	public function store($value='',$source=''){

		if( isset($_FILES) && array_key_exists($source, $_FILES) && !empty($_FILES[$source]['name'])):
			$this->upload->initialize($this->files_config);
			$ext = pathinfo($_FILES[$source]["name"], PATHINFO_EXTENSION);
			$nuevo_nombre = "thumb_".date('Y-m-d-H-i-s').".".$ext;
			$_FILES[$source]["name"] = $nuevo_nombre;
			if (!$this->upload->do_upload($source)):
				$error = array('error' => $this->upload->display_errors());
				$error_message = $error["error"];
				$this->messages->add("<center><strong>Error to upload imagen, please try again later</strong><center>","warning");
			else:
				$data = array('image_metadata' => $this->upload->data());
				$this->fields[$source]['value'] = $data['image_metadata']['file_name'];
			endif;
		endif;
	}
	


	/**
	 * Funcion privada que ayuda a validar campos
	 */
	private function config_validates(){
		$config = array();
		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}
		$this->form_validation->set_rules($config);
		foreach( $this->fields as $key=>$field )
			$this->fields[$key]['value'] = $this->input->post($key,true);
	}//end config_validates



}