<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends CI_Controller {


	var $fields = array(
			'titulo' => array('value'=>'','validate' => array(
											'label'   => 'Titulo',
											'rules'   => 'trim|xss_clean|required'
										)),
			'subtitulo' => array('value'=>'','validate' => array(
											'label'   => 'Subtitulo',
											'rules'   => 'trim|xss_clean|required'
										)),
	);

	var $data = array('pagina'=>'clients','title'=>'.:Triplo - Admin:.', 'miga_pan'=>'', 'encabezado'=>'');
	var $tabla = 'clientes';
	var $id    = 'id';
	
	var $estilos = array(
		'assets/admin/css/dataTables.min.css',
		'assets/admin/css/buttons.dataTables.min.css'
	);

	var $javascript = array(
		'assets/admin/js/app/dataTables/jquery.dataTables.min.js',
		'assets/admin/js/app/dataTables/dataTables.buttons.min.js',
		'assets/admin/js/app/dataTables/buttons.flash.min.js',
		'assets/admin/js/app/dataTables/jszip.min.js',
		'assets/admin/js/app/dataTables/pdfmake.js',
		'assets/admin/js/app/dataTables/vfs_fonts.js',
		'assets/admin/js/app/dataTables/buttons.html5.min.js',
		'assets/admin/js/app/dataTables/buttons.print.min.js'
	);


	
	function __construct(){
		parent::__construct();
		if( !$this->auth->loggedin() )
			redirect('admin/login');
	}	
	
	

	/**
	 * Funcion principal encargada de mostrar el template, tambien se encarga de obtener la informacion
	 * necesaria para realizar el login
	 * @return [template/html->login] [HTML relacionado al login]
	 */
	public function index(){
	    
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		$this->general->table = $this->tabla;
		$this->general->id    = 'clientes.id';
		$this->data['clientes']  = $this->general->get(null, array('clientes.status'=>1), 'clientes.*, clientes.id as idcliente, clientes.nombre as nombre_cliente, paises.*, paises.id as idpais, paises.nombre as nombre_pais', "", true, array('paises'=>'clientes.idpais = paises.id'));
        
        
        
        $this->data['miga_pan'] = 'Clients';
        $this->data['encabezado'] = 'Clients';
		$this->template->content->view('admin/clients', $this->data);
		$this->template->publish('admin/template_admin');
	}//end function


	/**
	 * cambiar estado habilitado o deshabilitado
	 * @param  string $value [description]
	 * @return [type]        [description]
	 */
	public function status(){
		$result = array('result'=>false,'message'=>'Error to acept/decline product');
		
		//logica
		$this->form_validation->set_rules('estado','Estado','trim|required|xss_clean|max_length[2]');
		$this->form_validation->set_rules('registro','Registro','trim|required|xss_clean');
		if( $this->form_validation->run() != FALSE ):
			$estado   = $this->input->post('estado', true);
			$registro = explode("-", $this->input->post('registro', true));
			$registro = $registro[1];
			$this->general->table = 'clientes';
			$this->general->id    = 'id';
			$campos['es_activo']['value'] = ( $estado =='1') ? 1 : -1;
			$success = $this->general->update($campos, $registro);
			if($success):
				$result['result'] = true;
				$result['message'] = ($estado == "1") ? "The client has been accepted" : "The client has been rejected";
			endif;
		endif;
		//fin logica
		echo json_encode($result);
	}

	public function delete($id=null){
		if( is_null($id) || !is_numeric($id) || $id < 0)
			redirect("admin/clients");

		$this->messages->clear();
		$this->general->table = 'usuarios';
		$this->general->id    = 'id';
		$campos['status']['value'] = 0;
		$success = $this->general->update($campos, $id);
		if ($success):
			$this->messages->add("<strong>Se a eliminado el registro con éxito</strong>","success");
		else:
			$this->messages->add("<strong>Se detectó un error, favor de intentarlo más tarde</strong>","error");
		endif;
		redirect("admin/clients");
	}





	/**
	 * Funcion privada que ayuda a validar campos
	 */
	private function config_validates(){
		$config = array();
		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}
		$this->form_validation->set_rules($config);
		foreach( $this->fields as $key=>$field )
			$this->fields[$key]['value'] = $this->input->post($key,true);
	}//end config_validates



}