<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Travel extends CI_Controller {

	var $data = array('pagina'=>'','title'=>'.:Triplo:.','encabezado'=>'','miga_pan'=>'','div_abre'=>'','div_cierra'=>'','mochila_abierta'=>'');
	var $tabla = '';
	var $id    = 'id';

	var $estilos = array(

	);

	var $javascript = array(

	);

	function __construct(){
		parent::__construct();
		#if( !$this->auth->loggedin() )
			#redirect('login');
		
		$this->data['header'] = $this->load->view('includes/header', null, TRUE);
		$this->data['menu_lateral'] = $this->load->view('includes/menulateral', null, TRUE);
		$this->data['mapa_giratorio'] = $this->load->view('includes/mapa', null, TRUE);
		$this->data['mochila_abierta'] = false;
	}

	public function index(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'dashboard';
		$this->data['title'] = '.:Plan Journey:.';
		$this->data['div_abre'] = '<div id ="BackpackTravelHome">';
		$this->data['div_cierra'] = '</div>';
		
		$this->template->content->view('travel', $this->data);
		$this->template->publish('template');
	}


	public function picturebrowsing(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'picturebrowsing';
		$this->data['title'] = '.:Picture Browsing:.';
		
		$this->template->content->view('picturebrowsing', $this->data);
		$this->template->publish('template');
	}


	public function tripplandays(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'tripplandays';
		$this->data['title'] = '.:tripplandays:.';
		
		$this->template->content->view('tripplanDays', $this->data);
		$this->template->publish('template');
	}


	public function startfinish(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'startfinish';
		$this->data['title'] = '.:Start - Finish:.';

		$this->template->content->view('startfinish', $this->data);
		$this->template->publish('template');
	}

	public function topcountries(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'topcountries';
		$this->data['title'] = '.:Top Countries:.';

		$this->template->content->view('topcountries', $this->data);
		$this->template->publish('template');
	}

	public function selectcountry(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'selectcountry';
		$this->data['title'] = '.:Select Country:.';

		$this->template->content->view('selectcountry', $this->data);
		$this->template->publish('template');
	}

	public function date(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'date';
		$this->data['title'] = '.:Date:.';

		$this->template->content->view('date', $this->data);
		$this->template->publish('template');
	}

	public function roadtrip(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'roadtrip';
		$this->data['title'] = '.:Road Trip:.';

		$this->template->content->view('roadtrip', $this->data);
		$this->template->publish('template');
	}

	public function picturebrowsing_go(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'picturebrowsing_go';
		$this->data['title'] = '.:Picture Browsing - Go:.';
		
		$this->template->content->view('picturebrowsing_go', $this->data);
		$this->template->publish('template');
	}

	public function letsplan(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'letsplan';
		$this->data['title'] = '.:Lets Plan:.';
		
		$this->template->content->view('letsplan', $this->data);
		$this->template->publish('template');
	}
	
	public function currentlocation(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'currentlocation';
		$this->data['title'] = '.:Current Location:.';
		
		$this->template->content->view('currentlocation', $this->data);
		$this->template->publish('template');
	}	
	
	public function letsplan_rt(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'letsplan_rt';
		$this->data['title'] = '.:Lets Plan RT:.';
		
		$this->template->content->view('letsplan_rt', $this->data);
		$this->template->publish('template');
	}	
	
	public function tripplan_rt(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'tripplan_rt';
		$this->data['title'] = '.:tripplan_rt:.';
		
		$this->template->content->view('tripplan_rt', $this->data);
		$this->template->publish('template');
	}	

	
	public function writingthem(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'writingthem';
		$this->data['title'] = '.:Writing them:.';
		
		$this->template->content->view('writingthem', $this->data);
		$this->template->publish('template');
	}

	
	public function tripplan(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'tripplan';
		$this->data['title'] = '.:tripplan:.';
		
		$this->template->content->view('tripplan', $this->data);
		$this->template->publish('template');
	}


	public function visatopc(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'visatopc';
		$this->data['title'] = '.:visatopc:.';
		
		$this->template->content->view('visatopc', $this->data);
		$this->template->publish('template');
	}



	public function letsplantopcountries(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'letsplantopcountries';
		$this->data['title'] = '.:letsplantopcountries:.';
		
		$this->template->content->view('letsplantopcountries', $this->data);
		$this->template->publish('template');
	}


	public function visa(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'visa';
		$this->data['title'] = '.:visa:.';
		
		$this->template->content->view('visa', $this->data);
		$this->template->publish('template');
	}

	public function letsplanselectcountry(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'letsplanselectcountry';
		$this->data['title'] = '.:letsplanselectcountry:.';
		
		$this->template->content->view('letsplanselectcountry', $this->data);
		$this->template->publish('template');
	}


	public function whattodo(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'whattodo';
		$this->data['title'] = '.:whattodo:.';
		
		$this->template->content->view('whattodo', $this->data);
		$this->template->publish('template');
	}

	public function whodecides(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'whodecides';
		$this->data['title'] = '.:whodecides:.';
		
		$this->template->content->view('whodecides', $this->data);
		$this->template->publish('template');
	}

	public function topplaces(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'topplaces';
		$this->data['title'] = '.:topplaces:.';
		
		$this->template->content->view('topplaces', $this->data);
		$this->template->publish('template');
	}

	public function activities(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'activities';
		$this->data['title'] = '.:activities:.';
		
		$this->template->content->view('activities', $this->data);
		$this->template->publish('template');
	}


	public function topactivities(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'topactivities';
		$this->data['title'] = '.:topactivities:.';
		
		$this->template->content->view('topactivities', $this->data);
		$this->template->publish('template');
	}

	public function picturebrowsingmain(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'topplaces';
		$this->data['title'] = '.:topplaces:.';
		
		$this->template->content->view('topplaces', $this->data);
		$this->template->publish('template');
	}

	
	public function write(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'write';
		$this->data['title'] = '.:Write:.';
		
		$this->template->content->view('write', $this->data);
		$this->template->publish('template');
	}


}
