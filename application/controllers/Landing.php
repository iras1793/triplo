<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	var $data = array('pagina'=>'','title'=>'.:Triplo:.','encabezado'=>'','miga_pan'=>'','div_abre'=>'','div_cierra'=>'');
	var $tabla = '';
	var $id    = 'id';

	var $estilos = array(

	);

	var $javascript = array(

	);

	function __construct(){
		parent::__construct();
		#if( !$this->auth->loggedin() )
			#redirect('login');
		$this->data['header'] = $this->load->view('includes/header', null, TRUE);
		$this->data['menu_lateral'] = $this->load->view('includes/menulateral', null, TRUE);
		$this->data['mapa_giratorio'] = $this->load->view('includes/mapa', null, TRUE);
		$this->data['select_paises'] = $this->load->view('includes/select_paises', null, TRUE);

	}	

	public function index(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['url_login'] = $this->facebookapi->getloginurl();
		$this->template->content->view('inicio', $this->data);
		$this->template->publish('template');
	}

}
