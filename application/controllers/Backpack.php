<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backpack extends CI_Controller {

	var $data = array('pagina'=>'','title'=>'.:Triplo:.','encabezado'=>'','miga_pan'=>'','div_abre'=>'','div_cierra'=>'','mochila_abierta'=>'');
	var $tabla = '';
	var $id    = 'id';

	var $estilos = array(

	);

	var $javascript = array(

	);

	function __construct(){
		parent::__construct();
		#if( !$this->auth->loggedin() )
			#redirect('login');
		
		$this->data['header'] = $this->load->view('includes/header', null, TRUE);
		$this->data['menu_lateral'] = $this->load->view('includes/menulateral', null, TRUE);
		$this->data['mapa_giratorio'] = $this->load->view('includes/mapa', null, TRUE);
		
	}

	public function index(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'dashboard';
		$this->data['title'] = '.:Plan Journey:.';
		$this->data['div_abre'] = '<div id ="BackpackTravelHome">';
		$this->data['div_cierra'] = '</div>';
		
		$this->template->content->view('travel', $this->data);
		$this->template->publish('template');
	}


	public function itinerary(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'itineraries';
		$this->data['title'] = '.:Itineraries:.';
		$this->data['mochila_abierta'] = true;
		$this->template->content->view('itinerary', $this->data);
		$this->template->publish('template');
	}


	public function checklist(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'checklist';
		$this->data['title'] = '.:Checklist:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('checklist', $this->data);
		$this->template->publish('template');
	}

	public function notes(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'notes';
		$this->data['title'] = '.:Notes:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('notes', $this->data);
		$this->template->publish('template');
	}

	public function activities(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'activities';
		$this->data['title'] = '.:Top Activities:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('topactivities', $this->data);
		$this->template->publish('template');
	}

	public function store(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'store';
		$this->data['title'] = '.:Store:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('store', $this->data);
		$this->template->publish('template');
	}

	public function translator(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'translator';
		$this->data['title'] = '.:Translator:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('translator', $this->data);
		$this->template->publish('template');
	}


	public function trackmoney(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'trackmoney';
		$this->data['title'] = '.:Track Money:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('trackmoney', $this->data);
		$this->template->publish('template');
	}


	public function moneyconverter(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'moneyconverter';
		$this->data['title'] = '.:Money Converter:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('moneyconverter', $this->data);
		$this->template->publish('template');
	}


	public function packinglist(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'packinglist';
		$this->data['title'] = '.:Packing List:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('packinglist', $this->data);
		$this->template->publish('template');
	}



	public function newpacking(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'newpacking';
		$this->data['title'] = '.:New Packing:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('newpacking', $this->data);
		$this->template->publish('template');
	}
	

	public function europechecklist(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'europechecklist';
		$this->data['title'] = '.:europechecklist:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('europechecklist', $this->data);
		$this->template->publish('template');
	}	
	
	public function newnote(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'newnote';
		$this->data['title'] = '.:newnote:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('newnote', $this->data);
		$this->template->publish('template');
	}	


	public function addimage(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'addimage';
		$this->data['title'] = '.:addimage:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('addimage', $this->data);
		$this->template->publish('template');
	}		

	public function addpic(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'addpic';
		$this->data['title'] = '.:addpic:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('addpic', $this->data);
		$this->template->publish('template');
	}	

	public function addstop(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'addstop';
		$this->data['title'] = '.:addstop:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('addstop', $this->data);
		$this->template->publish('template');
	}	

	public function addupdate(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'addupdate';
		$this->data['title'] = '.:addupdate:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('addupdate', $this->data);
		$this->template->publish('template');
	}
	
	public function newchecklist(){
		#$this->data['javascript'] = $this->javascript;
		$this->data['pagina'] = 'newchecklist';
		$this->data['title'] = '.:newchecklist:.';
        $this->data['mochila_abierta'] = true;
		$this->template->content->view('newchecklist', $this->data);
		$this->template->publish('template');
	}	



}
