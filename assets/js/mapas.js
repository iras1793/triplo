{let width=350
let height=250
const sensitivity=30
let projection=d3.geoOrthographic().scale(100).center([0,0]).rotate([0,30]).clipAngle([100]).translate([width/2,height/2])
const initialScale=projection.scale()
let path=d3.geoPath().projection(projection)
let svg=d3.select(".mapas").append("svg").attr("class",'width').attr("class",'height').attr("viewBox","0 0 "+width+" "+height);let globe=svg.append("circle").attr("fill","#C4E2FF").attr("stroke","white").attr("stroke-width","0.2").attr("cx",175).attr("cy",125).attr("r",initialScale)
var tooltip=d3.select("body").append("div").attr("class","tooltip");var centered;let map=svg.append("g")
d3.json("https://raw.githubusercontent.com/michael-keith/mps_interests/master/view/js/charts/data/world_map.json",function(err,d){console.log()
map.attr("class","countries").selectAll("path").data(d.features).enter().append("path").attr("class",d=>"country_"+d.properties.name.replace(" ","_")).attr("d",path).on("mouseover",showTooltip).on("mousemove",moveTooltip).on("mouseout",hideTooltip).on("click",clicked)})
svg.call(d3.drag().on('drag',()=>{const rotate=projection.rotate()
const k=sensitivity/projection.scale()
projection.rotate([rotate[0]+d3.event.dx*k,rotate[1]-d3.event.dy*k])
path=d3.geoPath().projection(projection)
svg.selectAll("path").attr("d",path)}))
function clicked(d,i){var x,y,k;if(d&&centered!==d){var centroid=path.centroid(d);var b=path.bounds(d);x=centroid[0];y=centroid[1];k=.8/Math.max((b[1][0]-b[0][0])/width,(b[1][1]-b[0][1])/height);centered=d}else{x=width/2;y=height/2;k=1;centered=null}
map.selectAll("path").classed("highlighted",function(d){return d===centered}).style("stroke-width",0.25/k+"px")}
let tooltipOffset={x:5,y:-25};function showTooltip(d){moveTooltip();tooltip.style("display","flex").text(d.properties.name)}
function moveTooltip(){tooltip.style("top",(d3.event.pageY+tooltipOffset.y)+"px").style("left",(d3.event.pageX+tooltipOffset.x)+"px")}
function hideTooltip(){tooltip.style("display","none")}
d3.timer(function(elapsed){const rotate=projection.rotate()
const k=sensitivity/projection.scale()
projection.rotate([rotate[0]-1*k,rotate[-1]])
path=d3.geoPath().projection(projection)
svg.selectAll("path").attr("d",path)},200)}
{let width=350
let height=250
const sensitivity=25
let projection=d3.geoOrthographic().scale(100).center([0,0]).rotate([0,30]).clipAngle([100]).translate([width/2,height/2])
const initialScale=projection.scale()
let path=d3.geoPath().projection(projection)
let top=d3.select("#TCmap").append("svg").attr("class",'width').attr("class",'height').attr("viewBox","0 0 "+width+" "+height);let globe=top.append("circle").attr("fill","#C4E2FF").attr("stroke","white").attr("stroke-width","0.2").attr("cx",175).attr("cy",125).attr("r",initialScale)
top.call(d3.drag().on('drag',()=>{const rotate=projection.rotate()
const k=sensitivity/projection.scale()
projection.rotate([rotate[0]+d3.event.dx*k,rotate[1]-d3.event.dy*k])
path=d3.geoPath().projection(projection)
top.selectAll("path").attr("d",path)}))
var centered;let places=[{"id":"ITA"},{"id":"ESP"},{"id":"USA"},{"id":"AUS"},{"id":"FRA"},{"id":"MEX"},{"id":"CHN"},{"id":"CAN"}]
d3.json("https://raw.githubusercontent.com/michael-keith/mps_interests/master/view/js/charts/data/world_map.json",function(err,d){const placesById={};places.forEach(place=>{placesById[place.id]=place});let feature=top.attr("class","countries").selectAll("path").data(d.features).enter().append("svg:path").attr("class",d=>"country_"+d.properties.name.replace(" ","_")).attr("fill",(d)=>{if(placesById[d.id]){return'#F3BDBD'}
return"#CDFFCC"}).on("mouseover",showTooltip).on("mousemove",moveTooltip).on("mouseout",hideTooltip).on("click",clicked)})
function clicked(d,i){feature.select("path").classed("highlighted",function(d){return d===centered}).style("stroke-width",0.25/k+"px")}
let tooltipOffset={x:5,y:-25};function showTooltip(d){moveTooltip();tooltip.style("display","flex").text(d.properties.name)}
function moveTooltip(){tooltip.style("top",(d3.event.pageY+tooltipOffset.y)+"px").style("left",(d3.event.pageX+tooltipOffset.x)+"px")}
function hideTooltip(){tooltip.style("display","none")}
d3.timer(function(elapsed){const rotate=projection.rotate()
const k=sensitivity/projection.scale()
projection.rotate([rotate[0]-1*k,rotate[-1]])
path=d3.geoPath().projection(projection)
top.selectAll("path").attr("d",path)},200)}(function(){"use strict"
var map
d3.json("https://raw.githubusercontent.com/michael-keith/mps_interests/master/view/js/charts/data/world_map.json",main)
function main(data){addLmaps()
drawFeatures(data)}
function addLmaps(){map=L.map('PB_gomap',{center:[19.4978,-99.1269],zoom:5});L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png',{attribution:'&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',subdomains:'abcd',maxZoom:19}).addTo(map);map.on("click",function(e){new L.Marker([e.latlng.lat,e.latlng.lng],{icon:myIcon,draggable:!0}).bindPopup(customPopup,customOptions).addTo(map)});var myIcon=L.icon({iconUrl:'../assets/img/iconospng/pinlocation.png',iconSize:[40,45],popupAnchor:[0,-18],shadowUrl:'../assets/img/iconospng/pinshadow.png',shadowSize:[40,45],shadowAnchor:[10,25]});var customPopup="<div class='popContent'><img src='../assets/img/PlaceTop1.jpg' alt='place' class='imagepop'/><a href='https://en.wikipedia.org/wiki/Pagoda_of_Fogong_Temple' class='linkplace' target='_blank'>Place name</a><ul class='popOptions'><li><a href=''><img src='../assets/img/iconospng/bucketlist2.png' alt='icono bucketlist' class='icon'/></a></li><li><a href=''><img src='../assets/img/iconospng/trash.png' alt='icono trash' class='icon'/></a></li></ul><div>";var customOptions={'className':'custom'}
L.svg({clickable:!0}).addTo(map)}
function projectPoint(x,y){var point=map.latLngToLayerPoint(new L.LatLng(y,x));this.stream.point(point.x,point.y)}
function drawFeatures(data){var svg=d3.select("#PB_gomap").select("svg").attr("pointer-events","auto")
var g=svg.select("g")
var transform=d3.geoTransform({point:projectPoint});var path=d3.geoPath().projection(transform)
var featureElement=g.selectAll("path").data(data.features).enter().append("path").attr("class","lands")
map.on("moveend",update);update();function update(){featureElement.attr("d",path)}}}())